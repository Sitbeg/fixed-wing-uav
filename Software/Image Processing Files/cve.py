from opcode import opname
import cv2
import numpy as np
import time 

cap=cv2.VideoCapture(0)

cap.set(200,300)

def getcont(img,dial):
  countours,hierarchy=cv2.findContours(dial,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
  for i in countours:
      area=cv2.contourArea(i)
      peri=cv2.arcLength(i,True)
      aprox=cv2.approxPolyDP(i,0.04*peri,True)
      if len(aprox)==4:
          (x,y,w,h)=cv2.boundingRect(aprox)
          ar=w/float(h)
      if area>1000 and len(aprox)>5 and len(aprox)!=10:
          warea.write(str(area))
          warea.write("\n")
          print("sth")
          cv2.drawContours(img,i,-1,(0,250,0),5)
         # time.sleep(1)

def empty(a):
    pass

cv2.namedWindow("Trackbars")

cv2.createTrackbar("Hue Min", "Trackbars", 0, 179,empty)
cv2.createTrackbar("Hue Max", "Trackbars", 179, 179,empty)
cv2.createTrackbar("Sat Min", "Trackbars", 0, 255,empty)
cv2.createTrackbar("Sat Max", "Trackbars", 255, 255,empty)
cv2.createTrackbar("Val Min", "Trackbars", 0, 255,empty)
cv2.createTrackbar("Val Max", "Trackbars", 255, 255,empty)



while True:
    success,img =cap.read()
    warea=open("area.txt","a")
    imgsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    h_min=cv2.getTrackbarPos("Hue Min","Trackbars")
    h_max=cv2.getTrackbarPos("Hue Max","Trackbars")
    s_min=cv2.getTrackbarPos("Sat Min","Trackbars")
    s_max=cv2.getTrackbarPos("Sat Max","Trackbars")
    v_min=cv2.getTrackbarPos("Val Min","Trackbars")
    v_max=cv2.getTrackbarPos("Val Max","Trackbars")
    
    lower=np.array([141,155,84])
    upper=np.array([179,255,255])
 
    mask=cv2.inRange(imgsv,lower,upper)
    
    getcont(img,mask)
    cv2.imshow("real",img)
    
    cv2.imshow("mask",mask)
    warea.close()
    cv2.waitKey(1)