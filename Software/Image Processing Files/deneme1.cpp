﻿// deneme1.cpp : Bu dosya 'main' işlevi içeriyor. Program yürütme orada başlayıp biter.
//

#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;
int hmin=141, smin=155, vmin=84;
int hmax=179, smax=255, vmax=255;

void getCont(Mat imgdil, Mat img) {

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    
    int area;
    float peri;
    findContours(imgdil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    
    vector<vector<Point>> conPoly(contours.size());
    vector<Rect> boundrect(contours.size());
    for (int i = 0; i < contours.size(); i++) {
        area = contourArea(contours[i]);

        if (area > 1000) {
            peri = arcLength(contours[i], true);
            approxPolyDP(contours[i], conPoly[i], 0.02 * peri, true);
            drawContours(img, conPoly, i, Scalar(255, 0, 255), 2);
            boundrect[i] = boundingRect(conPoly[i]);
            rectangle(img, boundrect[i].tl(), boundrect[i].br(), Scalar(0, 255, 0), 5);
            putText(img, "target", { boundrect[i].x+30,boundrect[i].y - 5 }, FONT_HERSHEY_DUPLEX, 0.75, Scalar(0, 0, 255));
        }

    }
}


int main()
{
    Mat img;
    VideoCapture cap(0);
    Mat imgGray,imgblurgray,canimage,mask,imgdil;
    Mat kernel = getStructuringElement(MORPH_RECT, Size(3, 3));
    /*namedWindow("Trackbars", (640, 220));
    createTrackbar("Hue min ", "Trackbars", &hmin, 179);
    createTrackbar("Hue min ", "Trackbars", &hmax, 179);
    createTrackbar("Hue min ", "Trackbars", &smin, 255);
    createTrackbar("Hue min ", "Trackbars", &smax, 255);
    createTrackbar("Hue min ", "Trackbars", &vmin, 255);
    createTrackbar("Hue min ", "Trackbars", &vmax, 255);*/
    
    while (true) {

        Scalar lower(hmin,smin,vmin);
        Scalar upper(hmax, smax, vmax);
        cap.read(img);
        cvtColor(img, imgGray, COLOR_BGR2HSV);
        GaussianBlur(imgGray, imgblurgray,Size(3,3),3,0);
        Canny(imgblurgray, canimage,15,50);
        dilate(canimage, imgdil, kernel);

            
        inRange(imgGray, lower, upper, mask);
        
        getCont(mask, img);
        imshow("image", img);
        
       waitKey(1);
    }

    
    
  
    return 0;
}

// Programı çalıştır: Ctrl + F5 veya Hata Ayıkla > Hata Ayıklamadan Başlat menüsü
// Programda hata ayıkla: F5 veya Hata Ayıkla > Hata Ayıklamayı Başlat menüsü

// Kullanmaya Başlama İpuçları: 
//   1. Dosyaları eklemek/yönetmek için Çözüm Gezgini penceresini kullanın
//   2. Kaynak denetimine bağlanmak için Takım Gezgini penceresini kullanın
//   3. Derleme çıktısını ve diğer iletileri görmek için Çıktı penceresini kullanın
//   4. Hataları görüntülemek için Hata Listesi penceresini kullanın
//   5. Yeni kod dosyaları oluşturmak için Projeye Git > Yeni Öğe ekle veya varolan kod dosyalarını projeye eklemek için Proje > Var Olan Öğeyi Ekle adımlarını izleyin
//   6. Bu projeyi daha sonra yeniden açmak için Dosya > Aç > Proje'ye gidip .sln uzantılı dosyayı seçin
