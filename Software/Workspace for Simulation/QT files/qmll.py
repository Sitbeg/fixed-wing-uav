from PyQt5.QtQml import*
from PyQt5.QtCore import*
from PyQt5.QtWidgets import QApplication
from dronekit import connect
from PyQt5.QtGui import *
from PyQt5.QtQuick import *
import sys 
 

class sitbeg(QObject):
	
	connection_string="127.0.0.1:14550"
	iha=connect(connection_string,wait_ready=True,timeout=100)
	app = QApplication(sys.argv)
	engine = QQmlApplicationEngine()

	altitude_ = 0
	latitude_ = 0
	longitude_ = 0
	arm_ = "0"
	pil_ = "%100"
	airspeed_ = "0"
	verticalspeed_ ="0"
	turnPlane_="0"
	pusula_= 0
	attitudeRoll_ = "0"
	attitudePitch_ = "0"
	mode_ = "----"

	#constructor
	def __init__(self):
		QObject.__init__(self)

		#Qtimer - run timer
		self.timer = QTimer()
		self.timer.timeout.connect(lambda: self.setAltitude()) # altimeter
		self.timer.timeout.connect(lambda: self.setLatitude()) 
		self.timer.timeout.connect(lambda: self.setLongitude())
		self.timer.timeout.connect(lambda: self.setArm()) # arm durumu
		self.timer.timeout.connect(lambda: self.setMode()) # mode durumu
		self.timer.timeout.connect(lambda: self.setPil()) # pil durumu
		self.timer.timeout.connect(lambda: self.setAirspeed()) # airspeed durumu
		self.timer.timeout.connect(lambda: self.setTurnPlane()) # ucak durumu
		self.timer.timeout.connect(lambda: self.setPusula()) # heading durumu
		self.timer.timeout.connect(lambda: self.setVerticalspeed()) # verticalspeed durumu
		self.timer.timeout.connect(lambda: self.setAttitudeIndRoll())
		self.timer.timeout.connect(lambda: self.setAttitudeIndPitch())		
		self.timer.start(100)

	printAltitude = pyqtSignal(str)
	printLatitude = pyqtSignal(str)
	printLongitude = pyqtSignal(str)
	printArm = pyqtSignal(str)
	printMode = pyqtSignal(str)
	printPil = pyqtSignal(str)
	printAirspeed = pyqtSignal(str)
	printTurnPlane = pyqtSignal(str)
	printVerticalspeed = pyqtSignal(str)
	printPusula = pyqtSignal(str)
	printAttitudeIndRol = pyqtSignal(str)
	printAttitudeIndPitch = pyqtSignal(str)

	def setAltitude(self): 
		self.altitude_ = self.iha.location.global_relative_frame.alt
		self.printAltitude.emit(str(self.altitude_))

	def setLatitude(self): 
		self.latitude_ = self.iha.location.global_relative_frame.lat
		self.printLatitude.emit(str(self.latitude_).split(".")[0])

	def setLongitude(self): 
		self.longitude_ = self.iha.location.global_relative_frame.lon
		self.printLongitude.emit(str(self.longitude_).split(".")[0])
	
	def setArm(self): 
		if(self.iha.armed): self.arm_ = "1"
		else: self.arm_ = "0"
		self.printArm.emit(self.arm_)
	
	def setMode(self): 
		self.mode_ = str(self.iha.mode).split(":")[1]
		self.printMode.emit(self.mode_)

	def setPil(self):
		self.pil_ = str(self.iha.battery.level)
		self.printPil.emit(self.pil_)
	
	def setTurnPlane(self):
		self.turnPlane_ += str(self.iha.attitude.roll)
		self.printTurnPlane.emit(self.turnPlane_)

	def setPusula(self):
		self.pusula_ = str(self.iha.heading)
		self.printPusula.emit(self.pusula_)

	def setAirspeed(self): 
		self.airspeed_ = str(self.iha.airspeed)
		self.printAirspeed.emit(self.airspeed_)

	def setVerticalspeed(self): 
		self.verticalspeed_ = str(self.iha.velocity[2]) 
		self.printVerticalspeed.emit(self.verticalspeed_)
	
	def altitude(self,data):
		self.data_ = data

	def setAttitudeIndRoll(self): 
		self.attitudeRoll_ += str(self.iha.attitude.roll)
		self.printAttitudeIndRol.emit(self.attitudeRoll_)

	def setAttitudeIndPitch(self): 
		self.attitudePitch_ += str(self.iha.attitude.pitch)
		self.printAttitudeIndPitch.emit(self.attitudePitch_)
   
	#to run the window application
	def run(self):
		self.engine.load('background.qml')
		self.engine.quit.connect(self.app.quit)
		if not self.engine.rootObjects():
			return -1
		return self.app.exec_()

	
	

if __name__ == "__main__":
	
	# get context
	class_obj=sitbeg()
	class_obj.engine.rootContext().setContextProperty("backend",class_obj)

		
	class_obj.run()
	sys.exit(class_obj.app.exec())
	
	
