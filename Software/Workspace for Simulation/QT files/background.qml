import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2

ApplicationWindow {
    width: 945
    height: /*738*/ 708
    visible: true
    title:"UAV-SITBEG"

    property int index: 0

    /* header: ToolBar{
        id:bar
        background:Rectangle{
            color:"dimgrey"
        }
        RowLayout{
            ToolButton{
                text:"                                                                                                                                        "
                onClicked:index=0
            }
            ToolButton{
                text:"Animation"
                onClicked:index=1
            }
        }
    }*/

    SwipeView{
        id:pages
        anchors.fill: parent
        currentIndex:index

         Page{
            id:main
            title: "Flight Tools"

            property bool run: true

            /* images */


            Image {
                id: background
                x: 0
                y: 0
                width: 945
                height: 708
                source: "sabit.png"
            }
            AnimatedImage {
                id: animation
                x: 450
                y: 40
                width: 364
                height: 100
                source: "plane (2).gif"

            }

            Text { 
                id: km
                text: qsTr("-")
                color:"purple" //hıza göre yazının rengini degis
                x:243
                y:265
                width:51
                height:21
            }

            Text {
                id: hg
                text: qsTr("-")
                color:"purple"
                x:817
                y:245
                width:61
                height:21
            }

            Image {
                id: speed_pointer
                objectName: "speed_point"
                x: 216
                y: 173 
                width: 22
                height: 91
                source: "hiz_hareketli.png"
                anchors.centerIn: speed_pointer
                property var aString: "0"
                transform: Rotation { origin.x: 11; origin.y: 80 ;angle: parseFloat(speed_pointer.aString)*3.6}
            }

            Image {
                id: horizontal_back
                x: 352
                y: 135
                width: 300	
                height: 251
                source: "horizontal hareketli.png"
                anchors.centerIn: horizontal_back
		    property var leftright: "0"
		    property var altust: "0"
		    transform:[ 
			
			Rotation { 
				origin.x: 103; 
				origin.y: 103 ;
				angle: parseFloat(horizontal_back.leftright)*3.6;
			},
			Translate { y: parseInt(horizontal_back.altust) }
		]
		
            }

            Image {
                id: horizontal_front
                x: 340
                y: 129
                width: 305
                height: 270
                source: "horizontal_sabit.png"
            }

            Image {
                id: altimeter_pointer
                x: 749
                y: 165
                width: 30
                height: 105
                source: "altimeter_yuz.png"
                anchors.centerIn: altimeter_pointer
                property var aStringaltimeter: "0"
                transform: Rotation { origin.x: 15; origin.y: 85; angle: parseFloat(altimeter_pointer.aStringaltimeter)*3.6}
            }

            Image {
                id: turn_ball
                x: 216
                y: 560
                width: 21
                height: 21
                source: "turn_coor._ball.png"
                anchors.centerIn: turn_ball
            }

            Image {
                id: turn_plane
                x: 160
                y: 483
                width: 131
                height: 51
                source: "turn_coor._plane.png"
                anchors.centerIn: turn_plane
		        property var aString: "0"
		        transform: Rotation { origin.x: 65; origin.y: 25; angle: parseFloat(turn_plane.aString)*3.6}
            }

            Image {
                id: pusula
                x: 400
                y: 421
                width: 188
                height: 188
                source: "direction_hareketli.png"
                anchors.centerIn: pusula
		        property var aString: "0"
                transform: Rotation { origin.x: 94; origin.y: 94; angle:parseInt(pusula.aString) }
            }

            Image {
                id: vertical
                x: 694
                y: 505
                width: 101
                height: 21
                source: "vertical_hareketli.png"
                anchors.centerIn: vertical
		        property var aStringvertical: 0
                transform: Rotation { origin.x: 70; origin.y: 15; angle:vertical.aStringvertical*3.6 }
            }

            //____________________________pil kısmı__________________________________
            Image{
                id: red1
                x:485
                y:10
                width:21
                height:25
                source:"pil_red"
            }
            Image{
                id: red2
                x:508
                y:10
                width:21
                height:25
                source:"pil_red"
            }
            Image{
                id: red3
                x:531
                y:10
                width:21
                height:25
                source:"pil_red"
            }
            Image{
                id: yellow1
                x:554
                y:10
                width:21
                height:25
                source:"pil_yellow"
            }
            Image{
                id: yellow2
                x:577
                y:10
                width:21
                height:25
                source:"pil_yellow"
            }
            Image{
                id: yellow3
                x:600
                y:10
                width:21
                height:25
                source:"pil_yellow"
            }
            Image{
                id: green1
                x:623
                y:10
                width:21
                height:25
                source:"pil_green"
            }
            Image{
                id: green2
                x:646
                y:10
                width:21
                height:25
                source:"pil_green"
            }
            Image{
                id: green3
                x:669
                y:10
                width:21
                height:25
                source:"pil_green"
            }
            Text {
                id: pil_text
                text:"%100"
                color:"purple"
                x:690
                y:14
                width:41
                height:25

            }

            /* _________________arm durumu __________________- */
            /*Image{
                id: arm
                x:610
                y:79
                width:76
                height:27
                source:"check1"
            } */
            Text{
                id: arm_text
                property var is_armed: 0
                text: qsTr("ARM")
                color: "#fffafa"
                x:103
                y:37
                width:25
                height:20
            }

            Text{ /* mode */
                id: mode_text
                text: qsTr("----")
                color: "#fffafa"
                x:227
                y:37
                width:20
                height:15
            }

            Text{ /* latitude */
                id: lattitude_text
                text: qsTr("----")
                color: "#fffafa"
                x:160
                y:70
                width:20
                height:15
            }

            Text{ /* longitude */
                id: longitude_text
                text: qsTr("----")
                color: "#fffafa"
                x:350
                y:70
                width:20
                height:15
            }

        }

        /*Page{
             id:page2
             title:"py"

             Image {
                 id: bg
                 source: "bg.jpg"
                 width: 945
                 height: 708
             }
             AnimatedImage {
                 id: deneme

                 width:945
                 height: 708
                 source: "plane3.gif"

             }
         } */
    }
    Connections{

        target : backend

        function onPrintAltitude(data){
            altimeter_pointer.aStringaltimeter = data
            if(arm_text.is_armed == 1) hg.text = data.slice(0,4)
            else if(arm_text.is_armed == 0) hg.text = "-----"      
        }

        function onPrintLatitude(lat_){ 
            if(arm_text.is_armed == 1) lattitude_text.text = lat_
            else if(arm_text.is_armed == 0) lattitude_text.text = "-----"      
        }

        function onPrintLongitude(lon_){
            if(arm_text.is_armed == 1) longitude_text.text = lon_
            else if(arm_text.is_armed == 0) longitude_text.text = "-----"      
        }


        function onPrintArm(arm_){
            if(arm_ == "1"){ arm_text.color = "#9370db"; arm_text.is_armed = 1}
            else if(arm_ == "0"){ arm_text.color = "#fffafa"; arm_text.is_armed = 0}

	        if(arm_ == "1") animation.playing = 1
	        else if(arm_ == "0") animation.playing = 0
        }

        function onPrintMode(mode_){
            mode_text.text = mode_;
        }

        function onPrintPil(pil_){
            pil_text.text = "%"+pil_
            if(parseInt(pil_) < 89) {green3.visible = false}
            if(parseInt(pil_) < 78) {green2.visible = false}
            if(parseInt(pil_) < 67) {green1.visible = false}
            if(parseInt(pil_) < 56) {yellow3.visible = false}
            if(parseInt(pil_) < 45) {yellow2.visible = false}
            if(parseInt(pil_) < 34) {yellow1.visible = false}
            if(parseInt(pil_) < 23) {red3.visible = false}
            if(parseInt(pil_) < 12) {red2.visible = false}
            if(parseInt(pil_) == 0) {red1.visible = false}
        }
        function onPrintAirspeed(Airspeed_){
            speed_pointer.aString = Airspeed_	       
            if(arm_text.is_armed == 1) km.text = Airspeed_.slice(0,4)
            else if(arm_text.is_armed == 0) km.text = "-----"
        }

	    function onPrintVerticalspeed(verticalspeed_){
            if(verticalspeed_[0]=="-") /* speeding up */
            {
                verticalspeed_ = verticalspeed_.slice(1);
                //console.log("aftwer:" +verticalspeed_);
                vertical.aStringvertical = parseFloat(verticalspeed_)*(9.0)
            }
            else  vertical.aStringvertical = parseFloat(verticalspeed_)*(-9.0)//vertical.aStringvertical = (parseFloat(verticalspeed_)*(-9.0)).toFixed(2)  /* speeding down */
           
        }


        // turn coordinator 
        function onPrintTurnPlane(TurnPlane_){
                turn_plane.aString = TurnPlane_
            }

        // heading
        function onPrintPusula(Pusula_){
                pusula.aString = Pusula_
            }
        
        function onprintAttitudeIndRoll(Roll_){
                horizontal_back.leftright = Roll_
            }

        function onprintAttitudeIndPitch(Pitch_){
                horizontal_back.altust = Pitch_
            }
    }
}
