#!/usr/bin/env python


from __future__ import print_function

import roslib
#roslib.load_manifest('my_project')
import sys
import rospy
import geometry_msgs.msg 
import std_msgs.msg
import std_msgs
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError
from opcode import opname
import cv2
import numpy as np
import time
from dronekit import connect, VehicleMode


class camera:

    def __init__(self):
        self.bridge = CvBridge()
        rospy.Subscriber("/camera/image_raw", Image, self.callback)

    def callback(self,data):
    
        try:
            img = self.bridge.imgmsg_to_cv2(data, desired_encoding="mono8")
        except CvBridgeError as e:
            rospy.logerr(e)
        
        imgsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        # Normal masking algorithm

        lower=np.array([141,155,84])
        upper=np.array([179,255,255])
          
        mask=cv2.inRange(imgsv,lower,upper)
        
        self.getcont(img,mask)
        mask = cv2.resize(img, (540, 540))
        cv2.imshow("mask",mask)
        img = cv2.resize(img, (540, 540))
        #cv2.imshow("real",img)
        cv2.waitKey(3)


    def getcont(self,img,dial):
        _,countours,_=cv2.findContours(dial,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)#[-2:]
        for i in countours:
            area=cv2.contourArea(i)
            peri=cv2.arcLength(i,True)
            aprox=cv2.approxPolyDP(i,0.04*peri,True)
            if len(aprox)==4:
                (x,y,w,h)=cv2.boundingRect(aprox)
                ar=w/float(h)
            if area>1000 and len(aprox)>6 and len(aprox)!=10:
                cv2.drawContours(img,i,-1,(0,250,0),5)
                time.sleep(1)

def main():
  camera()
  rospy.init_node('image_convert_node', anonymous=False)
  connection_string="127.0.0.1:14550"
  vehicle=connect(connection_string,wait_ready=True,timeout=100)
  # get the set of commands from the vehicle
  cmds = vehicle.commands
  cmds.download()
  cmds.wait_ready()
  vehicle.mode = VehicleMode("GUIDED")
  vehicle.armed = True
  vehicle.simple_takeoff(20)

  #set mode to AUTO to start mission
  vehicle.mode = VehicleMode("AUTO")

  try:
    rospy.spin()
    
  except KeyboardInterrupt:
    rospy.loginfo("Shutting down")
    cv2.destroyAllWindows()
  
  

if __name__ == '__main__':
    main()