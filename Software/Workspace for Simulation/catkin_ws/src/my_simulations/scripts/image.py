#!/usr/bin/env python

from __future__ import print_function

import roslib
#roslib.load_manifest('my_project')
import sys
import rospy
import geometry_msgs.msg 
import std_msgs.msg
import std_msgs
from sensor_msgs.msg import Image
from cv_bridge import CvBridge,CvBridgeError
from opcode import opname
import cv2
import numpy as np
import time
from dronekit import connect, VehicleMode


# modify as needed
#missionlist[0].command=mavutil.mavlink.MAV_CMD_NAV_TAKEOFF
""" eger degisiklik yaparsan
cmds.clear()

for cmd in missionlist:
    cmds.add(cmd)

cmds.upload()
"""

def calibrator(data):
    """Updates the calibration window"""

    bridge = CvBridge()

    try:
        img = bridge.imgmsg_to_cv2(data,"bgr8")
    except CvBridgeError as e:
        print(e)
   
    # get info from track bar and appy to result
    imgsv=cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
    """h_min=cv2.getTrackbarPos("Hue Min","Trackbars")
    h_max=cv2.getTrackbarPos("Hue Max","Trackbars")
    s_min=cv2.getTrackbarPos("Sat Min","Trackbars")
    s_max=cv2.getTrackbarPos("Sat Max","Trackbars")
    v_min=cv2.getTrackbarPos("Val Min","Trackbars")
    v_max=cv2.getTrackbarPos("Val Max","Trackbars")"""

    # Normal masking algorithm
    lower=np.array([141,155,84])
    upper=np.array([179,255,255])

    mask=cv2.inRange(imgsv,lower,upper)
    
    getcont(img,mask)
    cv2.imshow("real",img)

    cv2.imshow("mask",mask)
    cv2.waitKey(3)
    

def getcont(img,dial):
  countours,hierarchy=cv2.findContours(dial,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
  for i in countours:
      area=cv2.contourArea(i)
      peri=cv2.arcLength(i,True)
      aprox=cv2.approxPolyDP(i,0.04*peri,True)
      if len(aprox)==4:
          (x,y,w,h)=cv2.boundingRect(aprox)
          ar=w/float(h)
      if area>1000 and len(aprox)>6 and len(aprox)!=10:
          cv2.drawContours(img,i,-1,(0,250,0),5)
          time.sleep(1)

def empty(a):
    pass


def main(node, subscriber):
    """Creates a camera calibration node and keeps it running."""

    # Initialize node
    rospy.init_node(node)

    # Initialize CV Bridge
    bridge = CvBridge()

    # Create a named window to calibrate HSV values in
    """cv2.namedWindow("Trackbars")

    # Creating track bar
    cv2.createTrackbar("Hue Min", "Trackbars", 0, 179,empty)
    cv2.createTrackbar("Hue Max", "Trackbars", 179, 179,empty)
    cv2.createTrackbar("Sat Min", "Trackbars", 0, 255,empty)
    cv2.createTrackbar("Sat Max", "Trackbars", 255, 255,empty)
    cv2.createTrackbar("Val Min", "Trackbars", 0, 255,empty)
    cv2.createTrackbar("Val Max", "Trackbars", 255, 255,empty)"""

    
    # Subscribe to the specified ROS topic and process it continuously
    rospy.Subscriber(subscriber, Image, calibrator, callback_args=(bridge))
    """try:
        rospy.spin()
    except KeyboardInterrupt:
        print("shutting down")
        cv2.destroyAllWindows()"""

if __name__ == "__main__":

    #cv2.namedWindow("Image",cv2.WINDOW_NORMAL)
    #cv2.moveWindow("Image",25,75)

    connection_string="127.0.0.1:14550"
    vehicle=connect(connection_string,wait_ready=True,timeout=100)
    # get the set of commands from the vehicle
    cmds = vehicle.commands
    cmds.download()
    cmds.wait_ready()

    #save the vehicle cocmmands to a list
    missionlist = []
    for cmd in cmds:
        missionlist.append(cmd)

    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True
    vehicle.simple_takeoff(20)

    #set mode to AUTO to start mission
    vehicle.mode = VehicleMode("AUTO")
    while True:
        main("second_Task", "Image")

    
    
    
    

    

   
 
   

    

    
    
