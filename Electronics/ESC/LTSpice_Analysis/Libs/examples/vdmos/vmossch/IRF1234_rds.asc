Version 4
SHEET 1 2212 680
WIRE 512 0 416 0
WIRE 672 0 592 0
WIRE 416 32 416 0
WIRE -32 48 -32 32
WIRE -32 64 -32 48
WIRE 672 64 672 0
WIRE 368 112 240 112
WIRE -32 208 -32 144
WIRE 240 208 240 192
WIRE 416 208 416 128
WIRE 416 208 240 208
WIRE 672 208 672 144
WIRE 672 208 416 208
WIRE 240 224 240 208
FLAG 240 224 0
FLAG -32 208 0
FLAG 416 0 drain
FLAG -32 48 Rds
SYMBOL nmos 368 32 R0
SYMATTR InstName M1
SYMATTR VALUE IRF1234
SYMBOL voltage 672 48 R0
SYMATTR InstName Vds
SYMATTR Value 0
SYMBOL voltage 240 96 R0
WINDOW 0 31 43 Left 0
WINDOW 3 37 75 Left 0
SYMATTR InstName V2
SYMATTR Value {vgs}
SYMBOL current 592 0 R90
WINDOW 0 -32 40 VBottom 0
WINDOW 3 32 40 VTop 0
SYMATTR InstName Id
SYMATTR Value 0
SYMBOL bv -32 48 R0
SYMATTR InstName B1
SYMATTR Value V=v(drain)/I(Id)
TEXT 144 16 LEFT 0 .dc ID 1 20 1
TEXT 144 -16 Left 0 !.save v(rds)
TEXT 144 -48 LEFT 0 .param3 vgs=10
TEXT -56 -88 LEFT 0 .model IRF1234 VDMOS(Rg=3 Vto=2  Rd=20.0m Rs=8.0m Kp=30 lambda=0.01 Cgdmax=1.1n Cgdmin=0.28n Cgs=1.9n Cjo=0.56n Rb=10m N=1 Is=10p tt=60n mfg=International_Rectifier Vds=30 Ron=40m Qg=28n)

