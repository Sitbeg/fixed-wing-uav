[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 3 {524292,0,"V(thot)/1V"} {589827,0,"V(tcold)/1V"} {524290,0,"V(text)/1V"}
      Parametric: "Iqc*1V"
      X: (' ',0,0,10,100)
      Y[0]: (' ',0,-40,10,90)
      Y[1]: (' ',0,1e+308,10,-1e+308)
      Units: "" (' ',0,0,0,-40,10,90)
      Log: 0 0 0
      GridStyle: 1
      Text: "" 8 (85.2678571428572,24.6092184368737) ;Environment
      Text: "" 4 (14.5833333333333,45.190380761523) ;Hot side
      Text: "" 3 (12.5,-5.35070140280561) ;Cold side
      Text: "" 1 (93.75,-35.0501002004008) ;Heat,W
      Text: "" 1 (11.6071428571429,84.7895791583166) ;Temperature,C
   }
}
