Version 4
SymbolType BLOCK
LINE Normal 64 -8 -64 -8
LINE Normal 23 31 -19 31
LINE Normal 11 24 23 31
LINE Normal 11 38 23 31
LINE Normal 23 -43 -21 -43
LINE Normal 11 -50 23 -43
LINE Normal 11 -36 23 -43
RECTANGLE Normal 64 80 -64 -96
TEXT -1 21 Bottom 2 Heat
TEXT -2 -63 Bottom 2 Current
WINDOW 3 1 -96 Bottom 2
WINDOW 39 -2 80 Top 2
SYMATTR Value Se=53m R0=1.8
SYMATTR SpiceLine Cq=15 Rq=1.8
PIN -64 -48 LEFT 8
PINATTR PinName p
PINATTR SpiceOrder 1
PIN 64 -48 RIGHT 8
PINATTR PinName n
PINATTR SpiceOrder 2
PIN -64 32 LEFT 8
PINATTR PinName Tc
PINATTR SpiceOrder 3
PIN 64 32 RIGHT 8
PINATTR PinName Th
PINATTR SpiceOrder 4
