[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {589828,0,"V(in)"} {524291,0,"V(out)"}
      X: ('m',0,0,0.001,0.01)
      Y[0]: (' ',1,-0.2,0.2,1.8)
      Y[1]: (' ',0,1e+308,20,-1e+308)
      Volts: (' ',0,0,1,-0.2,0.2,1.8)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
      Representation: 1
   }
}
