[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(in)"} {524291,0,"-V(out)"}
      X: ('m',0,0,0.001,0.012)
      Y[0]: (' ',1,-0.3,0.1,1.1)
      Y[1]: (' ',0,1e+308,30,-1e+308)
      Volts: (' ',0,0,1,-0.3,0.1,1.1)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
