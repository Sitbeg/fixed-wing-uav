Version 4
SHEET 1 1932 996
WIRE 32 112 -96 112
WIRE 80 112 32 112
WIRE 160 112 80 112
WIRE 320 112 240 112
WIRE 80 144 80 112
WIRE 320 144 320 112
WIRE -96 176 -96 112
WIRE 320 288 320 224
WIRE 352 288 320 288
WIRE 400 288 352 288
WIRE 1184 288 480 288
WIRE -96 336 -96 256
WIRE 80 432 80 224
WIRE 144 432 80 432
WIRE 560 432 144 432
WIRE 992 432 928 432
WIRE 928 464 928 432
WIRE 560 480 560 432
WIRE 720 480 560 480
WIRE 848 480 800 480
WIRE 880 480 848 480
WIRE 80 496 80 432
WIRE 992 528 992 432
WIRE 1184 528 1184 288
WIRE 1184 528 992 528
WIRE 1248 528 1184 528
WIRE 320 560 320 288
WIRE 880 560 880 528
WIRE 880 560 320 560
WIRE 928 576 928 544
WIRE 80 640 80 576
FLAG -96 336 0
FLAG 80 640 0
FLAG 928 576 0
FLAG 1248 528 OUT
IOPIN 1248 528 Out
FLAG 32 112 1
FLAG 144 432 4
FLAG 352 288 3
FLAG 320 112 2
FLAG 848 480 5
SYMBOL res 64 128 R0
SYMATTR InstName R2
SYMATTR Value {wc(10k,tol)}
SYMBOL res 64 480 R0
SYMATTR InstName R1
SYMATTR Value {wc(100k,tol)}
SYMBOL voltage -96 160 R0
SYMATTR InstName V1
SYMATTR Value 10
SYMBOL res 304 128 R0
SYMATTR InstName R3
SYMATTR Value {wc(10k,tol)}
SYMBOL res 384 304 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R4
SYMATTR Value {wc(100k,tol)}
SYMBOL e 928 448 R0
SYMATTR InstName E1
SYMATTR Value 1e5
SYMBOL voltage 704 480 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 -32 56 VBottom 2
SYMATTR InstName V2
SYMATTR Value {1m*(1-wc(1,1))}
SYMBOL res 144 128 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 0 56 VBottom 2
SYMATTR InstName R5
SYMATTR Value 1
TEXT -88 -32 Left 2 !.op
TEXT -88 0 Left 2 !.step param x 0 400 1
TEXT -88 -88 Left 2 !.param tol=0.001 ; 0.1%\n.param offset=1m
TEXT 680 376 Left 2 ;Ideal OPAMP with offset
TEXT -80 -288 Left 4 ;Monte Carlo Worst Case Simulation Of A Differential Amplifier
TEXT 512 144 Left 2 ;mc(10k,tol) varies from 10k*(1-tol) to 10k*(1+tol)\n1m*(1-mc(1,1)) varies from -1m to +1m
TEXT 208 728 Left 2 ;Hint:\nYou can easily make a histogram of the output voltage with DPLOT95.\n \n1. Export the data in the Waveform Viewer of LTspice\nFile -> Export  V(out)\n \n2. Start DPLOT95\nEither drag the file into the window or use  File->Open  (o D multiple columns)\n \n3. From the menu in DPLOT95: Generate -> Histogram  0.005
TEXT 304 -88 Left 2 !.meas max_ MAX V(out)\n.meas min_ MIN V(out)\n.meas x_max FIND x when V(out)=max_\n.meas x_min FIND x when V(out)=min_
TEXT -80 -240 Left 2 ;The results from MEASURE are in the log-file.\nView -> SPICE Error Log
TEXT 848 -216 Left 2 !.meas R1_max FIND abs((V(4))/I(R1)) WHEN V(out)=max_\n.meas R2_max FIND abs((V(1)-V(4))/I(R2)) WHEN V(out)=max_\n.meas R3_max FIND abs(V(2,3)/I(R3)) WHEN V(out)=max_\n.meas R4_max FIND abs(V(3,out)/I(R4)) WHEN V(out)=max_\n.meas V2_max FIND V(4,5) WHEN V(out)=max_
TEXT 848 -64 Left 2 !.meas R1_min FIND abs((V(4))/I(R1)) WHEN V(out)=min_\n.meas R2_min FIND abs((V(1)-V(4))/I(R2)) WHEN V(out)=min_\n.meas R3_min FIND abs(V(2,3)/I(R3)) WHEN V(out)=min_\n.meas R4_minx FIND abs(V(3,out)/I(R4)) WHEN V(out)=min_\n.meas V2_min FIND V(4,5) WHEN V(out)=min_
TEXT -88 -144 Left 2 !.function wc(nom,tol) if(x==0, nom, if(flat(0.5)>0,nom*(1+tol),nom*(1-tol)))
LINE Normal 640 672 640 352
LINE Normal 960 352 640 352
LINE Normal 1088 528 960 352
LINE Normal 960 672 1088 528
LINE Normal 640 672 960 672
