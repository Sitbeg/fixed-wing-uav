Version 4
SymbolType CELL
RECTANGLE Normal 384 144 -368 -128
RECTANGLE Normal 97 44 -104 -20
TEXT -2 -53 Center 0 PLD3L
TEXT -96 -4 Left 0 DEG - L : 120deg
TEXT -96 28 Left 0 DEG - H :  60deg
SYMATTR Description PLD Program
SYMATTR Prefix X
SYMATTR Value switch33
SYMATTR ModelFile switch33.lib
PIN -368 -64 LEFT 8
PINATTR PinName H1
PINATTR SpiceOrder 1
PIN -368 -16 LEFT 8
PINATTR PinName H2
PINATTR SpiceOrder 2
PIN -368 32 LEFT 8
PINATTR PinName H3
PINATTR SpiceOrder 3
PIN -368 80 LEFT 8
PINATTR PinName DEG
PINATTR SpiceOrder 4
PIN -304 144 BOTTOM 8
PINATTR PinName _INT
PINATTR SpiceOrder 5
PIN 112 144 BOTTOM 8
PINATTR PinName PWM1
PINATTR SpiceOrder 6
PIN 384 -112 RIGHT 8
PINATTR PinName TA
PINATTR SpiceOrder 7
PIN 384 -64 RIGHT 8
PINATTR PinName TB
PINATTR SpiceOrder 8
PIN 384 -16 RIGHT 8
PINATTR PinName TC
PINATTR SpiceOrder 9
PIN 384 32 RIGHT 8
PINATTR PinName BA
PINATTR SpiceOrder 10
PIN 384 80 RIGHT 8
PINATTR PinName BB
PINATTR SpiceOrder 11
PIN 384 128 RIGHT 8
PINATTR PinName BC
PINATTR SpiceOrder 12
PIN -224 144 BOTTOM 8
PINATTR PinName DIR
PINATTR SpiceOrder 13
PIN -208 -128 TOP 8
PINATTR PinName RC2
PINATTR SpiceOrder 14
PIN -304 -128 TOP 8
PINATTR PinName RC12
PINATTR SpiceOrder 15
PIN -112 -128 TOP 8
PINATTR PinName RC22
PINATTR SpiceOrder 16
PIN -128 144 BOTTOM 8
PINATTR PinName _Fault
PINATTR SpiceOrder 17
PIN 192 -128 TOP 8
PINATTR PinName RC1
PINATTR SpiceOrder 18
PIN 96 -128 TOP 8
PINATTR PinName RC11
PINATTR SpiceOrder 19
PIN 288 -128 TOP 8
PINATTR PinName RC21
PINATTR SpiceOrder 20
PIN 224 144 BOTTOM 8
PINATTR PinName PWM2
PINATTR SpiceOrder 21
