Version 4
SymbolType CELL
LINE Normal 16 -32 32 0
LINE Normal 32 0 16 32
LINE Normal -16 -32 16 -32
LINE Normal -16 32 -16 -32
LINE Normal 16 32 -16 32
LINE Normal 49 0 32 0
TEXT -4 0 Left 0 C
WINDOW 123 2 -52 Center 0
SYMATTR Prefix X
SYMATTR SpiceModel CONST
SYMATTR Value2 c=0
SYMATTR ModelFile BLDCSupport.lib
PIN 48 0 NONE 8
PINATTR PinName OUT
PINATTR SpiceOrder 1
