[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"60*V(rps)/(2V*Pi)"}
      X: ('m',0,0,0.01,0.1)
      Y[0]: ('K',1,0,300,3600)
      Y[1]: ('K',1,1e+308,400,-1e+308)
      Units: "" ('K',0,0,1,0,300,3600)
      Log: 0 0 0
      GridStyle: 1
   }
}
