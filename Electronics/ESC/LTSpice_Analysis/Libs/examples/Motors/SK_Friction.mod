* D:\Simulation\BLDC Motor\LTspice\SK_FRICTION.asy
* Modified LTSpice Netlist Created : 2/19/2011
* ---------------------------------------------
* Description:  Start/Kynetic Friction
* ---------------------------------------------
.subckt SK_Friction w Tx Tf D
B0 Tf0 0 V=sgn(V(A))*(Tk+a/((b+abs(V(A)))*(1+k*abs(V(w)))))
.param b=0.1 k=0.05  kd=0.01 Tk=0.00001 Ts=0.00007
B1 Tf 0 V=if(abs(V(A))<abs(V(Tf0)),-V(A),-V(Tf0))
B2 A 0 V=V(Tx)-kd*V(D)
.param a=(Ts-Tk)*(b+Tk)
.ends SK_Friction
*
* End of Subcircuit Created from : SK_FRICTION.asc


