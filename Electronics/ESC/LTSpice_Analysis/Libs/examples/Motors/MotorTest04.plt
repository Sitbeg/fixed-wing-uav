[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 3 {524290,0,"V(a)"} {524291,0,"V(b)"} {524292,0,"V(c)"}
      X: ('m',0,0,0.003,0.033)
      Y[0]: (' ',0,-30,6,30)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,-30,6,30)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524293,0,"V(rps)/(2V*Pi)"}
      X: ('m',0,0,0.003,0.033)
      Y[0]: (' ',0,-20,10,130)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "" (' ',0,0,0,-20,10,130)
      Log: 0 0 0
      GridStyle: 1
   }
}
