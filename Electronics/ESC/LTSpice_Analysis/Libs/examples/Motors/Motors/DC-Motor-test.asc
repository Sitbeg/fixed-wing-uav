Version 4
SHEET 1 1036 680
WIRE 112 48 48 48
WIRE 288 48 112 48
WIRE 288 64 288 48
WIRE 48 96 48 48
WIRE 400 96 352 96
WIRE 400 160 352 160
WIRE 48 208 48 176
WIRE 288 208 288 192
FLAG 48 208 0
FLAG 112 48 1
FLAG 288 208 0
FLAG 400 96 RPM
IOPIN 400 96 Out
FLAG 400 160 POS
IOPIN 400 160 Out
SYMBOL voltage 48 80 R0
WINDOW 3 -25 167 Left 2
WINDOW 123 24 104 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value PWL(0m 0 1m 10 1000m 10 1010m 0 2000MS 0)
SYMATTR Value2 AC 1
SYMATTR InstName V_AMP
SYMBOL motor 288 128 R0
SYMATTR InstName X1
TEXT 48 -24 Left 2 !.tran 0 2 0 1m
TEXT 24 -96 Left 2 ;http://www.ecircuitcenter.com/Circuits/dc_motor_model/DCmotor_model.htm
TEXT 496 160 Left 2 ;position
TEXT 504 96 Left 2 ;speed
TEXT 24 -64 Left 2 ;Right-mouse-click on the symbol to change parameters of the motor.
