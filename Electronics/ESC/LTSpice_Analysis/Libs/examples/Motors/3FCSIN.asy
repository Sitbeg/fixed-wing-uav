Version 4
SymbolType CELL
RECTANGLE Normal -80 80 48 -96
TEXT -13 -85 Center 0 ANALsin
WINDOW 39 -16 -112 Center 0
SYMATTR SpiceLine Npp={Npp}
SYMATTR ModelFile BLDCSupport.lib
SYMATTR Description 3Phase Analog Sine Control
SYMATTR SpiceModel 3fcsin
SYMATTR Prefix X
PIN 48 -48 RIGHT 8
PINATTR PinName A
PINATTR SpiceOrder 1
PIN 48 0 RIGHT 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 48 48 RIGHT 8
PINATTR PinName C
PINATTR SpiceOrder 3
PIN -80 48 LEFT 8
PINATTR PinName Dir
PINATTR SpiceOrder 4
PIN -80 -48 LEFT 8
PINATTR PinName M
PINATTR SpiceOrder 5
PIN -16 80 VLEFT 8
PINATTR PinName Angle
PINATTR SpiceOrder 6
