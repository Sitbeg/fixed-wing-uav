Version 4
SymbolType BLOCK
LINE Normal -43 0 -64 0
LINE Normal -51 -8 -43 0
LINE Normal -51 8 -51 -8
LINE Normal -43 0 -51 8
LINE Normal 43 16 64 16
LINE Normal 51 8 43 16
LINE Normal 51 24 51 8
LINE Normal 43 16 51 24
LINE Normal 32 86 32 65
LINE Normal 40 78 32 86
LINE Normal 24 78 40 78
LINE Normal 32 86 24 78
LINE Normal -43 32 -64 32
LINE Normal -51 24 -43 32
LINE Normal -51 40 -51 24
LINE Normal -43 32 -51 40
RECTANGLE Normal 64 64 -64 -32
TEXT 0 -16 Left 0 Frict
WINDOW 3 0 -80 Center 0
WINDOW 123 0 -48 Center 0
SYMATTR Value Ts=0.3
SYMATTR Value2 Tk=0.1
SYMATTR ModelFile TK_FRICTION.mod
SYMATTR Description Start/Kynetic Friction
PIN 64 16 RIGHT 30
PINATTR PinName w
PINATTR SpiceOrder 1
PIN -64 0 LEFT 30
PINATTR PinName Tx
PINATTR SpiceOrder 2
PIN 32 64 BOTTOM 8
PINATTR PinName Tf
PINATTR SpiceOrder 3
PIN -64 32 LEFT 30
PINATTR PinName D
PINATTR SpiceOrder 4
