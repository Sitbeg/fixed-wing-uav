* C:\SwCAD Examples\BLDC Motor\LTspice\Group\DIRECTION.asy
* Modified LTSpice Netlist Created : 9/23/2009
* ---------------------------------------------
* Description: 
* ---------------------------------------------
.subckt direction IN OUT
B1 OUT 0 V=if (V(Delay)>0,5,0)
XU1 IN N001 Limit LIMIT=1 k=1
A1 N001 0 0 0 0 0 Delay 0 SCHMITT Vhigh=1 Vlow=-1 Vh={HYST}
.param HYST=0.1
.ends direction
*
.lib BLDCSupport.lib
* End of Subcircuit Created from : DIRECTION.asc


