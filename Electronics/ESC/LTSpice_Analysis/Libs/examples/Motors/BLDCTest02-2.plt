[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {68157443,0,"Ix(U1:A)"} {589826,1,"60*V(rps)/(2V*Pi)"}
      X: ('m',0,0,0.006,0.06)
      Y[0]: ('K',1,-400,200,1800)
      Y[1]: ('K',1,-400,400,4000)
      Amps: ('K',0,0,1,-400,200,1800)
      Units: "" ('K',0,0,1,-400,400,4000)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 3 {524292,0,"V(a)*Ix(U1:A)+V(b)*Ix(U1:B)+V(c)*Ix(U1:C)"} {589836,0,"1A*V(rps)*V(telmg)/1V"} {589834,1,"360deg*V(angle)/(2V*Pi)"}
      X: ('m',0,0,0.006,0.06)
      Y[0]: ('K',0,-80000,20000,120000)
      Y[1]: (' ',0,-80,40,320)
      Units: "W" ('K',0,0,0,-80000,20000,120000)
      Units: "�" (' ',0,0,0,-80,40,320)
      Log: 0 0 0
      GridStyle: 1
   }
}
