Version 4
SHEET 1 1300 680
WIRE 272 128 144 128
WIRE 304 128 272 128
WIRE 304 144 304 128
WIRE 96 208 16 208
WIRE 16 224 16 208
WIRE 176 272 144 272
WIRE 224 272 176 272
WIRE 144 288 144 272
FLAG 144 224 0
FLAG 16 304 0
FLAG 304 224 0
FLAG 224 352 0
FLAG 176 272 Rd
FLAG 272 128 d
FLAG 144 352 0
SYMBOL voltage 16 208 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 4
SYMBOL voltage 304 128 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value PWL(0 0 1 12)
SYMBOL bi 224 352 M180
WINDOW 0 41 49 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName B1
SYMATTR Value I=ddt(v(d))/ddt(-i(v2)) Rpar=1
SYMBOL cap 128 288 R0
SYMATTR InstName C1
SYMATTR Value 100n
SYMBOL nmos 96 128 R0
SYMATTR InstName M1
SYMATTR Value NM
TEXT -16 328 Left 2 !.tran 1
TEXT 376 304 Left 2 !.option plotwinsize=0
TEXT 416 72 Left 2 !.meas tran Rdmax  max V(Rd) from 0 to 1
TEXT 416 120 Left 2 !.meas tran Tsat  when  V(Rd)=Rdmax*0.5 cross=1
TEXT 416 160 Left 2 !.meas tran Vsat  find V(d) at Tsat
TEXT 88 -80 Left 2 ;The results of the .MEASURE commands are in the SPICE Error Log file\n \nView -> SPICE Error Log
TEXT 664 224 Left 2 !.model NM VDMOS Vto=2 kp=200u  Lambda=10m
TEXT 392 16 Left 3 ;Teoretical   Vsat=Vgate-Vto=4-2=2V
