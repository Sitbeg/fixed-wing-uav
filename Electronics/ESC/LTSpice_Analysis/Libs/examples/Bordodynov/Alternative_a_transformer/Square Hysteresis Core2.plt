[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(b)"}
      Parametric: "v(h)"
      X: (' ',0,-70,10,70)
      Y[0]: ('m',0,-0.6,0.1,0.6)
      Y[1]: ('�',0,-1.5e-005,3e-006,1.5e-005)
      Volts: ('m',0,0,1,-0.6,0.1,0.6)
      Amps: ('�',0,0,0,-1.5e-005,3e-006,1.5e-005)
      Log: 0 0 0
   }
}
