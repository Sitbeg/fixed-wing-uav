[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(b)"}
      Parametric: "V(h)"
      X: (' ',0,-120,20,100)
      Y[0]: ('m',0,-0.18,0.03,0.15)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.18,0.03,0.15)
      Log: 0 0 0
      GridStyle: 1
   }
}
