Version 4
SHEET 1 1836 680
WIRE 272 32 256 32
WIRE 272 80 256 80
WIRE 144 112 -32 112
WIRE 272 320 256 320
WIRE 272 368 256 368
WIRE 144 400 144 208
FLAG -32 192 0
FLAG 208 16 0
FLAG 208 224 0
FLAG 272 32 H
FLAG 272 80 B
FLAG 144 496 0
FLAG 208 304 0
FLAG 208 512 0
FLAG 272 320 H2
FLAG 272 368 B2
SYMBOL SBORKA\\TRANSFORM\\Winding 224 112 R0
SYMATTR Value2 N={N1}
SYMATTR InstName U1
SYMBOL current -32 192 R180
WINDOW 0 24 80 Left 2
WINDOW 3 -214 -81 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I2
SYMATTR Value SINE(0 -5m  5k 0 -500)
SYMBOL SBORKA\\TRANSFORM\\CoreSquareLoop2 224 32 R0
WINDOW 38 57 29 Left 2
SYMATTR InstName U2
SYMATTR SpiceLine2 P=0.01  mUmax=1000000
SYMBOL SBORKA\\TRANSFORM\\Winding 224 400 R0
SYMATTR Value2 N={N1}
SYMATTR InstName U3
SYMBOL SBORKA\\TRANSFORM\\CoreSquareLoop2 224 320 R0
WINDOW 38 78 32 Left 2
SYMATTR InstName U4
SYMATTR SpiceLine2 P=0.01  mUmax=1000000
SYMATTR SpiceModel CoreSquareLoop3
TEXT 544 344 Left 2 !.tran 0 3m .001u
TEXT 544 160 Left 2 !.param  N1=100 Area=24u Len=32m
TEXT 544 224 Left 2 !.option  plotwinsize=0
TEXT 528 272 Left 2 !.param mU0=Pi*4e-7 mUmax=1000000  Bs=0.57
