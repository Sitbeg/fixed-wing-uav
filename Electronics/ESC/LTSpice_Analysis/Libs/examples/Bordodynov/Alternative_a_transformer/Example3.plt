[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(vcc,out)"} {524291,0,"V(b1)"}
      X: ('m',1,0,0.0001,0.0015)
      Y[0]: (' ',0,-14,2,8)
      Y[1]: ('m',0,1e+308,0.004,-1e+308)
      Volts: (' ',0,0,0,-14,2,8)
      Log: 0 0 0
      GridStyle: 1
   }
}
