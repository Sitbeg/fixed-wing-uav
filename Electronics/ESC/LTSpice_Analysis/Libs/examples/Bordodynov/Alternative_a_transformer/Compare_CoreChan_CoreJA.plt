[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(b)"} {524291,0,"V(b1)"}
      Parametric: "v(H)"
      X: ('K',1,-1000,200,800)
      Y[0]: ('m',0,-0.54,0.09,0.54)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.54,0.09,0.54)
      Log: 0 0 0
      GridStyle: 1
   }
}
