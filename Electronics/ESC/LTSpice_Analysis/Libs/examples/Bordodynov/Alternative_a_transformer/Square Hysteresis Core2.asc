Version 4
SHEET 1 1836 680
WIRE 272 32 256 32
WIRE 272 80 256 80
WIRE 144 112 -32 112
WIRE 944 112 896 112
WIRE 976 112 944 112
WIRE 896 160 896 112
WIRE 912 368 864 368
WIRE 944 368 912 368
WIRE 864 416 864 368
FLAG 144 208 0
FLAG -32 192 0
FLAG 208 16 0
FLAG 208 224 0
FLAG 272 32 H
FLAG 272 80 B
FLAG 896 240 0
FLAG 944 112 DeltaB
FLAG 864 496 0
FLAG 912 368 R_air
SYMBOL SBORKA\\TRANSFORM\\Winding 224 112 R0
SYMATTR Value2 N={N1}
SYMATTR InstName U1
SYMBOL current -32 192 R180
WINDOW 0 24 80 Left 2
WINDOW 3 -214 -81 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I2
SYMATTR Value SINE(0 -5m  5k 0 -500)
SYMBOL SBORKA\\TRANSFORM\\CoreSquareLoop2 224 32 R0
SYMATTR InstName U2
SYMATTR SpiceLine2 P=0.01  mUmax=1000000
SYMBOL voltage 896 144 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value {mU0*35}
SYMBOL voltage 864 400 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value {Len/Area/mU0}
TEXT 328 200 Left 2 !.tran 0 3m .001u
TEXT 328 16 Left 2 !.param  N1=100 Area=24u Len=32m
TEXT 328 80 Left 2 !.option  plotwinsize=0 numdgt=10 reltol=1u abstol=1f vntol=10n Gmin=1f
TEXT 312 128 Left 2 !.param mU0=Pi*4e-7 mUmax=1000000  Bs=0.57
TEXT 240 256 Left 2 !.meas tran b1 find   V(b)  at=2.46m
TEXT 240 288 Left 2 !.meas tran b2  find V(b)  at=2.49m
TEXT 240 328 Left 2 !.meas tran h1 find   V(h) at=2.46m
TEXT 248 360 Left 2 !.meas tran h2 find  V(h)  at=2.49m
TEXT 248 400 Left 2 !.meas tran mUsat param  (B2-B1)/((H2-H1)*pi*4e-7)
