[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(b2)"} {524291,0,"V(b)"}
      Parametric: "v(h)"
      X: (' ',0,-250,50,250)
      Y[0]: ('m',0,-0.6,0.1,0.6)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.6,0.1,0.6)
      Log: 0 0 0
      GridStyle: 1
   }
}
