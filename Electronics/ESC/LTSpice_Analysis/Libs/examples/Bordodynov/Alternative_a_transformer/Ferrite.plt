[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(b)"}
      Parametric: "V(h)"
      X: (' ',0,-200,40,200)
      Y[0]: ('m',0,-0.4,0.08,0.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.4,0.08,0.4)
      Log: 0 0 0
   }
}
