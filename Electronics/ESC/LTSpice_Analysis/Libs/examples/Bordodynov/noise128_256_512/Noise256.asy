Version 4
SymbolType CELL
RECTANGLE Normal -16 -24 32 24
WINDOW 0 8 -24 Bottom 2
WINDOW 3 8 24 Top 2
WINDOW 39 8 48 Top 2
SYMATTR Value Noise256
SYMATTR SpiceLine TIM=1m MAG=1
SYMATTR Prefix X
SYMATTR ModelFile noise128_256_512.lib
PIN 32 0 RIGHT 8
PINATTR PinName out
PINATTR SpiceOrder 1
