Version 4
SymbolType BLOCK
LINE Normal 32 24 -31 24
LINE Normal 32 40 -31 40
LINE Normal 0 24 0 16
LINE Normal 0 41 0 48
LINE Normal 0 40 0 41
RECTANGLE Normal -96 -32 -64 96
CIRCLE Normal 64 -32 -64 96
WINDOW 0 8 -40 Bottom 2
PIN 0 -32 TOP 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 0 96 BOTTOM 8
PINATTR PinName -
PINATTR SpiceOrder 2
