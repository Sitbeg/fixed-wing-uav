[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {34668548,0,"I(R2)"}
      X: (' ',0,-5,1,5)
      Y[0]: (' ',1,-1,0.2,1.2)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,1,-1,0.2,1.2)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
