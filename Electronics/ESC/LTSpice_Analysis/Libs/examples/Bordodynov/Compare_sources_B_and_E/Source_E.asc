Version 4
SHEET 1 1380 680
FLAG 400 144 0
FLAG 400 64 Out
FLAG -16 128 0
FLAG -16 48 x
FLAG -16 272 0
FLAG -16 192 y
FLAG -16 416 0
FLAG -16 336 z
SYMBOL Misc\\Epoly 400 48 R0
SYMATTR InstName E1
SYMATTR Value POLY(3) x 0 y 0 z 0 0 {a} {a} {a}
SYMBOL voltage -16 32 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value PULSE(0 1 0 10n 10n 1m 2m)
SYMBOL voltage -16 176 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value PULSE(0 1 0 10n 10n 2m 4m)
SYMBOL voltage -16 320 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V3
SYMATTR Value PULSE(0 1 0 10n 10n 4m 8m)
TEXT 160 0 Left 2 !.param A=5
TEXT 488 8 Left 2 !.tran 0 80m 0 100n
TEXT 400 232 Left 2 ;--- Expanded Netlist ---\nb�e1 out 0 v=5*v(x)+5*v(y)+5*v(z)\nv1 x 0 pulse(0 1 0 10n 10n 1m 2m)\nv2 y 0 pulse(0 1 0 10n 10n 2m 4m)\nv3 z 0 pulse(0 1 0 10n 10n 4m 8m)\n.tran 0 80m 0 1u\n.end\nTotal elapsed time: 16.735 seconds.
TEXT 832 256 Left 2 ;vs b1 out 0 v=5 *(v(x)+v(y)+v(z))
