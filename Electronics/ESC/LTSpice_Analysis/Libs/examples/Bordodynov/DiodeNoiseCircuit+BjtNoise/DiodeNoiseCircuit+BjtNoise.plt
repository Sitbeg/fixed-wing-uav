[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(onoise)"}
      X: ('G',0,1,0,1e+009)
      Y[0]: ('p',0,0,3e-012,3.3e-011)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('p',0,0,0,0,3e-012,3.3e-011)
      Log: 1 0 0
      GridStyle: 1
   }
}
