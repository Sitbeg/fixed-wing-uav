[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(bf)"}
      Parametric: "ic(q1)"
      X: ('m',0,0,0.002,0.02)
      Y[0]: (' ',0,270,30,600)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,270,30,600)
      Log: 0 0 0
   }
}
