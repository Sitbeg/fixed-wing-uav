[AC Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {65540,0,"im(V(L))/(2*pi*freq)*1Hz/1V"}
      X: ('M',0,1000,0,1e+006)
      Y[0]: ('m',1,0,0.0001,0.0011)
      Y[1]: (' ',2,179.8,0.04,180.2)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
   },
   {
      traces: 1 {589827,0,"V(r)/1A"}
      X: ('M',0,1000,0,1e+006)
      Y[0]: (' ',0,0,3,33)
      Y[1]: ('m',2,-0.001,0.0002,0.001)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
   }
}
