[AC Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {589830,0,"V(ab)"} {524291,0,"V(as)"}
      X: ('M',0,1,0,1e+007)
      Y[0]: (' ',0,0.0316227766016838,3,1.99526231496888)
      Y[1]: (' ',0,-240,20,0)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 2 {2,0,"abs(V(As)/v(ia,As))"} {4,0,"abs(V(Ab)/v(is,AB))"}
      X: ('M',0,1,0,1e+007)
      Y[0]: (' ',0,0.0316227766016838,10,316227.766016838)
      Y[1]: ('m',1,-0.0014,0.0002,0.0014)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
