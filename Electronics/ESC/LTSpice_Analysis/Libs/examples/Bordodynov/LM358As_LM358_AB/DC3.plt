[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(os)"} {524291,0,"V(oa)"}
      X: ('m',0,1e-006,0.004,0.0406)
      Y[0]: (' ',0,4,1,14)
      Y[1]: ('m',0,1e+308,0.004,-1e+308)
      Volts: (' ',0,0,0,4,1,14)
      Log: 0 0 0
      GridStyle: 1
   }
}
