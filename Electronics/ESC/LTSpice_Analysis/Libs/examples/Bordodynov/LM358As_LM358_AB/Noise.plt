[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(onoise)"}
      X: ('K',0,1,0,100000)
      Y[0]: ('n',0,3.2e-008,8e-009,1.2e-007)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('n',0,0,0,3.2e-008,8e-009,1.2e-007)
      Log: 1 0 0
      GridStyle: 1
   }
}
