[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524291,0,"i1"}
      Parametric: "v(x)"
      X: (' ',0,0,3,33)
      Y[0]: ('_',0,1e-008,0,0.1)
      Y[1]: ('m',0,1e+308,0.001,-1e+308)
      Amps: ('_',1,0,0,1e-008,0,0.1)
      Log: 0 1 0
      GridStyle: 1
   }
}
