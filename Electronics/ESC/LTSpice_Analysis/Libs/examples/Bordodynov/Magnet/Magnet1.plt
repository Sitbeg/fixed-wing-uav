[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(b)"}
      Parametric: "v(H)"
      X: (' ',0,-20,4,20)
      Y[0]: ('m',0,-0.24,0.04,0.24)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.24,0.04,0.24)
      Log: 0 0 0
   }
}
