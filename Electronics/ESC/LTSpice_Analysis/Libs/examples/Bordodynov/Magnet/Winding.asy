Version 4
SymbolType CELL
LINE Normal 0 24 0 41
CIRCLE Normal -25 -20 -34 -11
ARC Normal -31 16 -64 -16 -48 16 -48 -16
ARC Normal -31 48 -64 16 -48 48 -48 16
ARC Normal -31 80 -64 48 -48 80 -48 48
WINDOW 0 -16 -40 Bottom 2
WINDOW 39 -99 18 Top 2
SYMATTR SpiceLine N=1
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR Value2 Lser=1n Rser=1m Cpar=1p
SYMATTR SpiceModel Winding
PIN -48 -16 NONE 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -48 80 NONE 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 0 32 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 3
