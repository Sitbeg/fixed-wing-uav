[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {268959746,0,"V(b)"}
      Parametric: "v(H)"
      X: ('m',0,-0.015,0.003,0.018)
      Y[0]: ('�',0,-6e-006,2e-006,1.8e-005)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('�',0,0,0,-6e-006,2e-006,1.8e-005)
      Log: 0 0 0
   },
   {
      traces: 2 {524291,0,"V(3)"} {34603012,1,"I(I2)"}
      Parametric: "v(H)"
      X: ('m',0,-0.015,0.003,0.018)
      Y[0]: ('m',0,-0.6,0.1,0.6)
      Y[1]: (' ',0,-600,100,600)
      Volts: ('m',0,0,0,-0.6,0.1,0.6)
      Amps: (' ',0,0,0,-600,100,600)
      Log: 0 0 0
   }
}
