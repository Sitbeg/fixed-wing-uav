Version 4
SHEET 1 3264 680
WIRE 144 128 144 112
WIRE 144 144 144 128
FLAG 144 208 0
FLAG 144 32 0
FLAG 144 128 X
SYMBOL diode 160 208 R180
WINDOW 0 51 53 Left 2
WINDOW 3 34 21 Left 2
SYMATTR InstName D1
SYMATTR Value 1N3993A
SYMBOL current 144 32 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I1
SYMATTR Value 1m
TEXT 224 72 Left 2 !.model 1N3993A d is=.1n BV=3.645 Ibv=640m Rs=0.4\n+ Xti=3 Eg=1.11 Cjo=2n M=.6 Vj=.75 Fc=.5   tt=1u\n+ Isr=10n Nr=2 nbv=13.5 Ibvl=1m Nbvl=35 tbv1=-2.34m
TEXT 288 32 Left 2 !.dc dec i1 100u 1 100
TEXT 224 184 Left 2 !.meas dc v1m find v(x) at 1mA\n.meas dc v1_1m find v(x) at 1.1mA\n.meas Rd1mA param (v1_1m-v1m)/0.1mA\n.meas dc v640m find v(x) at 640mA\n.meas dc v641m find v(x) at 641mA\n.meas Rd640mA param (v641m-v640m)/1mA
TEXT 752 184 Left 2 ;v1m: v(x)=1.36654 at 0.001\nv1_1m: v(x)=1.40092 at 0.0011\nrd1ma: (v1_1m-v1m)/0.1ma=343.791\nv640m: v(x)=3.90044 at 0.64\nv641m: v(x)=3.90139 at 0.641\nrd640ma: (v641m-v640m)/1ma=0.947566
