[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(onoise)"}
      X: ('K',0,10,0,100000)
      Y[0]: ('n',0,3e-008,3e-009,9e-008)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('n',0,0,0,3e-008,3e-009,9e-008)
      Log: 1 0 0
      GridStyle: 1
   }
}
