Version 4
SymbolType BLOCK
LINE Normal 64 32 -32 32
LINE Normal 11 -4 -31 -4
LINE Normal 64 -4 22 -4
LINE Normal -9 -16 -9 -4
LINE Normal -48 -16 -9 -16
LINE Normal 41 -16 41 -4
LINE Normal 80 -16 41 -16
LINE Normal 16 32 16 48
RECTANGLE Normal 64 4 -32 23
WINDOW 0 8 -40 Bottom 2
PIN -48 -16 BOTTOM 8
PINATTR PinName in
PINATTR SpiceOrder 1
PIN 16 48 RIGHT 8
PINATTR PinName COM
PINATTR SpiceOrder 2
PIN 80 -16 BOTTOM 8
PINATTR PinName OUT
PINATTR SpiceOrder 3
