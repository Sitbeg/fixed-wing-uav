[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(rx)/1A"}
      X: ('m',0,1e-011,0,0.025)
      Y[0]: ('_',0,10,0,1e+008)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "ohm" ('_',1,0,0,10,0,1e+008)
      Log: 1 1 0
      GridStyle: 1
   }
}
