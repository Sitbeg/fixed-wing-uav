Version 4
SymbolType CELL
LINE Normal 0 64 0 -47
LINE Normal -48 -32 -64 -32
LINE Normal -48 64 -64 64
LINE Normal 64 -32 48 -32
LINE Normal 64 64 48 64
LINE Normal 16 -63 0 -47
LINE Normal 32 -63 16 -63
ARC Normal -64 0 -32 -32 -48 0 -48 -32
ARC Normal -64 32 -32 0 -48 32 -48 0
ARC Normal -64 64 -32 32 -48 64 -48 32
ARC Normal 63 0 31 -32 47 -32 47 0
ARC Normal 63 32 31 0 47 0 47 32
ARC Normal 63 64 31 32 47 32 47 64
SYMATTR SpiceModel Mutual_Ind
SYMATTR SpiceLine L1=1m L2=4m
SYMATTR SpiceLine2 r1=1m r2=1m
SYMATTR Description Only    .TRAN
SYMATTR Prefix x
PIN 32 -64 BOTTOM 8
PINATTR PinName k
PINATTR SpiceOrder 1
PIN -64 -32 NONE 8
PINATTR PinName p1
PINATTR SpiceOrder 2
PIN -64 64 NONE 8
PINATTR PinName p2
PINATTR SpiceOrder 3
PIN 64 -32 NONE 8
PINATTR PinName s1
PINATTR SpiceOrder 4
PIN 64 64 NONE 8
PINATTR PinName s2
PINATTR SpiceOrder 5
