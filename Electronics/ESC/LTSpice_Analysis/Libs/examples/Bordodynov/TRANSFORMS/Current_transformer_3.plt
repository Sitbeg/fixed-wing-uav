[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524291,0,"V(b)"}
      Parametric: "V(H)"
      X: (' ',0,-120,20,120)
      Y[0]: ('m',0,-0.42,0.07,0.42)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.42,0.07,0.42)
      Log: 0 0 0
   }
}
