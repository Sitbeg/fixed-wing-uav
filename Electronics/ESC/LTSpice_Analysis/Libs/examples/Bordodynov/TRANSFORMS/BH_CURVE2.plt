[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {589830,0,"V(b)"}
      Parametric: "V(H)"
      X: (' ',0,-240,40,160)
      Y[0]: ('m',0,-0.4,0.08,0.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.4,0.08,0.4)
      Log: 0 0 0
      GridStyle: 1
   }
}
