[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"Ic(Q1)/ib(q1)"}
      X: (' ',1,0.02,0.2,2)
      Y[0]: (' ',0,145,5,200)
      Y[1]: (' ',0,1e+308,10,-1e+308)
      Units: "" (' ',0,0,0,145,5,200)
      Log: 0 0 0
   }
}
