[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {589830,0,"V(b)"}
      Parametric: "v(h)"
      X: (' ',0,-30,6,30)
      Y[0]: ('m',0,-0.2,0.04,0.24)
      Y[1]: ('m',0,1e+308,0.002,-1e+308)
      Volts: ('m',0,0,0,-0.2,0.04,0.24)
      Log: 0 0 0
      GridStyle: 1
   }
}
