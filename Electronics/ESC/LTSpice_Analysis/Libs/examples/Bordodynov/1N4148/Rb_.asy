Version 4
SymbolType CELL
LINE Normal 48 0 0 0
LINE Normal 29 0 15 -8
LINE Normal 15 7 29 0
SYMATTR Prefix X
SYMATTR SpiceModel Rb_
SYMATTR SpiceLine2 Tau=80n K=1000
SYMATTR SpiceLine Rmax=140
PIN 0 0 NONE 8
PINATTR PinName P
PINATTR SpiceOrder 1
PIN 48 0 NONE 8
PINATTR PinName M
PINATTR SpiceOrder 2
