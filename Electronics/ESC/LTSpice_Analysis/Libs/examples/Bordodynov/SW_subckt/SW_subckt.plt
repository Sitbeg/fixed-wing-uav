[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('m',1,0,0.0005,0.005)
      Y[0]: (' ',1,-0.3,0.3,3.6)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-0.3,0.3,3.6)
      Log: 0 0 0
      GridStyle: 1
   }
}
[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('M',0,1,0,2e+008)
      Y[0]: (' ',0,0.0177827941003892,7,281.838293126445)
      Y[1]: (' ',0,-450,30,-90)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
