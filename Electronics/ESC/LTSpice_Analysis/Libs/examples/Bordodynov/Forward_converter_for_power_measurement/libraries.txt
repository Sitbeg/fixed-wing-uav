* this is a collection of parts for the forward converter
*
.model STW11NM80 VDMOS(Rg=3 Vto=4.5 Rd=140m Rs=100m Rb=175m Kp=30 Cgdmax=.5n Cgdmin=.05n Cgs=2n Cjo=.3n Is=88p mfg=STMicroelectronics Vds=800 Ron=350m Qg=44n)
.model MR760 D(IS=5.86764e-08 RS=0.00170261 N=1.7374 EG=0.6 XTI=0.5 BV=1000 IBV=2.5e-05 CJO=1.33832e-09 VJ=0.4 M=0.708966 FC=0.5 TT=5.45339e-06 KF=0 AF=1 Iave=6 Vpk=1000 mfg=GI type=silicon)
.model MBR20100CT D(Is=10u Rs=.005 N=1.5 Ikf=.3 Isr=10u Nr=3 Cjo=1e-11 Vj=.7 Iave=10 Vpk=100 mfg=OnSemi type=Schottky)
