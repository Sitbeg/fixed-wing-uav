[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 2 {68157442,0,"Ix(U1:Drain)"} {34603011,0,"Id(M1)"}
      X: (' ',1,0,0.8,8)
      Y[0]: (' ',0,-1,1,12)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,0,-1,1,12)
      Log: 0 0 0
      GridStyle: 1
   }
}
