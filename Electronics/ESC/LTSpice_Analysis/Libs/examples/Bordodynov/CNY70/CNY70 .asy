Version 4
SymbolType CELL
LINE Normal -96 -48 -56 -48
LINE Normal -56 -16 -56 -48
LINE Normal -56 16 -56 48
LINE Normal -96 48 -56 48
LINE Normal -80 -16 -32 -16
LINE Normal -56 16 -32 -16
LINE Normal -56 16 -80 -16
LINE Normal -80 16 -32 16
LINE Normal 96 -48 72 -48
LINE Normal 32 0 72 -48
LINE Normal 32 0 68 36
LINE Normal 32 -28 32 28
LINE Normal 96 48 80 48
LINE Normal 80 48 64 40
LINE Normal 80 48 72 32
LINE Normal 64 40 72 32
LINE Normal 24 0 12 -4
LINE Normal 24 0 20 -12
LINE Normal 20 -4 24 0
LINE Normal 0 -64 0 16
LINE Normal -11 -52 -11 -16
LINE Normal -14 -43 -11 -52
LINE Normal -8 -43 -14 -43
LINE Normal -11 -52 -8 -43
LINE Normal -14 -26 -11 -16
LINE Normal -7 -26 -14 -26
LINE Normal -11 -16 -7 -26
RECTANGLE Normal -96 -64 96 64
ARC Normal -4 12 20 -12 16 -4 -4 0
ARC Normal -28 12 -4 -12 -28 4 -4 0
WINDOW 3 0 80 Center 2
WINDOW 0 -101 -92 Left 2
SYMATTR Value CNY70
SYMATTR Prefix X
SYMATTR ModelFile CNY70 .sub
SYMATTR SpiceLine d=0
SYMATTR SpiceLine2 rel_CTR0=0.05
PIN -96 -48 NONE 0
PINATTR PinName A
PINATTR SpiceOrder 1
PIN -96 48 NONE 0
PINATTR PinName K
PINATTR SpiceOrder 2
PIN 96 48 NONE 0
PINATTR PinName E
PINATTR SpiceOrder 3
PIN 96 -48 NONE 0
PINATTR PinName C
PINATTR SpiceOrder 4
PIN 0 -64 NONE 8
PINATTR PinName Gate
PINATTR SpiceOrder 5
