Version 4
SHEET 1 7232 3376
WIRE 304 -48 256 -48
WIRE 480 -48 304 -48
WIRE 480 -32 480 -48
WIRE 256 -16 256 -48
WIRE 192 32 160 32
WIRE 160 112 160 32
WIRE 256 112 256 80
FLAG 160 112 0
FLAG 480 48 0
FLAG 304 -48 C
SYMBOL npn 192 -16 R0
SYMATTR InstName Q1
SYMATTR Value KT872A
SYMBOL voltage 480 -48 R0
WINDOW 123 0 0 Left 2
WINDOW 39 24 124 Left 2
SYMATTR SpiceLine Rser=100
SYMATTR InstName V1
SYMATTR Value 10
TEXT 104 -144 Left 2 !.model KT872A npn Is=10p  bf=55 vaf=600 ikf=1.2 nk=612m  br=0.1 var=50 ikr=5 bvbe=7  ibvbe=10mA ise=5n ne=2  isc=3.3u\n+  nc=4  rb=1 irb=150m rbm=20m  rc=50m re=10m cjc=550p vjc=0.90 mjc=0.28 cje=4.7n vje=0.68 mje=0.33 fc=0.56 \n+  tf=14n itf=1 vtf=13.7 xtf=5.46 tr=1u gamma=30u qco=90n rco=2 vo=1k BVcbo=1500 nBVcbo=2.
TEXT 112 232 Left 2 !.dc v1 1000 1500 1
TEXT 656 -40 Left 2 !.meas Vcb when Ic(Q1)=1mA
TEXT 664 8 Left 2 ;vcb: ic(q1)=1ma AT 1499.15r
