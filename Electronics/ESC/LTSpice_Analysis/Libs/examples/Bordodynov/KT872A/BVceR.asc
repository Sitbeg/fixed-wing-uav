Version 4
SHEET 1 7232 3376
WIRE 304 -48 256 -48
WIRE 480 -48 304 -48
WIRE 480 -32 480 -48
WIRE 256 -16 256 -48
WIRE 1072 0 1040 0
WIRE 1104 0 1072 0
WIRE 192 32 144 32
WIRE 1040 32 1040 0
WIRE 256 112 256 80
FLAG 256 112 0
FLAG 480 48 0
FLAG 304 -48 C
FLAG 144 112 0
FLAG 1040 112 0
FLAG 1072 0 R
SYMBOL npn 192 -16 R0
SYMATTR InstName Q1
SYMATTR Value KT872A
SYMBOL voltage 480 -48 R0
WINDOW 123 0 0 Left 2
WINDOW 39 24 124 Left 2
SYMATTR SpiceLine Rser=1
SYMATTR InstName V1
SYMATTR Value 10
SYMBOL res 128 16 R0
SYMATTR InstName R1
SYMATTR Value {R}
SYMBOL voltage 1040 16 R0
WINDOW 123 0 0 Left 2
WINDOW 39 24 124 Left 2
SYMATTR InstName V2
SYMATTR Value {R}
TEXT 104 -144 Left 2 !.model KT872A npn Is=10p  bf=55 vaf=600 ikf=1.2 nk=612m  br=0.1 var=50 ikr=5 bvbe=7  ibvbe=10mA ise=5n ne=2  isc=3.3u\n+  nc=4  rb=1 irb=150m rbm=20m  rc=50m re=10m cjc=550p vjc=0.90 mjc=0.28 cje=4.7n vje=0.68 mje=0.33 fc=0.56 \n+  tf=14n itf=1 vtf=13.7 xtf=5.46 tr=1u gamma=30u qco=90n rco=2 vo=1k BVcbo=1500 nBVcbo=2.
TEXT 112 232 Left 2 !.dc v1 500 1510 100m
TEXT 656 -40 Left 2 !.meas Vce when Ic(Q1)=1m
TEXT 328 160 Left 2 !.step dec param R  10 100k 2
TEXT 680 8 Left 2 ;.step r=31.6228\n.step r=100\n.step r=316.228\n.step r=1000\n.step r=3162.28\n.step r=10000\n.step r=31622.8\n.step r=100000\n \n\nMeasurement: vce\n  step          ic(q1)=1m\n     1          1497.54\n     2          1497.56\n     3          1497.62\n     4          1496.6\n     5          1488.7\n     6          1464\n     7          1386.3\n     8          1152\n     9          703.7
