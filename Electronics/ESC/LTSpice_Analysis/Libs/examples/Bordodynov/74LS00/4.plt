[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524291,0,"V(2)"} {589828,0,"V(1)"}
      X: ('n',0,0,2e-008,2e-007)
      Y[0]: (' ',1,0,0.4,4.4)
      Y[1]: ('m',0,1e+308,0.002,-1e+308)
      Volts: (' ',0,0,1,0,0.4,4.4)
      Log: 0 0 0
      GridStyle: 1
   }
}
[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {589830,0,"V(2)"}
      X: (' ',1,0,0.2,2)
      Y[0]: (' ',1,0,0.4,4.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.4,4.4)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
