Version 4
SymbolType BLOCK
LINE Normal 0 -96 0 -53
LINE Normal 0 53 0 96
LINE Normal -80 0 -47 0
LINE Normal -47 0 -58 7
LINE Normal -47 0 -58 -7
LINE Normal 50 0 78 0
LINE Normal 78 0 66 7
LINE Normal 78 0 66 -7
RECTANGLE Normal 80 96 -80 -96
RECTANGLE Normal 9 -42 -10 -53
RECTANGLE Normal 10 53 -10 42
CIRCLE Normal 3 3 -3 -3
CIRCLE Normal 43 43 -43 -41
TEXT -58 18 VRight 2 Magn
TEXT -57 -17 VLeft 2 Field
TEXT 55 19 VRight 2 Load
TEXT 55 -11 VLeft 2 Speed
PIN 0 -96 NONE 8
PINATTR PinName p
PINATTR SpiceOrder 1
PIN 0 96 NONE 8
PINATTR PinName n
PINATTR SpiceOrder 2
PIN -80 0 NONE 8
PINATTR PinName MF
PINATTR SpiceOrder 3
PIN 80 0 NONE 8
PINATTR PinName Speed
PINATTR SpiceOrder 4
