Version 4
SymbolType BLOCK
LINE Normal 0 -96 0 -64
LINE Normal 0 64 0 96
LINE Normal 48 0 20 0
LINE Normal 36 -7 48 0
LINE Normal 36 7 48 0
RECTANGLE Normal 48 96 -27 -97
ARC Normal -16 -64 17 -32 1 -32 0 -63
ARC Normal -16 -33 17 -1 1 -1 0 -32
ARC Normal -16 -1 17 31 1 31 0 0
ARC Normal -17 32 16 64 0 64 -1 33
TEXT 33 17 VRight 2 Magn
TEXT 33 -15 VLeft 2 Field
PIN 0 -96 NONE 8
PINATTR PinName p
PINATTR SpiceOrder 1
PIN 0 96 NONE 8
PINATTR PinName n
PINATTR SpiceOrder 2
PIN 48 0 NONE 8
PINATTR PinName MF
PINATTR SpiceOrder 3
