[AC Analysis]
{
   Npanes: 1
   {
      traces: 3 {589830,0,"V(coss)"} {524291,0,"V(crss)"} {524292,0,"V(ciss)"}
      X: ('K',1,1,100,1000)
      Y[0]: ('_',1,1e-011,0,1e-008)
      Y[1]: (' ',1,88.5,0.1,90)
      Log: 0 1 0
      GridStyle: 1
      PltMag: 1
   }
}
