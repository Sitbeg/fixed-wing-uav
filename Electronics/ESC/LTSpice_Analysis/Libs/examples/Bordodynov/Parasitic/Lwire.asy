Version 4
SymbolType CELL
LINE Normal 48 0 0 0
SYMATTR Prefix X
SYMATTR SpiceModel Lwire
SYMATTR SpiceLine2 length=1 D=1m
SYMATTR ModelFile Parasitic.lib
PIN 0 0 NONE 8
PINATTR PinName n1
PINATTR SpiceOrder 1
PIN 48 0 NONE 8
PINATTR PinName n2
PINATTR SpiceOrder 2
