.subckt Rb P M params: Rmax=10 Tau=1u K=10
* Accounting for the modulation of the base resistance of the diode.
V1 P s 0
B1 s M I=V(s,M)/(Rmax*(1-tanh(k*v(q))))
B2 0 q I=i(v1) Rpar=1
C1 q 0 {Tau}
.ends Rb

.subckt Rwire n1 n2 length=1 D=0 S=0 N=1 ro=0.0175u
* Cu-0.0175u, Al=0.028u
R n1 n2 {ro*length*N/(0.7854*d*d+S)}
.ends Rwire
.Subckt Lwire N1 N2 length=1 D=1m
L1 N1 N2 {200n*length*(log(length/D)+0.389*D/length+0.636)}
.ends Lwire
.subckt Cparasitic 1 C=50F
C 1 0 {c}
.ends
.subckt Lparasitic n1 n2 L=4n Rs=1m Rp=1k
L n1 n2 {L} rser={Rs} Rpar={Rp}
.ends
.subckt resUS 1 2 R=1K Cp=0.1p Ls=1n Rmin=1
C 1 2 {Cp} Lser={Ls} Rpar={R} Rser={Rmin}
.ends

.Subckt R_Current_noise_dB R+ R- R=1K Vn_dB=-15 Cp=0.1p
.param IonVV=971*(10**(Vn_dB/20))**2
CR1 R+ Test {Cp} Rpar={R}
B1 N003 N001 I=v(R+,test)**2*IonVV
B2 Test R- V=I(VAnoise)
VAnoise N002 0 0
Q1 0 N002 N003 0 NPNn
Q2 0 N002 N001 0 PNPn
.MODEL pnpn pnp is=1f  bf=1 af=1 kf=1f 
.MODEL npnn npn is=1f  bf=1 af=1 kf=1f
.ends 

.Subckt R_Current_noise R+ R- R=1K Vn=1 Cp=0.1p
.param IonVV=971*Vn**2
CR1 R+ Test {Cp} Rpar={R}
B1 N003 N001 I=v(R+,test)**2*IonVV
B2 Test R- V=I(VAnoise)
VAnoise N002 0 0
Q1 0 N002 N003 0 NPNn
Q2 0 N002 N001 0 PNPn
.MODEL pnpn pnp is=1f  bf=1 af=1 kf=1f 
.MODEL npnn npn is=1f  bf=1 af=1 kf=1f
.ends 

.SUBCKT Skin a b  Rdc=1 Ldc=50u  Rac=10 Fac=100k
*
.PARAM l=Ldc*3 g=Pi*Fac*l/Rac**2
*
R1 a 2 {rdc}
L1 2 b {l/3}
R2 2 3 {5/g}
L2 3 b {l/7}
R3 3 4 {9/g}
L3 4 b {l/11}
R4 4 5 {13/g}
L4 5 b {l/15}
R5 5 6 {17/g}
L5 6 b {l/19}
R6 6 7  {21/g}
L6 7 b {l/23}
R7 7 8  {25/g}
L7 8 b {l/27}
R8 8 9  {29/g}
L8 8 b {l/31}
R9 9 10  {33/g}
L9 10 b {l/34}
.ends
