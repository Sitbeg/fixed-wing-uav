Version 4
SymbolType CELL
LINE Normal 48 0 0 0
SYMATTR Prefix X
SYMATTR SpiceModel Skin
SYMATTR SpiceLine2 Rdc=1 Ldc=50u  Rac=10 Fac=100k
SYMATTR ModelFile Parasitic.lib
SYMATTR Description Skin effect
PIN 0 0 NONE 8
PINATTR PinName n1
PINATTR SpiceOrder 1
PIN 48 0 NONE 8
PINATTR PinName n2
PINATTR SpiceOrder 2
