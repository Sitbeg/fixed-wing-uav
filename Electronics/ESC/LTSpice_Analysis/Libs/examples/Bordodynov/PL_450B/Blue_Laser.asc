Version 4
SHEET 1 1468 680
WIRE 208 96 208 -16
WIRE 288 128 256 128
WIRE 208 192 208 160
FLAG 208 192 0
FLAG 208 -96 0
FLAG 288 128 Plight
SYMBOL current 208 -96 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I1
SYMATTR Value 80mA
SYMBOL lazer3 208 96 R0
WINDOW 39 37 -49 Left 2
WINDOW 40 37 -20 Left 2
SYMATTR SpiceLine Pnom=80mW inom=100mA ith=30mA  Pth=10uW
SYMATTR SpiceLine2 cLd=10p  rsLd=18 nLd=7.5 isLD=1e-10 tauLD=1n
SYMATTR InstName U1
TEXT 304 -80 Left 2 !.dc i1 0 110mA 1mA
TEXT 488 -32 Left 3 ;PL 450B
TEXT 384 112 Left 2 !.Subckt Lazer3 com lk pw Pnom=1 inom=1200m ith=240m Pth=1m\n+ cLd=10p rsLd=1 nLd=2 isLD=1e-17 tauLD=1n\n.param k=(Pnom-Pth*inom/ith)/(inom-ith)\nc com lk {cLd}\nD1 5 lk0 dLd\nD10 com lk0 dLd2\nrs lk0 lk {rsLd}\nVAm1 com 5 0\nrut lk com 10Meg\nv1 ith 0 {ith}\nHled pwled 0 vam1 {2*Pth/ith}\nB1 pw pwled v=uramp(2*i(vam1)-v(ith))*k\n.model dLd D is={isLd/2} n={nLd} eg={nLd*1.11}\n.model dLd2 D is={isLd/2} n={nLd} eg={nLd*1.11} tt={tauLD*2}\n.ends Lazer3
