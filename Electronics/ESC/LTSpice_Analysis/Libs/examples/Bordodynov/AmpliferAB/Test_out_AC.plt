[AC Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {524292,0,"V(out)"} {524291,0,"V(in)"}
      X: ('M',0,1000,0,1e+007)
      Y[0]: (' ',2,0.08,0.08,1.04)
      Y[1]: (' ',0,-240,20,0)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 1 {589830,0,"v(out)/V(in,out)"}
      X: ('M',0,1000,0,1e+007)
      Y[0]: (' ',0,0,3,45)
      Y[1]: (' ',0,-150,10,0)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
