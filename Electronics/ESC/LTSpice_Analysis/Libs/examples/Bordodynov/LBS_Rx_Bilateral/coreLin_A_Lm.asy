Version 4
SymbolType CELL
LINE Normal 0 -16 0 48
WINDOW 0 -34 -6 Bottom 2
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR SpiceModel coreLin_A_Lm
SYMATTR Value2 A=14u Lm=31m Lg=0 mU=2000
SYMATTR SpiceLine2 Fe=10Meg
PIN 0 48 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 1
