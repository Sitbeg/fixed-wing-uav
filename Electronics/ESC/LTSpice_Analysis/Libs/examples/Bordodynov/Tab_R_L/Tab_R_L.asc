Version 4
SHEET 1 1368 680
WIRE 96 64 96 48
WIRE 96 128 96 64
WIRE 96 240 96 208
WIRE 96 352 96 320
FLAG 96 352 0
FLAG 96 -32 0
FLAG 96 64 Z
DATAFLAG 96 112 ""
DATAFLAG 96 224 ""
SYMBOL res 80 112 R0
SYMATTR InstName R1
SYMATTR Value R={10-1} Laplace=1/Fr(S)
SYMBOL current 96 -32 R0
WINDOW 123 24 108 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value2 AC 1
SYMATTR InstName I1
SYMATTR Value 1A
SYMBOL res 80 224 R0
SYMATTR InstName R2
SYMATTR Value R=1 Laplace=1/s/(FL(S))
TEXT 112 -96 Left 2 !.func FR(x) {tbl(abs(x)/6.283, 0,10, 1k,11, 10k,15, 30k,20, 100k,25)/(10-1)}
TEXT 112 -56 Left 2 !.func FL(x) {tbl(abs(x)/6.283, 0,3m, 1k,3.01m, 10k,3.1m, 30k,3.15m, 100k,3.2m)}
TEXT 528 160 Left 2 !.ac dec 100 1 100k
TEXT 240 56 Left 2 !.opt plotwinsize=0
TEXT 16 176 Left 4 ;R
TEXT 24 288 Left 4 ;L
TEXT 64 -160 Left 4 ;Plot re(V(z))/1A and im(V(z))/2/pi/freq*1Hz/1A/1Ohm
