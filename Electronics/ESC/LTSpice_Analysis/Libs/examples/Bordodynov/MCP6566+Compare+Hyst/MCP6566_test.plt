[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(vout)"} {524291,0,"V(vout_jt2)"}
      X: ('n',0,0,1e-008,9.5e-008)
      Y[0]: (' ',1,0,0.5,5.5)
      Y[1]: ('m',1,1e+308,0.0008,-1e+308)
      Volts: (' ',0,0,0,0,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
   }
}
