Version 4
SymbolType BLOCK
LINE Normal -12 -37 -20 -37
LINE Normal -16 -37 -12 -37
LINE Normal -16 -41 -16 -37
LINE Normal -16 -33 -16 -41
RECTANGLE Normal 48 48 -48 -48
TEXT -41 -1 Left 2 in
TEXT 36 1 Right 2 out
TEXT -20 -37 Left 2 vcc
TEXT -17 33 Left 2 vss
WINDOW 38 32 64 Left 2
WINDOW 0 32 -64 Left 2
SYMATTR SpiceModel XXXX
SYMATTR Prefix X
PIN 0 -48 NONE 8
PINATTR PinName vcc
PINATTR SpiceOrder 3
PIN -48 0 NONE 8
PINATTR PinName in
PINATTR SpiceOrder 1
PIN 48 0 NONE 8
PINATTR PinName OUT
PINATTR SpiceOrder 2
PIN 0 48 NONE 8
PINATTR PinName VSS
PINATTR SpiceOrder 4
