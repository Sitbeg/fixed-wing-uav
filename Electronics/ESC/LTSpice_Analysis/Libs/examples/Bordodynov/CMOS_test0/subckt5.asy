Version 4
SymbolType BLOCK
LINE Normal 4 -37 -4 -37
LINE Normal 0 -37 4 -37
LINE Normal 0 -41 0 -37
LINE Normal 0 -33 0 -41
RECTANGLE Normal 64 48 -48 -48
TEXT -42 -33 Left 2 in1
TEXT 52 1 Right 2 out
TEXT -4 -37 Left 2 vcc
TEXT -1 33 Left 2 vss
TEXT -41 29 Left 2 in2
WINDOW 38 48 64 Left 2
WINDOW 0 48 -64 Left 2
SYMATTR SpiceModel XXXX
SYMATTR Prefix X
PIN -48 -32 NONE 8
PINATTR PinName in
PINATTR SpiceOrder 1
PIN -48 32 NONE 8
PINATTR PinName in1
PINATTR SpiceOrder 2
PIN 64 0 NONE 8
PINATTR PinName OUT
PINATTR SpiceOrder 3
PIN 16 -48 NONE 8
PINATTR PinName vcc
PINATTR SpiceOrder 4
PIN 16 48 NONE 8
PINATTR PinName VSS
PINATTR SpiceOrder 5
