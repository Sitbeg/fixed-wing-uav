Version 4
SymbolType BLOCK
LINE Normal 1 -80 -48 -80
LINE Normal 128 0 1 -80
LINE Normal 0 80 128 0
LINE Normal -48 80 0 80
LINE Normal -48 -80 -48 80
LINE Normal -48 16 -64 16
LINE Normal -48 -16 -64 -16
LINE Normal -48 48 -64 48
LINE Normal 144 0 128 0
LINE Normal 32 60 32 96
LINE Normal 96 20 96 96
LINE Normal -16 80 -16 96
LINE Normal -16 -80 -16 -96
LINE Normal 64 -40 64 -96
TEXT -36 -90 Left 1 7
TEXT -39 90 Left 1 1
TEXT -61 39 Left 1 4
TEXT -60 -28 Left 1 9
TEXT -66 5 Left 1 10
TEXT 129 -11 Left 1 5
TEXT 35 80 Left 1 2
TEXT 101 26 Left 1 3
TEXT -43 48 Left 2 GND
TEXT -21 -2 Left 2 K140UD1A
TEXT 44 -62 Left 1 12
WINDOW 0 62 -98 Bottom 2
PIN -64 16 LEFT 20
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -64 -16 LEFT 20
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 32 96 NONE 8
PINATTR PinName 5
PINATTR SpiceOrder 3
PIN 96 96 NONE 8
PINATTR PinName 6
PINATTR SpiceOrder 4
PIN -64 48 NONE 10
PINATTR PinName GrND
PINATTR SpiceOrder 5
PIN 144 0 NONE 8
PINATTR PinName OUT
PINATTR SpiceOrder 6
PIN -16 -96 NONE 8
PINATTR PinName VCC
PINATTR SpiceOrder 7
PIN -16 96 NONE 8
PINATTR PinName VSS
PINATTR SpiceOrder 8
PIN 64 -96 NONE 8
PINATTR PinName 12
PINATTR SpiceOrder 9
