Version 4
SymbolType BLOCK
LINE Normal 1 -80 -48 -80
LINE Normal 128 0 1 -80
LINE Normal 0 80 128 0
LINE Normal -48 80 0 80
LINE Normal -48 -80 -48 80
LINE Normal -48 16 -64 16
LINE Normal -48 -16 -64 -16
LINE Normal -48 48 -64 48
LINE Normal 144 0 128 0
LINE Normal 32 60 32 96
LINE Normal 96 20 96 96
LINE Normal -16 80 -16 96
LINE Normal -16 -80 -16 -96
TEXT -36 -90 Left 2 8
TEXT -39 90 Left 2 4
TEXT -61 39 Left 2 1
TEXT -60 -28 Left 2 2
TEXT -59 5 Left 2 3
TEXT 129 -11 Left 2 7
TEXT 35 80 Left 2 5
TEXT 87 32 Left 2 6
TEXT -43 48 Left 2 GND
TEXT -21 -2 Left 2 uA702
WINDOW 0 77 -62 Bottom 2
PIN -64 16 LEFT 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN -64 -16 LEFT 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 32 96 NONE 8
PINATTR PinName 5
PINATTR SpiceOrder 3
PIN 96 96 NONE 8
PINATTR PinName 6
PINATTR SpiceOrder 4
PIN -64 48 NONE 8
PINATTR PinName GrND
PINATTR SpiceOrder 5
PIN 144 0 NONE 8
PINATTR PinName OUT
PINATTR SpiceOrder 6
PIN -16 -96 NONE 8
PINATTR PinName VCC
PINATTR SpiceOrder 7
PIN -16 96 NONE 8
PINATTR PinName VSS
PINATTR SpiceOrder 8
