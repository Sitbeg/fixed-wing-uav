Version 4
SymbolType BLOCK
RECTANGLE Normal -96 -32 112 48
WINDOW 0 8 -40 Bottom 2
PIN -96 0 LEFT 8
PINATTR PinName input
PINATTR SpiceOrder 1
PIN 112 0 RIGHT 8
PINATTR PinName output
PINATTR SpiceOrder 2
PIN 16 48 BOTTOM 8
PINATTR PinName return
PINATTR SpiceOrder 3
