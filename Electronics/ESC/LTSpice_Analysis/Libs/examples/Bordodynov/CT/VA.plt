[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(N002)*Ix(LU3:core)+V(b)*Ix(LU3:B)+V(h)*Ix(LU3:H)"} {524291,0,"V(X)*Ix(LU2:1)+V(N005)*Ix(LU2:core)"}
      X: ('m',0,0.711,0.003,0.744)
      Y[0]: ('m',0,-0.07,0.07,0.7)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "W" ('m',0,0,0,-0.07,0.07,0.7)
      Log: 0 0 0
      GridStyle: 1
   }
}
