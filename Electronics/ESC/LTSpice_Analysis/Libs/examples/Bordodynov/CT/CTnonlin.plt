[Transient Analysis]
{
   Npanes: 3
   {
      traces: 1 {524291,0,"V(b)"}
      X: (' ',0,0,10,100)
      Y[0]: ('m',0,-0.16,0.04,0.32)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.16,0.04,0.32)
      Log: 0 0 0
   },
   {
      traces: 1 {524290,0,"V(x)"}
      X: (' ',0,0,10,100)
      Y[0]: (' ',1,-3.5,0.7,3.5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-3.5,0.7,3.5)
      Log: 0 0 0
   },
   {
      traces: 1 {524292,0,"V(m)"}
      X: (' ',0,0,10,100)
      Y[0]: ('m',0,-0.06,0.01,0.06)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,1,-0.06,0.01,0.06)
      Log: 0 0 0
   }
}
