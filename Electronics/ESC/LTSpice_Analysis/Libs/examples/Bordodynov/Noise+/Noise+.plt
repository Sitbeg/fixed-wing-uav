[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {524291,0,"V(inoise)"}
      X: ('K',0,1,0,10000)
      Y[0]: ('n',0,0,9e-009,1.08e-007)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('n',0,0,0,0,9e-009,1.08e-007)
      Log: 1 0 0
      GridStyle: 1
   }
}
