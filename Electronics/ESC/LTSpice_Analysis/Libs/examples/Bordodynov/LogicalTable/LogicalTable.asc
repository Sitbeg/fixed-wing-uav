Version 4
SHEET 1 1580 724
WIRE 576 528 576 496
FLAG 576 608 0
FLAG 576 496 L
FLAG 816 608 0
FLAG 1088 608 0
FLAG 816 528 x0
FLAG 1088 528 x8
FLAG 1344 608 0
FLAG 1344 528 x15
FLAG 800 208 0
FLAG 944 208 0
FLAG 1072 208 0
FLAG 1200 208 0
FLAG 800 128 L
FLAG 944 128 x0
FLAG 1072 128 x8
FLAG 1200 128 x15
SYMBOL bv 576 512 R0
SYMATTR InstName B1
SYMATTR Value V=L(X)
SYMBOL voltage 816 512 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value {LFT(0,0,0,0)}
SYMBOL voltage 1088 512 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value {LFT(1,0,0,0)}
SYMBOL voltage 1344 512 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V3
SYMATTR Value {LFT(1,1,1,1)}
SYMBOL res 784 112 R0
SYMATTR InstName R1
SYMATTR Value 1k
SYMBOL res 928 112 R0
SYMATTR InstName R2
SYMATTR Value 1k
SYMBOL res 1056 112 R0
SYMATTR InstName R3
SYMATTR Value 1k
SYMBOL res 1184 112 R0
SYMATTR InstName R4
SYMATTR Value 1k
TEXT -40 40 Left 4 ;The Logical function given by table.
TEXT -8 144 Left 2 ;n    i1 i2  i3 i4   F\n_______________\n0    0  0   0  0    1\n1    0  0   0  1    0\n2    0  0   1  0    0 \n3    0  0   1  1    1\n4    0  1   0  0    1\n5    0  1   0  1    0\n6    0  1   1  0    1\n7    0  1   1  1    1   \n8    1  0   0  0    0\n9    1  0   0  1    0\n10  1  0   1  0    1\n11  1  0   1  1    1\n12  1  1   0  0    0\n13  1  1   0  1    0\n14  1  1   1  0    1\n15  1  1   1  1    1
TEXT 304 168 Left 2 !.func L(x) table(x,\n+ 0  ,     1 ,\n+ 1  ,     0 ,\n+ 2  ,     0 ,\n+ 3  ,      1,\n+ 4   ,    1,\n+ 5  ,     0,\n+ 6  ,     1,\n+ 7   ,    1,   \n+ 8   ,    0,\n+ 9    ,   0,\n+ 10  ,   1,\n+ 11  ,   1,\n+ 12  ,   0,\n+ 13   ,  0,\n+ 14  ,   1,\n+ 15   ,  1)
TEXT 568 304 Left 4 !.func LFT(i1,i2,i3,i4)  L(i1*8+i2*4+i3*2+i4)
TEXT 32 96 Left 2 ;Four input variable.
TEXT 664 432 Left 4 !.step param x 0 15 1
TEXT 584 232 Left 2 !.op
TEXT 1016 696 Left 4 ;See Ctrl+L
LINE Normal 16 640 16 144
LINE Normal 144 640 144 144
