[AC Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {2,0,"V(out)@2/v(in)"} {4,0,"250*0.7"}
      X: ('K',0,10,0,100000)
      Y[0]: (' ',0,40,20,260)
      Y[1]: (' ',0,-300,30,30)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 2 {3,0,"V(out)@1/v(in)"} {5,0,"518*0.7"}
      X: ('K',0,10,0,100000)
      Y[0]: (' ',0,0,50,550)
      Y[1]: (' ',0,-300,30,30)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
