Version 4
SHEET 1 1496 680
WIRE 336 176 256 176
WIRE 432 176 336 176
WIRE 256 384 256 176
WIRE 416 432 256 432
WIRE 208 464 -32 464
WIRE -32 480 -32 464
WIRE 256 528 256 480
FLAG 432 256 0
FLAG 336 176 Vds
FLAG 256 528 0
FLAG -32 560 0
FLAG 416 512 0
SYMBOL nmos4 208 384 R0
WINDOW 123 -92 -48 Left 2
SYMATTR InstName M1
SYMATTR Value mosn
SYMATTR Value2 l=3.5u w=1000u ad=1n pd=605u
SYMBOL voltage 432 160 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName Vd
SYMATTR Value 3.
SYMBOL voltage -32 464 R0
WINDOW 3 24 128 Left 2
WINDOW 123 24 72 Left 2
SYMATTR Value {vgs}
SYMATTR Value2 AC {1/(2*PI*F)} 0
SYMATTR InstName Vg
SYMBOL voltage 416 416 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName Vsub
SYMATTR Value {vs}
TEXT 64 24 Left 2 !.model mosn nmos level=2 uo=600 vto=0.8 tox=45n nsub=2.8e16 ucrit=164k uexp=386m vmax=61.e3\n+ xj=475e-9 ld=136n pb=0.809 neff=679e-3 cj=335e-6 mj=284e-3 cjsw=2.40e-9\n+ mjsw=715.5e-3 cgso=1.10e-10 cgdo=1.1e-10 cgbo=1.39e-10
TEXT 552 432 Left 2 !.step param vgs 0 7 50m
TEXT 592 400 Left 2 !.param F=100
TEXT 336 544 Left 3 ;Igs= Vgs*j*2*PI*F*Cgs, Vgs=1/(2*pi*F) ---> Igs=j*Cgs
TEXT 552 464 Left 2 !.ac list   {F}
TEXT 848 432 Left 2 !.step param vs list 0 -2
TEXT 592 256 Left 3 ;Cgs_model=(im(I(Vg))-im(Id(M1)))/1A
