Version 4
SHEET 1 1656 680
WIRE 336 176 256 176
WIRE 496 176 416 176
WIRE 592 176 496 176
WIRE 688 176 592 176
WIRE 256 256 256 176
WIRE 384 256 256 256
WIRE 496 256 496 176
WIRE 496 256 432 256
WIRE 256 384 256 256
WIRE 416 432 256 432
WIRE 208 464 -32 464
WIRE -32 480 -32 464
WIRE 416 480 416 432
WIRE 416 480 256 480
WIRE 256 528 256 480
FLAG 688 256 0
FLAG 592 176 Vds
FLAG 256 528 0
FLAG -32 560 0
SYMBOL nmos4 208 384 R0
SYMATTR InstName M1
SYMATTR Value mosn
SYMATTR Value2 l=3.5u w=1000u ad=1n pd=605u
SYMBOL voltage 688 160 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName Vd
SYMATTR Value 3.
SYMBOL voltage -32 464 R0
WINDOW 3 24 128 Left 2
WINDOW 123 24 72 Left 2
SYMATTR Value {vgs}
SYMATTR Value2 AC {1/(2*PI*F)} 90
SYMATTR InstName Vg
SYMBOL res 432 160 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 1m  ac=1G
SYMBOL SWeq 384 256 R0
WINDOW 39 -3 53 Left 2
WINDOW 40 -5 22 Left 2
SYMATTR SpiceLine2 y={n}
SYMATTR InstName U1
TEXT 64 24 Left 2 !.model mosn nmos level=2 uo=600 vto=0.8 tox=45n nsub=2.8e16 ucrit=164k uexp=386m vmax=61.e3\n+ xj=475e-9 ld=136n pb=0.809 neff=679e-3 cj=335e-6 mj=284e-3 cjsw=2.40e-9\n+ mjsw=715.5e-3 cgso=1.10e-10 cgdo=1.1e-10 cgbo=1.39e-10
TEXT 552 432 Left 2 !.step param vgs 0 7 50m
TEXT 592 400 Left 2 !.param F=100
TEXT 336 544 Left 3 ;Igs= Vgs*j*2*PI*F*Cgs, Vgs=1/(2*pi*F) ---> Igs=j*Cgs
TEXT 552 464 Left 2 !.ac list   {F}
TEXT 64 -32 Left 2 ;The proof of the veracity of the method. Capacitance Cgd virtually no effect on the results.
TEXT 880 432 Left 2 !.step param n list 0 1
TEXT 824 176 Left 2 !.subckt SWeq 1 2 x=1 y=1 Rmin=1u Rmax=1T\n.func EQ(a,b) if((a>b)|(b>a),0,1)\nR 1 2 R=if(EQ(x,y),Rmin,Rmax)\n.ends SWeq
