Version 4
SymbolType BLOCK
LINE Normal -32 0 -48 0
LINE Normal 32 0 48 0
RECTANGLE Normal 32 -10 -32 11
WINDOW 0 2 -14 Bottom 2
PIN -48 0 NONE 8
PINATTR PinName R+
PINATTR SpiceOrder 1
PIN 48 -80 LEFT 8
PINATTR PinName N
PINATTR SpiceOrder 2
PIN 48 0 NONE 8
PINATTR PinName R-
PINATTR SpiceOrder 3
