[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {268959746,0,"V(inoise)"}
      X: ('G',1,1e+008,3e+008,3e+009)
      Y[0]: ('p',0,0,9e-011,9.9e-010)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('p',0,0,0,0,9e-011,9.9e-010)
      Log: 1 0 0
   }
}
