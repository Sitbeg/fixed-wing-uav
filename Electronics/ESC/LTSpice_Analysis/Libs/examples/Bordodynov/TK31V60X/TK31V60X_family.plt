[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524292,0,"V(out)"} {524291,0,"V(in)*40"}
      X: ('�',1,0,1e-007,1e-006)
      Y[0]: (' ',0,0,40,440)
      Y[1]: (' ',1,1e+308,0.9,-1e+308)
      Volts: (' ',0,0,0,0,40,440)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {34603010,0,"Id(M1)"}
      X: (' ',0,0,1,10)
      Y[0]: (' ',0,0,9,99)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,0,0,9,99)
      Log: 0 0 0
      GridStyle: 1
   }
}
