Version 4
SymbolType CELL
LINE Normal 16 -16 4 -16
LINE Normal 16 32 4 32
RECTANGLE Normal 4 -16 -4 48
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR SpiceModel core_H_B
SYMATTR Value Hc=5 Bs=430m Br=100m
SYMATTR Value2 A=14u Lm=31m Lg=0
SYMATTR SpiceLine2 Fe=100Meg
PIN 0 48 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 1
PIN 16 -16 BOTTOM 8
PINATTR PinName H
PINATTR SpiceOrder 2
PIN 16 32 BOTTOM 8
PINATTR PinName B
PINATTR SpiceOrder 3
