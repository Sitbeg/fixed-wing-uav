Version 4
SymbolType CELL
RECTANGLE Normal 4 -16 -4 48
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR SpiceModel core
SYMATTR Value Hc=50 Bs=350m ur=2000
SYMATTR Value2 Ae=14u Lm=31m Lg=0
SYMATTR SpiceLine2 Eddy=2Meg
PIN 0 48 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 1
