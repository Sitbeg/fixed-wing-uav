Version 4
SHEET 1 7232 3376
WIRE 304 -48 256 -48
WIRE 480 -48 304 -48
WIRE 480 -32 480 -48
WIRE 256 -16 256 -48
WIRE 192 32 144 32
WIRE 256 112 256 80
FLAG 256 112 0
FLAG 480 48 0
FLAG 304 -48 C
FLAG 144 112 0
SYMBOL npn 192 -16 R0
SYMATTR InstName Q1
SYMATTR Value 2SC2099
SYMATTR Prefix xQN
SYMBOL voltage 480 -48 R0
WINDOW 123 0 0 Left 2
WINDOW 39 24 124 Left 2
SYMATTR SpiceLine Rser=1
SYMATTR InstName V1
SYMATTR Value 10
SYMBOL res 128 16 R0
SYMATTR InstName R1
SYMATTR Value {R}
TEXT 96 -128 Left 2 !.lib 2SC2099.sub
TEXT 112 232 Left 2 !.dc v1 1 45 100m
TEXT 656 -40 Left 2 !.meas Vce when Ix(Q1:C)=100u
TEXT 352 216 Left 2 !.step param R list 5.6 10k 100k 1G
TEXT 680 8 Left 2 ;Measurement: vce\n  step          ix(q1:c)=100u\n     1          40.0006\n     2          40.2\n     3          30\n     4          18
