Version 4
SHEET 1 880 680
FLAG 96 128 0
FLAG 96 256 0
FLAG 96 48 x
FLAG 96 176 y
FLAG 96 320 F
FLAG 96 400 0
SYMBOL voltage 96 32 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value PULSE(0 1 0 {T/2} {T/2} 0 {T})
SYMBOL voltage 96 160 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value PWL(0 1u 1m 0.999999 2m 1u)
SYMBOL bv 96 304 R0
SYMATTR InstName B1
SYMATTR Value V=A*u(v(y,x))
TEXT 88 -16 Left 2 !.param T=10u A=5
TEXT 192 64 Left 2 !.tran 0 1m 0 1n
TEXT 192 24 Left 2 !.options plotwinsize=0
