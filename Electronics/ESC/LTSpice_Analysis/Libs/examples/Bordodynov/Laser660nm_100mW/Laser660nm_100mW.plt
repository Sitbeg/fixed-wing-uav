[DC transfer characteristic]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(pop)"}
      X: ('m',0,0,0.02,0.16)
      Y[0]: ('m',0,-0.01,0.01,0.11)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,1,-0.01,0.01,0.11)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(A,C)"}
      X: ('m',0,0,0.02,0.16)
      Y[0]: (' ',1,0,0.3,2.7)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,0,0.3,2.7)
      Log: 0 0 0
      GridStyle: 1
   }
}
