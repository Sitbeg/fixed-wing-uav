Version 4
SymbolType CELL
LINE Normal 6 -31 -33 23
LINE Normal 30 -31 6 -31
LINE Normal -16 31 -16 48
LINE Normal -16 -32 -16 -48
RECTANGLE Normal -7 -32 -24 31
TEXT 15 -42 Left 2 t
WINDOW 0 20 -54 Bottom 2
SYMATTR Prefix X
SYMATTR Value NTC_BTH_3pin
SYMATTR ModelFile NTC_BTH_3pin.sub
SYMATTR SpiceLine R0=40 B=3300
SYMATTR SpiceLine2 RTH=49.4  TAU=114
PIN -16 -48 NONE 8
PINATTR PinName 10
PINATTR SpiceOrder 1
PIN -16 48 NONE 8
PINATTR PinName 3
PINATTR SpiceOrder 2
PIN 32 -32 NONE 8
PINATTR PinName 9
PINATTR SpiceOrder 3
