[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {34603011,0,"I(I1)"}
      Parametric: "V(x)"
      X: (' ',1,0.4,0.4,4.4)
      Y[0]: (' ',1,0,0.1,1)
      Y[1]: (' ',1,1e+308,0.2,-1e+308)
      Amps: (' ',0,0,1,0,0.1,1)
      Log: 0 0 0
      GridStyle: 1
   }
}
