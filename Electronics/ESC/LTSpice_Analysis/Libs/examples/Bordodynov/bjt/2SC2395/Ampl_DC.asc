Version 4
SHEET 1 7232 3376
WIRE 496 -272 256 -272
WIRE 256 -256 256 -272
WIRE 496 -256 496 -272
WIRE 256 -128 256 -176
WIRE 320 -128 256 -128
WIRE 256 -16 256 -128
WIRE 0 32 -16 32
WIRE 128 32 80 32
WIRE 192 32 128 32
WIRE -16 80 -16 32
WIRE 256 112 256 80
WIRE -16 208 -16 160
WIRE 96 208 48 208
WIRE -112 304 -192 304
WIRE -16 304 -16 208
WIRE -16 304 -32 304
WIRE 96 304 96 208
WIRE 96 304 64 304
WIRE 96 320 96 304
FLAG 256 112 0
FLAG 496 -176 0
FLAG 128 112 0
FLAG 96 320 0
FLAG -192 384 0
FLAG 128 32 B
FLAG 256 -128 C
SYMBOL voltage 496 -272 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 12.5
SYMBOL npn 192 -16 R0
SYMATTR InstName Q1
SYMATTR Value 2SC2395
SYMATTR Prefix xQN
SYMBOL res 112 16 R0
SYMATTR InstName R1
SYMATTR Value 5.6
SYMBOL ind -16 48 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L2
SYMATTR Value 54n
SYMBOL ind -32 64 R0
SYMATTR SpiceLine Rser=1.2
SYMATTR InstName LRFC1
SYMATTR Value 1.89�
SYMBOL cap 48 192 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C5
SYMATTR Value 0.4�
SYMBOL ind 240 -272 R0
SYMATTR InstName LRFC2
SYMATTR Value 1.223�
SYMATTR SpiceLine Rser=0.63
SYMBOL res -16 288 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 5
SYMBOL res 80 288 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 1.5
SYMBOL voltage -192 288 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 3.105
TEXT 568 -216 Left 2 !.lib 2SC2395.sub
TEXT -208 -96 Left 2 !.dc V2 2.7 5 0.1m
TEXT 536 -40 Left 2 !.meas V1A when Ix(Q1:c)=1A
TEXT 512 8 Left 2 !.meas V25mA when Ix(Q1:c)=50mA
TEXT 520 72 Left 2 ;v1a: ix(q1:c)=1a AT 3.67486\nv25ma: ix(q1:c)=50ma AT 3.09423
