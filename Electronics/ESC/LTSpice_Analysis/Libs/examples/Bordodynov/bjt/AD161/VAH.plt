[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"Ic(Q1)*if(V(C)*Ic(Q1)+V(B)*Ib(Q1)<5.5W,1,0)"}
      X: (' ',0,0,1,10)
      Y[0]: (' ',1,-0.2,0.2,2.2)
      Y[1]: (' ',0,1e+308,8,-1e+308)
      Amps: (' ',0,0,1,-0.2,0.2,2.2)
      Log: 0 0 0
   }
}
