Version 4
SHEET 1 1316 680
WIRE 432 0 368 0
WIRE 480 0 432 0
WIRE 480 16 480 0
WIRE 368 64 368 0
WIRE 288 112 240 112
WIRE 304 112 288 112
WIRE 480 128 480 96
WIRE 368 176 368 160
WIRE 384 176 368 176
WIRE 496 176 384 176
WIRE 368 192 368 176
WIRE 496 192 496 176
WIRE 240 256 240 112
WIRE 368 304 368 272
FLAG 368 304 0
FLAG 480 128 0
FLAG 432 0 c
FLAG 384 176 e
FLAG 240 256 0
FLAG 288 112 b
FLAG 496 272 0
SYMBOL npn 304 64 R0
SYMATTR InstName Q1
SYMATTR Value KT6113G
SYMBOL voltage 480 0 R0
SYMATTR InstName V1
SYMATTR Value {Vc}
SYMBOL current 368 192 R0
WINDOW 3 19 79 Left 2
WINDOW 123 26 119 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value {Iedc}
SYMATTR InstName Ie
SYMBOL current 496 192 R0
WINDOW 3 26 143 Left 2
WINDOW 123 36 40 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value ""
SYMATTR Value2 AC 1
SYMATTR InstName IeAC
TEXT 240 -128 Left 2 ;NPN Ft Testjig
TEXT 240 360 Left 2 !.ac list {Ttest}
TEXT 240 440 Left 2 !.step dec param Iedc 0.2m 30m 12\n.step param Vc list  5 8
TEXT 384 360 Left 2 ;<====== Choose Freq to suit the particular transistor.\n                 You will also need to change the waveform\n                 expression accordingly.
TEXT 528 440 Left 2 ;<======= Choose Iedc to suit the particular transistor.
TEXT 528 464 Left 2 ;<======= Sets the collector-basa voltage
TEXT 712 48 Left 2 !.param Ttest=100Meg
RECTANGLE Normal 1040 496 224 336
