[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(po)"}
      X: ('�',0,0,2e-005,0.0002)
      Y[0]: (' ',0,-30,5,30)
      Y[1]: (' ',0,1e+308,2,-1e+308)
      Volts: (' ',0,0,0,-30,5,30)
      Log: 0 0 0
      GridStyle: 1
   }
}
[FFT of time domain data]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(po)"}
      X: ('M',0,5000,0,6.55355e+008)
      Y[0]: (' ',0,1e-008,20,10)
      Y[1]: (' ',0,-200,40,200)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
   }
}
