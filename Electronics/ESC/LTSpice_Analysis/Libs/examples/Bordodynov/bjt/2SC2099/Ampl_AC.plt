[AC Analysis]
{
   Npanes: 3
   Active Pane: 2
   {
      traces: 1 {589828,0,"V(b)"}
      X: ('M',0,1e+007,9e+006,1e+008)
      Y[0]: ('m',0,0,0.007,0.077)
      Y[1]: (' ',0,-360,40,80)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 1 {589830,0,"V(in)"}
      X: ('M',0,1e+007,9e+006,1e+008)
      Y[0]: ('m',0,0,0.08,1.04)
      Y[1]: (' ',0,-80,10,50)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 2 {524291,0,"V(po)"} {524290,0,"V(c)"}
      X: ('M',0,1e+007,9e+006,1e+008)
      Y[0]: (' ',1,0,0.2,2.4)
      Y[1]: (' ',0,-780,60,-60)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {68157442,0,"Ix(Q1:C)"}
      X: (' ',2,2.7,0.08,3.5)
      Y[0]: (' ',1,0,0.5,5.5)
      Y[1]: (' ',0,1e+308,70,-1e+308)
      Amps: (' ',0,0,0,0,0.5,5.5)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[
