[AC Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {3,0,"re(Zout(v3))"} {4,0,"im(Zout(v3))"}
      X: ('M',0,1e+007,9e+006,1e+008)
      Y[0]: (' ',1,0.5,0.5,5.5)
      Y[1]: ('m',1,-0.001,0.0002,0.001)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 2 {2,0,"re(Z11(v3))"} {5,0,"im(Z11(v3))"}
      X: ('M',0,1e+007,9e+006,1e+008)
      Y[0]: (' ',1,0,0.1,1)
      Y[1]: (' ',0,-360,40,40)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
