[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"Ic(Q1)/Ib(Q1)"}
      X: ('m',0,0.0001,0,0.05)
      Y[0]: (' ',0,0,40,440)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "" (' ',0,0,0,0,40,440)
      Log: 1 0 0
      GridStyle: 1
   }
}
