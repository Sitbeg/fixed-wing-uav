[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(inoise)"}
      X: ('K',0,30,0,15000)
      Y[0]: ('n',0,6e-009,1e-009,2.1e-008)
      Y[1]: (' ',0,1e+308,2,-1e+308)
      Units: "V/Hz�" ('n',0,0,0,6e-009,1e-009,2.1e-008)
      Log: 1 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
