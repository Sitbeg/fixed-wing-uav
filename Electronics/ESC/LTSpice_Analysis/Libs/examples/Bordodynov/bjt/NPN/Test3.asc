Version 4
SHEET 1 3300 3648
WIRE 176 144 144 144
WIRE 352 144 320 144
WIRE 144 192 144 144
WIRE -16 240 -16 224
WIRE 80 240 -16 240
WIRE 144 304 144 288
FLAG 352 224 0
FLAG 144 304 0
FLAG -16 144 0
FLAG 144 144 C
SYMBOL npn 80 192 R0
SYMATTR InstName Q1
SYMATTR Value m504
SYMBOL current -16 144 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName I1
SYMATTR Value 100�
SYMBOL voltage 352 128 R0
WINDOW 123 0 0 Left 2
WINDOW 39 24 124 Left 2
SYMATTR InstName V1
SYMATTR Value 5
SYMBOL diode 240 128 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName D1
SYMATTR Value lim
SYMBOL res 336 128 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value -0.1
TEXT -48 328 Left 2 !.dc v1 0.1 20  0.1 i1 0 1m 0.1m
TEXT 40 80 Left 2 !.model lim d Vfwd=0 Ron=0.1  ilimit=100mA
TEXT -144 -176 Left 2 !.model m504 npn  level=504 tref=25.0 exmod=1.0 exphi=1.0 exavl=0.0 is=22.0e-18 ik=0.1\n+ ver=2.5 vef=44.0 bf=215.0 ibf=2.7e-15 mlf=2.0 xibi=0.0 bri=7.0 ibr=1.0e-15 vlr=0.2 xext=0.63\n+ wavl=1.1e-6 vavl=3.0 sfh=0.3 re=5.0 rbc=23. rbv=18. rcc=12. rcv=150. scrcv=1250.0\n+ ihc=4.e-3 axi=0.3 cje=73.0e-15 vde=0.95 pe=0.4 xcje=0.4 cjc=78.0e-15 vdc=0.68 pc=0.5\n+ xp=0.35 mc=0.5 xcjc=32.e-3 mtau=1.0 taue=2.0e-12 taub=4.2e-12 tepi=41.e-12\n+ taur=520.e-12 deg=0.01 xrec=0.1 aqbo=0.3 ae=0.0 ab=1.0 aepi=2.5 aex=0.62 ac=2.0\n+ dvgbf=0.05 dvgbr=0.045 vgb=1.17 vgc=1.18 vgj=1.15 dvgte=0.05 af=2.0 kf=2.e-11\n+ kfn=2.e-11 iss=48.e-18 iks=250.e-6 cjs=315.e-15 vds=0.62 ps=0.34 vgs=1.20 as=1.58
