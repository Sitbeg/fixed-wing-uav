Version 4
SHEET 1 1220 680
WIRE 560 -304 224 -304
WIRE 224 -208 224 -304
WIRE 240 32 -128 32
WIRE 496 32 480 32
WIRE 528 32 496 32
WIRE -128 64 -128 32
WIRE 496 64 496 32
WIRE 608 64 608 32
WIRE 400 96 400 32
WIRE 240 240 -128 240
WIRE -128 272 -128 240
FLAG -128 352 0
FLAG 240 240 tri
FLAG 224 -128 0
FLAG 560 -304 out
IOPIN 560 -304 Out
FLAG -128 144 0
FLAG 240 32 gianni
FLAG 400 176 0
FLAG 608 32 OUTF
FLAG 496 128 0
FLAG 608 128 0
SYMBOL voltage -128 256 R0
WINDOW 0 24 16 Invisible 2
SYMATTR InstName tri
SYMATTR Value PULSE(0 1 0 25u 25u 0 50u)
SYMBOL voltage -128 48 R0
WINDOW 0 24 16 Invisible 2
SYMATTR InstName gianni
SYMATTR Value SINE(0.5 0.5 440)
SYMBOL bi2 400 96 R0
SYMATTR InstName B2
SYMATTR Value I=V(out) Rpar=1 Cpar=50u
SYMBOL bi2 224 -208 R0
SYMATTR InstName B1
SYMATTR Value I=if(V(gianni)>V(tri) ,1, 0) Rpar=1 Cpar=1u
SYMBOL res 496 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 10
SYMBOL cap 480 64 R0
SYMATTR InstName C1
SYMATTR Value 5�
SYMBOL res 624 16 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R2
SYMATTR Value 100
SYMBOL cap 592 64 R0
SYMATTR InstName C2
SYMATTR Value 0.5�
TEXT -192 -112 Left 2 !.tran 0 0.51s 0.01s 1u
TEXT -144 -256 Left 4 !.opt plotwinsize=0
