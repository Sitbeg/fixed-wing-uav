Version 4
SHEET 1 1268 884
WIRE 560 48 176 48
WIRE 176 64 176 48
WIRE 512 128 304 128
WIRE 304 160 304 128
WIRE 368 160 304 160
WIRE 560 160 560 144
WIRE 560 160 448 160
WIRE 304 176 304 160
WIRE 368 256 352 256
WIRE 528 256 512 256
WIRE 560 256 560 224
WIRE 560 256 528 256
WIRE 608 256 560 256
WIRE 656 256 608 256
WIRE 528 304 528 256
WIRE 304 320 304 272
WIRE 368 320 368 256
WIRE 304 352 304 320
WIRE 304 352 192 352
WIRE 192 368 192 352
WIRE 656 368 656 256
WIRE 368 384 368 320
WIRE 368 384 336 384
WIRE 560 384 560 352
WIRE 560 384 368 384
WIRE 304 416 256 416
WIRE 176 464 176 144
WIRE 192 464 176 464
WIRE 176 496 176 464
WIRE 192 496 176 496
WIRE 304 496 304 416
WIRE 304 496 272 496
WIRE 304 512 304 496
WIRE 560 512 560 464
WIRE 656 512 656 432
WIRE 272 624 208 624
WIRE 368 624 272 624
WIRE 208 656 208 624
WIRE 368 656 368 624
WIRE 480 672 416 672
WIRE 480 688 480 672
WIRE 208 752 208 736
WIRE 368 752 368 736
WIRE 416 752 416 720
WIRE 416 752 368 752
WIRE 368 784 368 752
WIRE 480 784 480 768
FLAG 608 256 Out
FLAG 560 512 0
FLAG 656 512 0
FLAG 304 512 0
FLAG 208 752 0
FLAG 368 784 0
FLAG 480 784 0
FLAG 272 624 Out
SYMBOL voltage 176 48 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
WINDOW 3 56 22 Left 2
SYMATTR Value SINE(300 15V 100 1m)
SYMATTR InstName V1
SYMBOL res 544 368 R0
SYMATTR InstName R2
SYMATTR Value 10K
SYMATTR SpiceLine tol=1 pwr=0.1
SYMBOL Misc\\EuropeanPolcap 640 368 R0
WINDOW 0 -19 2 Left 2
WINDOW 39 24 92 Left 2
SYMATTR InstName C1
SYMATTR SpiceLine V=350 Rser=3
SYMATTR Value 3.3�
SYMBOL res 464 144 R90
WINDOW 0 50 112 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R4
SYMATTR Value 470
SYMBOL zener 576 224 R180
WINDOW 0 40 29 Left 2
WINDOW 3 -60 15 VRight 2
SYMATTR InstName D1
SYMATTR Value BZX384B5V6
SYMATTR Description Diode
SYMATTR Type diode
SYMBOL res 288 480 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R1
SYMATTR Value 3
SYMBOL npn 256 368 M0
WINDOW 0 39 45 Left 2
WINDOW 3 -12 -44 Left 2
SYMATTR InstName Q1
SYMATTR Value 2N3904
SYMBOL current 208 656 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName ILoad1
SYMATTR Value 0.2
SYMBOL sw 368 752 R180
WINDOW 3 33 55 Left 2
SYMATTR InstName S1
SYMBOL voltage 480 672 R0
WINDOW 3 -87 -21 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value PULSE(-2 2 95ms 1us 0 1s)
SYMATTR InstName V2
SYMBOL cap 368 304 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C3
SYMATTR Value 1n
SYMBOL cap 512 240 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C4
SYMATTR Value 0.1�
SYMBOL res 464 240 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R3
SYMATTR Value 10k
SYMBOL EXTRA\\VolReg\\TL431A 320 352 M0
WINDOW 0 24 64 Invisible 2
WINDOW 3 -94 57 Left 2
SYMATTR InstName U1
SYMBOL pot 560 304 R0
SYMATTR InstName U3
SYMATTR Value R=1Meg
SYMATTR Value2 T={w}
SYMBOL DepletionNmos 512 48 R0
WINDOW 3 -142 52 Left 2
SYMATTR InstName M3
SYMATTR Value IXTP6N100D2
SYMBOL DepletionNmos 352 176 M0
SYMATTR InstName M1
SYMATTR Value DN2540
TEXT 376 456 Left 2 !.tran 90m
TEXT 344 24 Left 2 ;.step param w list 0.5 1
TEXT 352 504 Left 2 !.opt reltol=10u
TEXT 200 824 Left 2 !.model SW SW(Ron=10 Roff=1Meg Vt=0 Vh=-.5)
TEXT 360 592 Left 3 ;Simulation Load
TEXT 192 24 Left 2 !.param w=1
TEXT 352 536 Left 2 !.lib sum.txt
RECTANGLE Normal 720 880 176 560 1
