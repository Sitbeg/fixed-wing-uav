Version 4
SymbolType BLOCK
RECTANGLE Normal -32 -56 80 56
TEXT -32 71 Left 2 G3VM-101
WINDOW 0 0 -56 Bottom 2
PIN -32 -32 LEFT 8
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN -32 0 LEFT 8
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN -32 32 LEFT 8
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN 80 32 RIGHT 8
PINATTR PinName 4
PINATTR SpiceOrder 4
PIN 80 0 RIGHT 8
PINATTR PinName 5
PINATTR SpiceOrder 5
PIN 80 -32 RIGHT 8
PINATTR PinName 6
PINATTR SpiceOrder 6
