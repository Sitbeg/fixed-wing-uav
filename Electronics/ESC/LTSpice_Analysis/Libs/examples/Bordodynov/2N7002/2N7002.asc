Version 4
SHEET 1 1228 680
WIRE 224 48 192 48
WIRE 304 48 224 48
WIRE 192 112 192 48
WIRE 512 112 512 64
WIRE 96 192 16 192
WIRE 144 192 96 192
WIRE 464 192 416 192
FLAG 192 208 0
FLAG 304 128 0
FLAG 16 272 0
FLAG 96 192 G
FLAG 224 48 D
FLAG 512 208 0
FLAG 512 64 D
FLAG 416 192 G
SYMBOL nmos 144 112 R0
SYMATTR InstName M1
SYMATTR Value 2N7002
SYMBOL voltage 304 32 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 12
SYMBOL voltage 16 176 R0
WINDOW 0 10 -5 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 12
SYMBOL nmos 464 112 R0
SYMATTR InstName M2
SYMATTR Value 2N7002new
TEXT 96 328 Left 2 !.model 2n7002new ako: 2n7002 kp=0.3 vto=2.1  lambda=0.3m \n+ ksubthres=100m tt=100n theta=86m RDS=100MEG BV=75 ibv=10u nbv=5
TEXT -32 400 Left 2 !.dc v1 0 5 10m v2 3 10 1
TEXT 96 256 Left 2 !.model 2n7002 vdmos(rg=3 vto=1.6 rd=0 rs=.75 rb=.14 kp=.17 mtriode=1.25 cgdmax=80p\n+ cgdmin=12p cgs=50p cjo=50p is=.04p mfg=fairchild vds=60 ron=2 qg=1.5n)
