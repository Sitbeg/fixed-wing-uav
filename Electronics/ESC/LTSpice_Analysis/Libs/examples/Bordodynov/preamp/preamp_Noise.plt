[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 1
   {
      traces: 1 {268959747,0,"V(inoise)"}
      X: ('K',0,10,0,100000)
      Y[0]: ('n',1,1.4e-009,2e-010,3e-009)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "V/Hz�" ('n',0,0,0,1.4e-009,2e-010,3e-009)
      Log: 1 0 0
      GridStyle: 1
   }
}
