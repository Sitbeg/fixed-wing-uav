[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"-Id(M2)"}
      Parametric: "- V1"
      X: (' ',1,0,0.5,5)
      Y[0]: (' ',1,-0.1,0.1,1.1)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,1,-0.1,0.1,1.1)
      Log: 0 0 0
      GridStyle: 1
   }
}
