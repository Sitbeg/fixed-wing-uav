[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {34603010,0,"I(R1)"}
      Parametric: "- Vds"
      X: (' ',0,0,5,50)
      Y[0]: ('m',0,0,0.09,0.99)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('m',0,0,0,0,0.09,0.99)
      Log: 0 0 0
      GridStyle: 1
   }
}
