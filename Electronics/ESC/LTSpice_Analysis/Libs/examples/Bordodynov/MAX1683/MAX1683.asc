Version 4
SHEET 1 1024 680
WIRE 752 208 752 176
WIRE 752 208 656 208
WIRE 464 224 464 176
WIRE 544 224 464 224
WIRE 560 224 544 224
WIRE 464 240 464 224
WIRE 752 240 752 208
WIRE -64 256 -144 256
WIRE -48 256 -64 256
WIRE 752 336 752 320
FLAG -144 336 0
FLAG -64 256 osc
FLAG -144 32 in
IOPIN -144 32 BiDir
FLAG -144 112 GND_
FLAG -16 176 GND_
FLAG -16 96 G
FLAG 416 304 GND_
FLAG 416 256 G
FLAG 464 320 GND_
IOPIN 464 320 BiDir
FLAG 416 160 GND_
FLAG 416 112 G
FLAG 464 96 in
FLAG 704 304 GND_
FLAG 704 256 G
FLAG 704 160 VSS
FLAG 704 112 G
FLAG 752 96 IN
FLAG 544 224 C1-
IOPIN 544 224 BiDir
FLAG 656 208 C1+
IOPIN 656 208 BiDir
FLAG 752 336 OUT
IOPIN 752 336 Out
SYMBOL voltage -144 240 R0
WINDOW 3 -11 126 Left 2
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR Value PULSE(1 0 {0.5/35k} 30n 30n {0.5/35k-30n} {1/35k})
SYMATTR InstName V1
SYMBOL res -160 16 R0
SYMATTR InstName R1
SYMATTR Value 25k
SYMBOL bv -16 80 R0
SYMATTR InstName B1
SYMATTR Value V=V(in,gnd_)*v(osc)
SYMBOL sw 464 336 M180
SYMATTR InstName S1
SYMATTR Value S1
SYMBOL sw 464 192 M180
SYMATTR InstName S2
SYMATTR Value S2
SYMBOL sw 752 336 M180
SYMATTR InstName S3
SYMATTR Value S2
SYMBOL sw 752 192 M180
SYMATTR InstName S4
SYMATTR Value S1
TEXT -112 -16 Left 2 !.MODEL s1 SW(Roff=100G Ron={Rt} Vt=1.3 Vh=-0.2  ilimit=200m level=2 )
TEXT -80 48 Left 2 !.param Rt=0.9
TEXT -112 16 Left 2 !.MODEL s2 SW(Ron=100G Roff={Rt} Vt=1.3 Vh=-0.2 ilimit=200m  level=2 )
