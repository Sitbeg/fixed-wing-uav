[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524291,0,"V(b)"} {34603010,1,"I(V1)"}
      X: ('m',0,0.075,0.005,0.13)
      Y[0]: (' ',0,-15,3,15)
      Y[1]: ('m',0,-0.42,0.07,0.28)
      Volts: (' ',0,0,1,-15,3,15)
      Amps: ('m',0,0,0,-0.42,0.07,0.28)
      Log: 0 0 0
   }
}
