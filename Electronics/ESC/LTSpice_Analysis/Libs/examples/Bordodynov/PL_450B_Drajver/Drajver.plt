[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {589826,0,"V(plight)"}
      X: ('m',1,0,0.0001,0.001)
      Y[0]: ('m',0,0,0.009,0.09)
      Y[1]: ('m',0,1e+308,0.007,-1e+308)
      Volts: ('m',0,0,1,0,0.009,0.09)
      Log: 0 0 0
      GridStyle: 1
   }
}
