[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(plight)"}
      X: ('m',0,0,0.01,0.11)
      Y[0]: ('m',0,0,0.005,0.08)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,0,0.005,0.08)
      Log: 0 0 0
      GridStyle: 1
   }
}
