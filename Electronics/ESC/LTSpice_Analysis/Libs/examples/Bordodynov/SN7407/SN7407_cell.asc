Version 4
SHEET 1 1248 680
WIRE -32 -512 -32 -544
WIRE -32 -512 -240 -512
WIRE 128 -512 -32 -512
WIRE 128 -496 128 -512
WIRE -32 -416 -32 -432
WIRE -304 -368 -336 -368
WIRE -288 -368 -304 -368
WIRE -96 -368 -192 -368
WIRE 400 -368 288 -368
WIRE 448 -368 400 -368
WIRE 288 -336 288 -368
WIRE -32 -304 -32 -320
WIRE 128 -288 128 -416
WIRE 224 -288 128 -288
WIRE 128 -272 128 -288
WIRE 64 -224 -32 -224
WIRE -32 -208 -32 -224
WIRE 400 -208 400 -368
WIRE -304 -192 -304 -368
WIRE -32 -128 -304 -128
WIRE 128 -128 128 -176
WIRE 128 -128 -32 -128
WIRE 288 -128 288 -240
WIRE 288 -128 128 -128
WIRE 400 -128 400 -144
WIRE 400 -128 288 -128
WIRE -32 -96 -32 -128
FLAG -336 -368 in
IOPIN -336 -368 In
FLAG 448 -368 OUT
IOPIN 448 -368 Out
FLAG -32 -544 VDD
IOPIN -32 -544 BiDir
FLAG -32 -96 VSS
IOPIN -32 -96 BiDir
SYMBOL npn -192 -432 R90
SYMATTR InstName Q1
SYMATTR Value q74
SYMBOL npn -96 -416 R0
SYMATTR InstName Q2
SYMATTR Value q74
SYMBOL npn 224 -336 R0
WINDOW 3 -24 -8 Left 2
SYMATTR Value q74 2
SYMATTR InstName Q3
SYMBOL res -256 -528 R0
SYMATTR InstName R1
SYMATTR Value 6K
SYMBOL res -48 -528 R0
SYMATTR InstName R2
SYMATTR Value 3.4K
SYMBOL res -48 -224 R0
SYMATTR InstName R3
SYMATTR Value 1K
SYMBOL diode -288 -128 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMATTR Value D74CLMP
SYMBOL diode 416 -144 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D2
SYMATTR Value D74
SYMBOL res -48 -320 R0
SYMATTR InstName R4
SYMATTR Value 100
SYMBOL npn 64 -272 R0
WINDOW 3 -24 -8 Left 2
SYMATTR Value q74
SYMATTR InstName Q4
SYMBOL res 112 -512 R0
SYMATTR InstName R5
SYMATTR Value 1.6K
TEXT -296 -24 Left 2 !.model D74 d (\n+               is=1e-16        rs=25           cjo=2pf BV=31\n+               )\n.model D74CLMP d (\n+               is=1e-15        rs=2            cjo=2pf\n+               )\n.model Q74 npn (\n+               ise=1e-16       isc=4e-16\n+               bf=49           br=.03\n+               cje=1pf         cjc=.5pf\n+               cjs=3pf         vje=0.9v\n+               vjc=0.8v        vjs=0.7v\n+               mje=0.5         mjc=0.33\n+               mjs=0.33        tf=0.2ns\n+               tr=10ns         rb=50\n+               rc=20\n+               )
