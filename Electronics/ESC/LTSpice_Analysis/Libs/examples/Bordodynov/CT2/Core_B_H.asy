Version 4
SymbolType CELL
RECTANGLE Normal 4 48 -4 -15
WINDOW 0 -37 5 Bottom 2
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR SpiceModel core_B_H
SYMATTR Value Hc=5 Bs=430m Br=100m
SYMATTR Value2 A=14u Lm=31m Lg=0
SYMATTR SpiceLine2 Fe=100Meg B0=0
PIN 0 48 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 1
PIN 0 -64 RIGHT 8
PINATTR PinName B
PINATTR SpiceOrder 2
PIN 0 -32 RIGHT 8
PINATTR PinName H
PINATTR SpiceOrder 3
