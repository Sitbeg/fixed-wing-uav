[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {589828,0,"V(b)"}
      Parametric: "V(h)"
      X: ('m',0,-0.8,0.1,0.8)
      Y[0]: (' ',1,-1.4,0.2,1.4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,-1.4,0.2,1.4)
      Log: 0 0 0
      GridStyle: 1
   }
}
