[Transient Analysis]
{
   Npanes: 2
   {
      traces: 1 {524290,0,"V(nput_cpu-mirror_folding_motor_current_sense_signal)"}
      X: ('m',1,0,0.0002,0.002)
      Y[0]: (' ',2,0.32,0.08,1.2)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0.32,0.08,1.2)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(nput_cpu-mirror_folding_motor_current_sense_signal2)"}
      X: ('m',1,0,0.0002,0.002)
      Y[0]: (' ',1,0,0.2,2)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.2,2)
      Log: 0 0 0
      GridStyle: 1
      Text: "V" 4 (0.000921940928270042,1.61904761904762) ;It looks at the reality
   }
}
