[DC transfer characteristic]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"-I(V1)"}
      X: (' ',1,0.8,0.1,2)
      Y[0]: (' ',0,0,3,27)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,0,0,3,27)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {34603011,0,"Ic(Q3)"}
      X: (' ',1,0.8,0.1,2)
      Y[0]: (' ',0,0,3,27)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: (' ',0,0,0,0,3,27)
      Log: 0 0 0
      GridStyle: 1
   }
}
