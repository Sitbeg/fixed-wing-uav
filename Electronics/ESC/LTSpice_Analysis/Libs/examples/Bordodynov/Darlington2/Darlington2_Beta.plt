[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524291,0,"Ic(Q3)/ib(q3)"} {524290,0,"Ix(Q1:C)/ix(q1:b)"}
      Parametric: "I(i1)"
      X: ('m',0,0,0.05,0.5)
      Y[0]: ('K',0,-6000,6000,60000)
      Y[1]: (' ',1,1e+308,0.1,-1e+308)
      Units: "" ('K',0,0,0,-6000,6000,60000)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
