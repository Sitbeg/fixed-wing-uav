[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 2 {34668550,0,"I(V1)"} {34603011,0,"I(V2)"}
      X: (' ',0,5,7,70)
      Y[0]: ('_',0,1e-009,0,0.0001)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('_',1,0,0,1e-009,0,0.0001)
      Log: 0 1 0
      GridStyle: 1
   }
}
