[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(y)"}
      X: ('m',0,0,0.01,0.0999)
      Y[0]: ('K',1,0,100,1000)
      Y[1]: ('m',0,1e+308,0.005,-1e+308)
      Volts: ('K',0,0,1,0,100,1000)
      Log: 0 0 0
      GridStyle: 1
   }
}
