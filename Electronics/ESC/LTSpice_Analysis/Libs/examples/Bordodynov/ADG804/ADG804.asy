Version 4
SymbolType BLOCK
RECTANGLE Normal 64 144 -96 -128
WINDOW 0 8 -132 Bottom 2
PIN -96 48 LEFT 8
PINATTR PinName A0
PINATTR SpiceOrder 1
PIN -96 80 LEFT 8
PINATTR PinName A1
PINATTR SpiceOrder 2
PIN -96 112 LEFT 8
PINATTR PinName EN
PINATTR SpiceOrder 3
PIN -96 -96 LEFT 8
PINATTR PinName S1
PINATTR SpiceOrder 4
PIN -96 -64 LEFT 8
PINATTR PinName S2
PINATTR SpiceOrder 5
PIN -96 -32 LEFT 8
PINATTR PinName S3
PINATTR SpiceOrder 6
PIN -96 0 LEFT 8
PINATTR PinName S4
PINATTR SpiceOrder 7
PIN 64 -48 RIGHT 8
PINATTR PinName D
PINATTR SpiceOrder 8
PIN 0 -128 TOP 8
PINATTR PinName VCC
PINATTR SpiceOrder 9
PIN 0 144 BOTTOM 8
PINATTR PinName VSS
PINATTR SpiceOrder 10
