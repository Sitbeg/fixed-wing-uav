Version 4
SHEET 1 1764 696
WIRE 112 144 32 144
WIRE 368 144 176 144
WIRE 32 160 32 144
WIRE 112 224 112 144
WIRE 128 224 112 224
FLAG 176 240 0
FLAG 32 240 0
FLAG 368 224 0
SYMBOL voltage 368 128 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value 1
SYMBOL voltage 32 144 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 0
SYMBOL Misc\\NIGBT 128 144 R0
SYMATTR InstName Z1
SYMATTR Value IXGT40N60C2
TEXT 168 288 Left 2 !.dc dec v1 10 1200 100
TEXT 792 8 Left 2 ;*.param T0=temp-kelvin T25=temp-25 Tk=25-kelvin Tr=T0/Tk \n* Parameter Effects: Wb, Nb, Bvf and Bvn control breakdown voltage.\n* Kf directly controls sharpness of transition to constant current\n* in curves.  Jsne directly controls saturation resistance and tail\n* current.  Tau controls switching time and effects transconductance.\n+ Agd=80u ; Gate-Drain overlap area (5e-6 A/V^2)\n+ Area=250u ; Active area (1e-5 m^2)\n+ Bvf=2 ; Avalanche uniformity factor (1.0)\n+ BvN=5 ; Avalanche multiplication exponent (4.0)\n+ Cgs=1n2 ; Gate-Source capacitance per unit area (1.24e-8 F/cm^2)\n+ Coxd=7n ; Gate-Drain oxide capacitance per unit area (3.5e-8 F/cm^2)\n+ Jsne=8p ; Emitter saturation current density (6.5e-13 A/cm^2)\n+ Kf=5 ; Triode region factor (1.0)\n+ Kp=15 ; MOSFET transconductance (0.38 A/V^2)\n+ MuN={1k5*Tr**-2.4} ; Electron mobility (1500 cm^2/V-s) ~(T0/Tk)^-2.4\n+ MuP={450*Tr**-2.2} ; Hole mobility (450 cm^2/V-s) ~(T0/Tk)^-2.2\n+ Nb=200T ; Base doping (2e14 1/cm^3)\n+ Tau={7u1*Tr**1.8} ; Ambipolar recombination lifetime (7.1us) ~(T0/Tk)^1.8\n+ Theta=100m ; Transverse field factor (20m/V)\n+ Vt={6.7*Tr**-0.3};*(1-T25*1m6-T25**2*2u)} ; Threshold voltage (4.7V)\n+ Vtd=-1 ; Gate-Drain overlap depletion threshold (1mV)\n+ Wb=90u ; Metallurgical base width (90um)\n+ SubThres=200m ; Subthreshold current parameter (0.02)\n+ Kfn=0 ; Flicker noise coefficient (0.0)\n+ Afn=1 ; Flicker noise exponent (1.0)
TEXT 456 40 Left 2 !* my\n.MODEL IXGT40N60C2 NIGBT\n+ TAU=50.100E-9\n+ KP=14.128\n+ AREA=37.500E-6\n+ AGD=15.000E-6\n+ VT=4.2961\n+ MUN=15.550E3\n+ MUP=100\n+ BVF=4.5000\n+ KF=.5005\n+ CGS=10.121E-9\n+ COXD=20.188E-9\n+ VTD=-5
TEXT 168 88 Left 2 !.temp -40 25 150
