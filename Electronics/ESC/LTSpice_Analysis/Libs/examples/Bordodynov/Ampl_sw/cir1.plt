[Noise Spectral Density - (V/Hz� or A/Hz�)]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {524290,0,"V(onoise)"} {268435459,1,"gain"}
      X: ('K',0,425000,0,525000)
      Y[0]: ('�',1,0,1e-007,1.3e-006)
      Y[1]: ('K',1,-300,300,3600)
      Units: "V/Hz�" ('�',0,0,1,0,1e-007,1.3e-006)
      Units: "" ('K',0,0,1,-300,300,3600)
      Log: 1 0 0
      GridStyle: 1
   },
   {
      traces: 1 {268959748,0,"V(inoise)"}
      X: ('K',0,425000,0,525000)
      Y[0]: ('n',1,4e-010,1e-010,1.7e-009)
      Y[1]: ('K',1,1e+308,300,-1e+308)
      Units: "V/Hz�" ('n',0,0,1,4e-010,1e-010,1.7e-009)
      Log: 1 0 0
      GridStyle: 1
   }
}
