Version 4
SHEET 1 988 680
WIRE 176 48 80 48
WIRE 352 48 176 48
WIRE 80 80 80 48
WIRE 80 176 80 160
WIRE 80 256 80 224
FLAG 80 176 0
FLAG 80 224 IN
FLAG 176 48 OUT
FLAG 80 336 0
FLAG 352 128 0
SYMBOL voltage 80 64 R0
WINDOW 39 24 124 Left 2
SYMATTR SpiceLine vn=100n  vK={par}
SYMATTR InstName V1
SYMATTR Value Vnoise
SYMATTR Prefix xV
SYMBOL voltage 80 240 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value 0
SYMBOL res 336 32 R0
SYMATTR InstName R1
SYMATTR Value 1 noiseless
TEXT 192 240 Left 2 !.subckt Vnoise 1 2 vn=75n  vK=0.44\nA 0 0 0 0 0 0 1 2 OTA g=1k linear en={vn} enk={vk} rout=1m\n.ends
TEXT 48 400 Left 2 !.noise V(out) V2 oct 100 1 10k
TEXT 504 136 Left 2 !.step param par list 0  1 3
TEXT 96 -16 Left 4 ;F=1Hz noise_out=Vn*(1+par)^0.5
