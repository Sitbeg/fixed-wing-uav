[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"limit(0,V(oa),10)"}
      X: ('m',0,1e-006,0,0.025)
      Y[0]: ('_',0,0.001,0,10)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('_',1,0,0,0.001,0,10)
      Log: 1 1 0
   }
}
