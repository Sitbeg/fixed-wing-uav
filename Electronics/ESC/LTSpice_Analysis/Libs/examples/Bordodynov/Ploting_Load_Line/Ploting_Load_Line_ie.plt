[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 2 {34603010,0,"Ic(Q1)"} {34603011,0,"I(B1)"}
      X: (' ',1,0,0.5,5)
      Y[0]: ('m',0,0,0.001,0.014)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('m',0,0,0,0,0.001,0.014)
      Log: 0 0 0
      GridStyle: 1
   }
}
