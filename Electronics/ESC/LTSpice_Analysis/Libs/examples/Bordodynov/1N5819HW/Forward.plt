[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {68157444,0,"Ix(D2:+)"}
      X: ('n',1,0,6e-010,6.6e-009)
      Y[0]: (' ',0,-27,3,6)
      Y[1]: (' ',0,1e+308,1,-1e+308)
      Amps: (' ',0,0,0,-27,3,6)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   },
   {
      traces: 1 {34603010,0,"I(D1)"}
      X: ('n',1,0,6e-010,6.6e-009)
      Y[0]: (' ',0,-100,10,10)
      Y[1]: (' ',0,1e+308,1,-1e+308)
      Amps: (' ',0,0,0,-100,10,10)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[Operating Point]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(x)"} {524291,0,"V(y)"}
      X: (' ',1,1e-006,0.3,3)
      Y[0]: ('m',0,0,0.07,0.7)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,0,0.07,0.7)
      Log: 0 0 0
      GridStyle: 1
   }
}
