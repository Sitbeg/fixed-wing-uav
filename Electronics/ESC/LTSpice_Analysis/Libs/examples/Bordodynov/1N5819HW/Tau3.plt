[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 1 {524290,0,"V(x)"}
      X: ('n',0,0,1e-008,1e-007)
      Y[0]: (' ',0,-44,4,4)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,-44,4,4)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524291,0,"V(y)"}
      X: ('n',0,0,1e-008,1e-007)
      Y[0]: (' ',0,-55,5,5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,-55,5,5)
      Log: 0 0 0
      GridStyle: 1
   }
}
