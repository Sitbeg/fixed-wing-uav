Version 4
SymbolType CELL
RECTANGLE Normal 4 48 -4 -15
TEXT 8 -14 Left 2 B
WINDOW 0 -37 5 Bottom 2
SYMATTR Prefix X
SYMATTR ModelFile Transformers.lib
SYMATTR SpiceModel core_B
SYMATTR Value Hc=5 Bs=430m Br=100m
SYMATTR Value2 A=14u Lm=31m Lg=0
SYMATTR SpiceLine2 Fe=10Meg
PIN 0 48 NONE 8
PINATTR PinName core
PINATTR SpiceOrder 1
PIN 0 -16 NONE 8
PINATTR PinName B
PINATTR SpiceOrder 2
