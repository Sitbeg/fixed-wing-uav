[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {68157442,0,"Ix(U1:+)"}
      X: ('m',0,0,0.05,0.5)
      Y[0]: ('m',1,0,0.0001,0.0011)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Amps: ('m',0,0,1,0,0.0001,0.0011)
      Log: 0 0 0
      GridStyle: 1
   }
}
