[DC transfer characteristic]
{
   Npanes: 1
   {
      traces: 1 {524291,0,"1mV/(Ix(U2:+)-Ix(U1:+))"}
      X: ('m',0,0.07,0.03,0.3)
      Y[0]: ('K',1,-2400,200,0)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Units: "ohm" ('K',0,0,0,-2400,200,0)
      Log: 0 0 0
      GridStyle: 1
   }
}
