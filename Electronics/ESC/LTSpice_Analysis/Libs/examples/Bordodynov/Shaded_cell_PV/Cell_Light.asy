Version 4
SymbolType BLOCK
LINE Normal 0 1 -80 1
LINE Normal -32 -4 0 1
LINE Normal -32 7 -32 -4
LINE Normal 0 1 -32 7
LINE Normal 95 -15 0 -15
LINE Normal 48 -32 48 -15
LINE Normal 48 17 48 32
RECTANGLE Normal 64 11 32 17
PIN -80 0 BOTTOM 8
PINATTR PinName P_Light
PINATTR SpiceOrder 1
PIN 48 -32 RIGHT 8
PINATTR PinName A
PINATTR SpiceOrder 2
PIN 48 32 RIGHT 8
PINATTR PinName K
PINATTR SpiceOrder 3
