Version 4
SHEET 1 1620 1076
WIRE 624 288 592 288
WIRE -160 304 -272 304
WIRE -48 304 -160 304
WIRE 592 320 592 288
WIRE -272 336 -272 304
WIRE -320 416 -400 416
WIRE -48 416 -48 304
WIRE -400 432 -400 416
WIRE 592 432 592 400
WIRE 624 496 592 496
WIRE -400 528 -400 512
WIRE -272 528 -272 432
WIRE -48 528 -48 496
WIRE 592 528 592 496
WIRE -160 624 -272 624
WIRE -48 624 -160 624
WIRE 592 640 592 608
WIRE -272 656 -272 624
WIRE 624 704 592 704
WIRE -320 736 -400 736
WIRE -48 736 -48 624
WIRE 592 736 592 704
WIRE -400 752 -400 736
WIRE -400 848 -400 832
WIRE -272 848 -272 752
WIRE -48 848 -48 816
WIRE 592 848 592 816
FLAG -272 528 0
FLAG -400 528 0
FLAG -48 528 0
FLAG -160 304 d1
FLAG -272 848 0
FLAG -400 848 0
FLAG -48 848 0
FLAG -160 624 d2
FLAG 592 432 0
FLAG 592 640 0
FLAG 592 848 0
FLAG 624 288 Coss
IOPIN 624 288 Out
FLAG 624 496 Crss
IOPIN 624 496 Out
FLAG 624 704 Ciss
IOPIN 624 704 Out
SYMBOL voltage -48 400 R0
WINDOW 123 47 104 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 51 43 Left 2
WINDOW 3 36 71 Left 2
SYMATTR Value2 AC 1
SYMATTR InstName V1
SYMATTR Value {-VDS}
SYMBOL voltage -400 416 R0
WINDOW 123 42 100 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 39 44 Left 2
WINDOW 3 42 73 Left 2
SYMATTR Value2 AC 0
SYMATTR InstName V2
SYMATTR Value 0
SYMBOL voltage -48 720 R0
WINDOW 123 53 105 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 54 46 Left 2
WINDOW 3 43 73 Left 2
SYMATTR Value2 AC 0
SYMATTR InstName V3
SYMATTR Value {-VDS}
SYMBOL voltage -400 736 R0
WINDOW 123 48 102 Left 2
WINDOW 39 0 0 Left 2
WINDOW 0 46 45 Left 2
WINDOW 3 47 75 Left 2
SYMATTR Value2 AC 1
SYMATTR InstName V4
SYMATTR Value 0
SYMBOL h 592 304 R0
WINDOW 0 52 42 Left 2
WINDOW 3 53 70 Left 2
SYMATTR InstName H1
SYMATTR Value V1 {-scale}
SYMBOL h 592 512 R0
WINDOW 0 49 50 Left 2
WINDOW 3 50 79 Left 2
SYMATTR InstName H2
SYMATTR Value V2 {scale}
SYMBOL h 592 720 R0
WINDOW 0 47 42 Left 2
WINDOW 3 48 72 Left 2
SYMATTR InstName H3
SYMATTR Value V4 {-scale}
SYMBOL pmos -320 336 R0
SYMATTR InstName M1
SYMATTR Value IRLML6401_AB
SYMBOL pmos -320 656 R0
SYMATTR InstName M2
SYMATTR Value IRLML6401_AB
TEXT -600 248 Left 2 !.step param VDS  1 10  .1
TEXT -520 216 Left 2 !.ac list {freq}
TEXT -56 152 Left 2 !.param\n+ freq = 1e6\n+ scale = 1/(2*pi*freq)
TEXT -448 -376 Left 3 ;"Typical Capacitance vs. Drain-to-Source Voltage"
TEXT -624 120 Left 2 ;* VGS = 0\n * f = 1MHz\n * Vds swept from 1 to 1000V
TEXT -400 -288 Left 2 ;7/14/2012 D. W. Hawkins (dwh@ovro.caltech.edu)
TEXT 360 72 Left 2 !.option plotwinsize=0; turn off compression
TEXT 360 112 Left 2 !.save V(Ciss) V(Coss) V(Crss); variables to save
TEXT 440 184 Left 2 ;Current-controlled voltage sources,\neg., Coss = -scale*I(V1)
TEXT -632 -152 Left 2 ;Capacitance is measured based on V = IZ, with Z = 1/(sC), and s = j*2*pi*f, i.e., C = I/(j*2*pi*f*V) = -j/(2*pi*f)  for V = 1\n(which is used below for the AC amplitude). Some capacitance curve tracer example circuits also use f = 1/(2*pi)\nso that C = -j*I, however, this circuit uses the data sheet specified test frequency, and then uses the CCVS scale\nfactor to convert to capitance (the MATLAB script extracts the imaginary part).
TEXT 664 -200 VLeft 2 ;Capacitance
TEXT 896 -112 Left 2 ;Vds
TEXT 872 -336 Left 2 ;Ciss
TEXT 584 -72 Left 1 ;Use Plot Settings -> Open Plot Settings after each\nsimulation to get the plot y-axis scaled correctly.
TEXT 872 -288 Left 2 ;Coss
TEXT 872 -240 Left 2 ;Crss
TEXT -640 8 Left 2 !.model IRLML6401_AB VDMOS pchan RG=6  VTO=-1 KP=29.64  CGS=629p Rd=0.1m Rs=27m Rb=36m n=1.5  lambda=0.263457 vj=5 m=0.899 A=0.1\n+  mtriode=1 Cgdmax=400p Cgdmin=31p  Cjo=120p is=7.85E-13  ksubthres=128m tt=30n  RDS=15MEG BV=12.5 ibv=250u nbv=1.5\n+  bex=-2 vtotc=+1.7m tksubthres1=3m trs1=3m vds=12
LINE Normal 688 -136 688 -392
LINE Normal 944 -136 688 -136
LINE Normal 716 -323 931 -320
LINE Normal 716 -298 931 -263
LINE Normal 716 -250 931 -215
RECTANGLE Normal 304 944 -656 -16
RECTANGLE Normal 976 944 304 -16
ARC Normal 688 -369 773 -322 687 -346 722 -322
ARC Normal 688 -344 773 -297 687 -321 722 -297
ARC Normal 688 -296 773 -249 687 -273 722 -249
