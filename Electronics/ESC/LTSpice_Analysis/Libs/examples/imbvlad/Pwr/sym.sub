********************************************************************************
*
*  Symmetrical components analyzer, phasor approach, ABC to 120 and inverse.
*
*							Vlad, ©2008 - 2014
*
********************************************************************************
*
* [CONTENTS]
*
* abc-120	abc to 120 symmetrical components matrix
* 120-abc	120 to abc	--,,--		--,,--
*
********************************************************************************
*
*		abc-120, 120-abc - Symmetrical components matrix
*
********************************************************************************
*
* [SYMBOL]
*
* A,B,C		= three-phase inputs/outputs, floating/1 Ohm;
* M[1:3]	= magnitude I/O, floating/1 Ohm;
* A[1:3]	= angle I/O, floating/1 Ohm.
*
*
* [PARAMETERS]
*
* f	[Hz]	= working frequency;
* deg	<0,1>	= whether angles are in radians <0> or in degrees <1>;
* ic	[V]	= initial conditions tweak in case the input is balanced and
*		undistorted.
*
*
* [NOTES]
*
* - It may need .IC set in the simulation card.
*
* - For abc-120 and balanced/undistorted inputs, set {ic} to be ~0.1-1% of the
* input's magnitude.
*
************************
*
*	abc-120
*
************************
*
*		phase 'A' input
*		| phase 'B' input
*		| | phase 'C' input
*		| | | phase 'A' magnitude output
*		| | | |	   phase 'B' magnitude output
*		| | | |	   |	phase 'C' magnitude output
*		| | | |	   |	|    phase 'A' angle output
*		| | | |	   |	|    |	  phase 'B' angle output
*		| | | |	   |	|    |	  |    phase 'C' angle output
*		| | | |	   |	|    |	  |    |
.subckt abc-120 A B C M[1] M[2] M[0] A[1] A[2] A[0]
.param f=50  deg=1 ic=0
A_A 0 A 0 0 0 Acos Asin 0 MODULATOR mark={f} space=0 ic=0
TAc Acos 0 a2 0 Td={1/f} Z0=1
TAs Asin 0 a1 0 Td={1/f} Z0=1
GAs 0 reA Asin a1 4
RAs a1 0 1
RAc a2 0 1
GAc 0 imA Acos a2 4
A_B 0 B 0 0 0 Bcos Bsin 0 MODULATOR mark={f} space=0 ic=0
TBc Bcos 0 b2 0 Td={1/f} Z0=1
TBs Bsin 0 b1 0 Td={1/f} Z0=1
GBs 0 reB Bsin b1 2
RBs b1 0 1
RBc b2 0 1
GBc 0 imB Bcos b2 2
A_C 0 C 0 0 0 Ccos Csin 0 MODULATOR mark={f} space=0 ic=0
TCc Ccos 0 c2 0 Td={1/f} Z0=1
TCs Csin 0 c1 0 Td={1/f} Z0=1
GCs 0 reC Csin c1 2
RCs c1 0 1
RCc c2 0 1
GCc 0 imC Ccos c2 2
CAs reA 0 {1/f} ic={ic} Rpar=1G
CAc imA 0 {1/f} ic=0 Rpar=1G
CBs reB 0 {1/f} ic=0 Rpar=1G
CBc imB 0 {1/f} ic=0 Rpar=1G
CCs reC 0 {1/f} ic=0 Rpar=1G
CCc imC 0 {1/f} ic=0 Rpar=1G
B_MA1 0 M[1] I=hypot((v(reA)-v(reB)-v(reC))/3+(v(imC)-v(imB))/sqrt(3),(v(imA)-v(imB)-v(imC))/3+(v(reB)-v(reC))/sqrt(3)) Rpar=1
B_MB1 0 M[2] I=hypot((v(reA)-v(reB)-v(reC))/3+(v(imB)-v(imC))/sqrt(3),(v(imA)-v(imB)-v(imC))/3+(v(reC)-v(reB))/sqrt(3)) Rpar=1
B_MC1 0 M[0] I=hypot((v(reA)+2*(v(reB)+v(reC)))/3,(v(imA)+2*(v(imB)+v(imC)))/3) Rpar=1
B_AA 0 A[1] I=atan2((v(imA)-v(imB)-v(imC))+(v(reB)-v(reC))*sqrt(3),(v(reA)-v(reB)-v(reC))+(v(imC)-v(imB))*sqrt(3)) Rpar={(180/pi)**u(deg)}
B_AC 0 A[0] I=atan2(v(imA)/2+v(imB)+v(imC),v(reA)/2+v(reB)+v(reC)) Rpar={(180/pi)**u(deg)}
B_AB 0 A[2] I=atan2((v(imA)-v(imB)-v(imC))+(v(reC)-v(reB))*sqrt(3),(v(reA)-v(reB)-v(reC))+(v(imB)-v(imC))*sqrt(3)) Rpar={(180/pi)**u(deg)}
.ends abc-120
************************
*
*	120-abc
*
************************
*
*		phase 'A' output
*		| phase 'B' output
*		| | phase 'C' output
*		| | | phase 'A' magnitude input
*		| | | |	   phase 'B' magnitude input
*		| | | |	   |	phase 'C' magnitude input
*		| | | |	   |	|    phase 'A' angle input
*		| | | |	   |	|    |	  phase 'B' angle input
*		| | | |	   |	|    |	  |    phase 'C' angle input
*		| | | |	   |	|    |	  |    |
.subckt 120-abc A B C M[1] M[2] M[0] A[1] A[2] A[0]
.param f=50 deg=0 ic=0
B_A01 0 a1 I=cos(v(A[0])*(pi/180)**deg)
R_A01 a1 0 R=if(v(M[0])==0,1u,v(M[0]))
B_A11 0 a2 I=-cos(v(A[1])*(pi/180)**deg)
R_A11 a2 0 R=if(v(M[1])==0,1u,v(M[1]))
B_A21 0 a3 I=cos(v(A[2])*(pi/180)**deg)
R_A21 a3 0 R=if(v(M[2])==0,1u,v(M[2]))
B_A02 0 a5 I=sin(v(A[0])*(pi/180)**deg)
R_A02 a5 0 R=if(v(M[0])==0,1u,v(M[0]))
B_A12 0 a6 I=-sin(v(A[1])*(pi/180)**deg)
R_A12 a6 0 R=if(v(M[1])==0,1u,v(M[1]))
B_A22 0 a7 I=sin(v(A[2])*(pi/180)**deg)
R_A22 a7 0 R=if(v(M[2])==0,1u,v(M[2]))
G_A3 0 a8 a5 a6 1
G_A4 0 a8 a7 0 1
R_A2 a8 0 2
G_A1 0 a4 a1 a2 1
G_A2 0 a4 a3 0 1
R_A1 a4 0 2
A_A1 0 a4 0 0 0 0 A 0 MODULATOR mark={f} space=0 rout=2
A_A2 0 a8 0 0 0 A 0 0 MODULATOR mark={f} space=0 rout=2
G_B1 0 b1 a1 0 1
R_B1 b1 0 2
R_B2 b2 0 2
G_B6 0 b2 a5 0 1
G_B4 0 b1 0 a6 {sqrt(3)/2}
G_B5 0 b1 a2 0 0.5
G_B2 0 b1 0 a7 {sqrt(3)/2}
G_B3 0 b1 0 a3 0.5
G_B7 0 b2 a2 0 {sqrt(3)/2}
G_B8 0 b2 a6 0 0.5
G_B9 0 b2 a3 0 {sqrt(3)/2}
G_B10 0 b2 0 a7 0.5
A_B1 0 b1 0 0 0 0 B 0 MODULATOR mark={f} space=0 rout=2
A_B2 0 b2 0 0 0 B 0 0 MODULATOR mark={f} space=0 rout=2
A_C1 0 c1 0 0 0 0 C 0 MODULATOR mark={f} space=0 rout=2
A_C2 0 c2 0 0 0 C 0 0 MODULATOR mark={f} space=0 rout=2
G_C1 0 c1 a1 0 1
R_C1 c1 0 2
G_C6 0 c2 a5 0 1
R_C2 c2 0 2
G_C7 0 c2 0 a2 {sqrt(3)/2}
G_C8 0 c2 a6 0 0.5
G_C9 0 c2 0 a3 {sqrt(3)/2}
G_C10 0 c2 0 a7 0.5
G_C4 0 c1 a6 0 {sqrt(3)/2}
G_C5 0 c1 a2 0 0.5
G_C2 0 c1 a7 0 {sqrt(3)/2}
G_C3 0 c1 0 a3 0.5
.ends 120-abc
********************************************************************************
*
* [UPDATES]
*
* 2014.04.13	- Corrected and optimized 120-abc.
*
********************************************************************************
