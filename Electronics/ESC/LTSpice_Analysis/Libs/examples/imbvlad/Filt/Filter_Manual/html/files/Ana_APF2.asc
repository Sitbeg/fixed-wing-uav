Version 4
SHEET 1 1240 680
WIRE -176 160 -192 160
WIRE 16 160 -176 160
WIRE 368 160 128 160
WIRE -192 256 -192 160
FLAG -192 336 0
FLAG 368 160 out
FLAG -176 160 in
FLAG 16 192 0
SYMBOL Filter 80 160 R0
WINDOW 40 -4 143 Center 2
WINDOW 39 -1 106 Center 2
WINDOW 3 1 69 Center 2
SYMATTR SpiceLine2 G=1 Asc=2 Ap=1 As=40 N=4
SYMATTR SpiceLine f0=2k fc=0 BWp=1k BWs=1k
SYMATTR InstName U1
SYMATTR SpiceModel Butterworth
SYMBOL voltage -192 240 R0
SYMATTR InstName V1
SYMATTR Value sin 0 1 1k ac 1
TEXT 384 248 Left 2 !.ac dec 1k 10 100k
TEXT 384 216 Left 2 ;.tran 0 10m 0 1u
TEXT 376 296 Left 2 !.save v(*)
