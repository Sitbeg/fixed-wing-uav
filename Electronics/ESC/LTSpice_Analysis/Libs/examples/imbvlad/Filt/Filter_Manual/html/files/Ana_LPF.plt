[Transient Analysis]
{
   Npanes: 1
   {
      traces: 3 {524290,0,"V(out)"} {524291,0,"V(in)"} {524292,0,"V(out,in)"}
      X: ('m',0,0,0.005,0.05)
      Y[0]: (' ',1,-1.2,0.2,1.2)
      Y[1]: ('K',1,1e+308,300,-1e+308)
      Volts: (' ',0,0,1,-1.2,0.2,1.2)
      Log: 0 0 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('K',0,10,0,100000)
      Y[0]: (' ',0,3.16227766016838e-028,50,1)
      Y[1]: ('K',1,-2700,300,600)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
