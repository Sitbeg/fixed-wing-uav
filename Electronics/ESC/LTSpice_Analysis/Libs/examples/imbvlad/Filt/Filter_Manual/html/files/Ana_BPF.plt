[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('M',1,100000,0,1e+007)
      Y[0]: (' ',0,3.16227766016838e-014,30,31622.7766016838)
      Y[1]: ('K',1,-1200,200,1200)
      Volts: (' ',0,0,0,-18,3,18)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[Transient Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('m',1,0,0.0001,0.001)
      Y[0]: (' ',0,-18,3,18)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,-18,3,18)
      Log: 0 0 0
      GridStyle: 1
   }
}
