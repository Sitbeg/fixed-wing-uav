Version 4
SHEET 1 1240 680
WIRE -96 128 -176 128
WIRE 64 160 48 160
WIRE 160 160 64 160
WIRE 208 160 160 160
WIRE 416 160 320 160
WIRE -176 224 -176 128
WIRE 272 224 272 192
WIRE 272 240 272 224
FLAG -176 304 0
FLAG 416 160 out
FLAG 160 160 in
FLAG 208 192 0
FLAG 64 240 0
DATAFLAG 272 224 ""
SYMBOL Filter 272 160 R0
WINDOW 40 47 219 Center 2
WINDOW 39 45 183 Center 2
WINDOW 3 56 108 Center 2
WINDOW 123 49 144 Center 2
SYMATTR SpiceLine2 G=10 Asc=3.0103 Ap={Ap} As={As} N={N}
SYMATTR SpiceLine f0=2k fc=1Meg BWp=150k BWs=175k
SYMATTR Value sigma=0.5 nT=-1 Zin=1 Zout=1
SYMATTR Value2 Win=1 Choice=1 sim=2m test=0 SH=1 Fact=1
SYMATTR InstName U1
SYMATTR SpiceModel Cauer
SYMBOL SpecialFunctions\\modulate -96 128 R0
SYMATTR InstName A1
SYMATTR SpiceLine mark=1e6 space=0
SYMBOL voltage -176 208 R0
WINDOW 3 -66 135 Left 2
SYMATTR Value pwl 0.2m 0.8 2m 1.2
SYMATTR InstName V1
SYMBOL current 64 240 M180
WINDOW 0 27 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName I1
SYMATTR Value ac 1
TEXT 640 328 Left 2 ;.tran 2m
TEXT 560 136 Left 2 !.param N=0 Ap=0.01 As=120
TEXT 632 200 Left 2 ;.step param N list 0 15
TEXT 632 232 Left 2 ;.step param Ap list 0 0.01
TEXT 632 264 Left 2 ;.step param As list 0 120
TEXT 640 368 Left 2 !.ac dec 8192 100k 10Meg
TEXT 640 416 Left 2 !.save v(out) v(in)
