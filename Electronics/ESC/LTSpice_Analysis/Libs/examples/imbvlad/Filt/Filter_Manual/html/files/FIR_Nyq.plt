[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('m',0,0.001,0.1,0.999)
      Y[0]: (' ',0,1e-009,20,100)
      Y[1]: ('K',1,-2200,200,0)
      Volts: ('m',0,0,0,-0.08,0.02,0.18)
      Log: 0 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
