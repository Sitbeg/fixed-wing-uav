Version 4
SHEET 1 1240 680
WIRE -176 160 -192 160
WIRE 16 160 -176 160
WIRE 368 160 128 160
WIRE -192 256 -192 160
FLAG -192 336 0
FLAG 368 160 out
FLAG -176 160 in
FLAG 16 192 0
SYMBOL Filter 80 160 R0
WINDOW 40 -4 109 Center 2
WINDOW 39 -1 73 Center 2
SYMATTR SpiceLine2 G=1 Asc={Asc} Ap=1 As=40 N=29
SYMATTR SpiceLine f0=10k fc=0 BWp=159.155 BWs=1k
SYMATTR InstName U1
SYMBOL voltage -192 240 R0
SYMATTR InstName V1
SYMATTR Value sine 0 1 1k ac 1
TEXT 376 320 Left 2 !.ac dec 1k 10 100k
TEXT 376 352 Left 2 ;.tran 10m
TEXT 376 208 Left 2 !.param Asc=0
TEXT 376 256 Left 2 ;.step param Ac list 0 3
TEXT 240 352 Left 2 !.save v(*)
