[AC Analysis]
{
   Npanes: 1
   {
      traces: 1 {524290,0,"V(out)"}
      X: ('K',1,1,300,3199)
      Y[0]: (' ',0,3.16227766016838e-006,10,1)
      Y[1]: (' ',0,-210,30,120)
      Log: 0 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(out)"} {524291,0,"V(in)"}
      X: ('m',0,0,0.004,0.04)
      Y[0]: (' ',1,0,0.2,2)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.2,2)
      Log: 0 0 0
      GridStyle: 1
   }
}
