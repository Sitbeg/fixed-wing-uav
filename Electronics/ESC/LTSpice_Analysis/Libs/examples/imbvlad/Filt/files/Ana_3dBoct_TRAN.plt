[Transient Analysis]
{
   Npanes: 1
   {
      traces: 2 {524290,0,"V(out)"} {524291,0,"V(filt)"}
      X: ('m',0,0,0.01,0.1)
      Y[0]: ('m',0,-0.7,0.1,0.7)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: ('m',0,0,0,-0.7,0.1,0.7)
      Log: 0 0 0
      GridStyle: 1
   }
}
