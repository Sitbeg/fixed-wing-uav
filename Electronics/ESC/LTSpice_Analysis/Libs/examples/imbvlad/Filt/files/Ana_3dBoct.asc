Version 4
SHEET 1 1476 680
WIRE 368 -144 208 -144
WIRE 160 -128 80 -128
WIRE 368 -16 208 -16
WIRE 80 0 80 -128
WIRE 160 0 80 0
WIRE 80 160 80 0
WIRE 80 160 16 160
WIRE 144 160 80 160
WIRE 368 160 256 160
FLAG 16 240 0
FLAG 368 160 out
FLAG 144 192 0
FLAG 160 48 0
FLAG 208 64 0
FLAG 368 -16 comp
FLAG 160 -80 0
FLAG 208 -64 0
FLAG 368 -144 comp2
SYMBOL Filter 208 160 R0
WINDOW 40 0 127 Center 2
WINDOW 3 -4 66 Center 2
WINDOW 39 -5 98 Center 2
SYMATTR SpiceLine2 G={G} N=40
SYMATTR Value sigma={sigma}
SYMATTR SpiceLine fc={fc}
SYMATTR SpiceModel 3dBoct
SYMATTR InstName U1
SYMBOL voltage 16 144 R0
WINDOW 0 37 38 Left 2
SYMATTR InstName V1
SYMATTR Value ac 1
SYMBOL e 208 -32 R0
WINDOW 0 33 40 Left 2
WINDOW 3 33 85 Left 2
SYMATTR InstName E_low-pass
SYMATTR Value Laplace={G}/sqrt(s/2/pi/{fc}+1)
SYMBOL e 208 -160 R0
WINDOW 0 33 40 Left 2
WINDOW 3 33 85 Left 2
SYMATTR InstName E_integrator
SYMATTR Value Laplace={G}/sqrt(s/2/pi/{fc})
TEXT -16 360 Left 2 !.ac dec 2048 10 100k
TEXT 496 216 Left 2 !.param G=1 fc=100\n.param sigma=0.5
TEXT 496 296 Left 2 ;.step param sigma 0 1 0.1
TEXT 840 48 Left 2 !.meas v1 find -20*log10(v(out)) at=1k\n.meas v2 find -20*log10(v(out)) at=2k\n.meas v3 find -20*log10(v(out)) at=4k\n.meas v4 find -20*log10(v(out)) at=8k\n.meas v5 find -20*log10(v(out)) at=16k\n.meas v6 find -20*log10(v(out)) at=32k\n.meas v7 find -20*log10(v(out)) at=64k
TEXT 848 272 Left 2 !.meas slope1 param v1-v2\n.meas slope2 param v2-v3\n.meas slope3 param v3-v4\n.meas slope4 param v4-v5\n.meas slope5 param v5-v6\n.meas slope6 param v6-v7
TEXT -16 392 Left 2 !.opt meascplxfmt=cartesian
TEXT 720 -8 Left 2 ;Press Ctrl+L to check the log for the values of the slope
TEXT -16 320 Left 2 !.save v(*)
