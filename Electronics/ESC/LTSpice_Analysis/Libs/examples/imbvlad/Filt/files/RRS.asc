Version 4
SHEET 1 1240 680
WIRE -64 160 -96 160
WIRE 112 160 -64 160
WIRE 368 160 224 160
WIRE -96 256 -96 160
FLAG -96 336 0
FLAG 368 160 out
FLAG -64 160 in
FLAG 112 192 0
SYMBOL Filter 176 160 R0
WINDOW 40 -4 104 Center 2
WINDOW 39 -2 68 Center 2
WINDOW 38 -14 -51 Center 2
SYMATTR SpiceLine2 G=1 Asc=3 Ap=1 As=40 N={N}
SYMATTR SpiceLine f0={f0*N} fc=250 BWp=100 BWs=200
SYMATTR SpiceModel RRS
SYMATTR Value2 Win=1 Choice=1 sim=1 test=0 SH=0 Fact=1
SYMATTR InstName U1
SYMBOL voltage -96 240 R0
WINDOW 0 34 78 Left 2
WINDOW 3 23 108 Left 2
SYMATTR InstName V1
SYMATTR Value sin 1 1 {f0} 0 0 -0 ac 1
TEXT 464 288 Left 2 !.tran 0 1 0
TEXT 464 152 Left 2 !.param f0=50 N=32
TEXT 464 320 Left 2 ;.ac lin 4096 1 {f0*N/2-1}
TEXT 464 224 Left 2 !.save v(in) v(out)
