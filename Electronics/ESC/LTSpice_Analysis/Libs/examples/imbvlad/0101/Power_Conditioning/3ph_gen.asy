Version 4
SymbolType CELL
LINE Normal -48 48 -48 -48
LINE Normal 48 48 48 -48
LINE Normal -48 -48 48 -48
LINE Normal -48 48 48 48
LINE Normal 32 14 -28 14
LINE Normal 29 16 32 14
LINE Normal 32 14 29 12
LINE Normal -26 -30 -26 16
LINE Normal -24 -27 -26 -30
LINE Normal -26 -30 -28 -27
LINE Normal 28 26 29 20
LINE Normal 30 23 28 23
LINE Normal 42 -26 48 -32
LINE Normal 48 -32 42 -38
LINE Normal 48 0 42 -6
LINE Normal 42 6 48 0
LINE Normal 42 38 48 32
LINE Normal 48 32 42 26
RECTANGLE Normal -18 14 -19 -30
RECTANGLE Normal -13 14 -12 -25
RECTANGLE Normal -7 14 -6 -16
RECTANGLE Normal -1 14 0 -5
RECTANGLE Normal 5 14 6 6
RECTANGLE Normal 11 14 12 11
RECTANGLE Normal 17 14 18 9
RECTANGLE Normal 23 14 24 12
ARC Normal 29 19 31 21 31 20 29 20
WINDOW 0 -48 -55 Left 2
SYMATTR SpiceLine h1=0 h2=0 h3=0 phi1=0 phi2=0 phi3=0
SYMATTR SpiceLine2 a=0 b=0 c=0 d=0 e=0 p=1 q=0 xp=1 xq=1
SYMATTR Prefix x
SYMATTR SpiceModel 3ph_gen
SYMATTR Description Three-phase, voltage/current harmonics generator +. See the .sub file for description.
SYMATTR ModelFile pwr.sub
SYMATTR Value2 dc1=0 dc2=0 dc3=0 A1=1 A2=1 A3=1
SYMATTR Value sym=1 f=50 amp=325 phi=0 Ro=1 N=-51
PIN 48 -32 NONE 3
PINATTR PinName 1
PINATTR SpiceOrder 1
PIN 48 0 NONE 3
PINATTR PinName 2
PINATTR SpiceOrder 2
PIN 48 32 NONE 3
PINATTR PinName 3
PINATTR SpiceOrder 3
PIN -48 0 VTOP 1
PINATTR PinName NUL
PINATTR SpiceOrder 4
PIN -32 48 BOTTOM 1
PINATTR PinName FM
PINATTR SpiceOrder 5
PIN 0 48 BOTTOM 1
PINATTR PinName AM
PINATTR SpiceOrder 6
PIN 32 48 BOTTOM 1
PINATTR PinName PM
PINATTR SpiceOrder 7
