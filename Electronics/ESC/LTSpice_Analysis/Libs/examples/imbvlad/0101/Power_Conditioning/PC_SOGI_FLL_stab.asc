Version 4
SHEET 1 2404 680
WIRE 1536 -592 576 -592
WIRE 752 -528 704 -528
WIRE 576 -512 576 -592
WIRE 320 -480 256 -480
WIRE 544 -480 320 -480
WIRE 704 -480 704 -528
WIRE 704 -480 624 -480
WIRE 816 -480 704 -480
WIRE 1008 -480 864 -480
WIRE 1216 -480 1088 -480
WIRE 1376 -480 1296 -480
WIRE 1536 -480 1536 -592
WIRE 1536 -480 1440 -480
WIRE 1600 -480 1536 -480
WIRE 1040 -416 1040 -448
WIRE 1600 -416 1040 -416
WIRE 32 -384 0 -384
WIRE 176 -384 176 -400
WIRE 176 -384 32 -384
WIRE 704 -368 704 -480
WIRE 32 -352 32 -384
WIRE 64 -352 32 -352
WIRE 208 -352 208 -400
WIRE 208 -352 112 -352
WIRE 304 -352 208 -352
WIRE 1040 -336 1040 -416
WIRE 1040 -336 736 -336
WIRE 1104 -336 1040 -336
WIRE 1376 -336 1184 -336
WIRE 1536 -336 1536 -480
WIRE 1536 -336 1440 -336
WIRE 32 -320 32 -352
WIRE 304 -320 32 -320
WIRE 1248 -304 1248 -448
WIRE 1248 -304 1152 -304
WIRE 240 -288 240 -400
WIRE 304 -288 240 -288
WIRE 240 -272 240 -288
WIRE 704 -96 704 -288
WIRE 64 -64 -32 -64
WIRE 336 -64 112 -64
WIRE 672 -64 416 -64
WIRE 816 -64 752 -64
WIRE 1056 -64 880 -64
WIRE 1184 -64 1136 -64
WIRE 1248 -64 1248 -304
WIRE 1248 -64 1184 -64
WIRE 48 16 -32 16
WIRE 208 16 112 16
WIRE 368 16 368 -32
WIRE 368 16 288 16
WIRE 1088 16 1088 -32
WIRE 48 80 -32 80
WIRE 240 80 240 48
WIRE 240 80 112 80
FLAG 320 -480 in
FLAG 240 -192 0
FLAG 1600 -480 v
IOPIN 1600 -480 Out
FLAG 1600 -416 qv
IOPIN 1600 -416 Out
FLAG 1088 96 0
FLAG -32 16 v
IOPIN -32 16 In
FLAG -32 80 qv
IOPIN -32 80 In
FLAG 752 -528 err
IOPIN 752 -528 Out
FLAG -32 -64 err
IOPIN -32 -64 In
FLAG 160 -448 0
FLAG 304 -352 amplitude
IOPIN 304 -352 Out
FLAG 304 -320 frequency
IOPIN 304 -320 Out
FLAG 304 -288 phase
IOPIN 304 -288 Out
FLAG 1184 -64 w
SYMBOL Gain 832 -480 R0
SYMATTR SpiceLine G=k
SYMATTR InstName U6
SYMBOL Integ_r 1408 -480 R0
WINDOW 39 7 -27 Left 2
SYMATTR SpiceLine tau=tau
SYMATTR InstName U4
SYMBOL Integ_r 1408 -336 M0
WINDOW 39 11 -26 Left 2
SYMATTR SpiceLine tau=tau
SYMATTR InstName U7
SYMBOL Integ_r 848 -64 R0
SYMATTR SpiceLine tau=tau
SYMATTR InstName U11
SYMBOL Gain 80 -64 R0
SYMATTR SpiceLine G=k
SYMATTR InstName U13
SYMBOL 3ph_gen 208 -448 R0
SYMATTR InstName U19
SYMATTR SpiceLine h1=3  h2=9  h3=0  phi1=0  phi2=0  phi3=0
SYMATTR SpiceLine2 a=1 b=1/3 c=pi/6 d=1/9 e=pi/2 p=1 q=0 xp=1.5
SYMATTR Value sym=1 f=0 amp=0 phi=0 Ro=1 N=11
SYMBOL Disturb -16 -384 R0
SYMATTR SpiceLine A=f B={f/10} delay=0.2 sigma=0.35 phi=pi/10 xp=1
SYMATTR InstName U20
SYMATTR SpiceLine2 f=5 phi=0 sq=1 skew=30
SYMBOL Gain 80 -352 R0
WINDOW 39 15 17 Left 2
SYMATTR SpiceLine G=1.5/f
SYMATTR InstName U21
SYMBOL Math1 80 16 R0
WINDOW 39 0 28 Center 2
SYMATTR SpiceModel pwr
SYMATTR InstName U30
SYMBOL Math1 80 80 R0
WINDOW 39 0 28 Center 2
SYMATTR SpiceModel pwr
SYMATTR InstName U31
SYMBOL Math2r 240 16 R0
SYMATTR SpiceModel +
SYMATTR InstName U1
SYMBOL Math2r 368 -64 R0
WINDOW -1 0 28 Center 2
SYMATTR SpiceModel /
SYMATTR InstName U2
SYMATTR SpiceLine2 min=50m
SYMBOL Math2r 704 -64 M180
SYMATTR InstName U3
SYMATTR SpiceModel +
SYMBOL Math2r 1088 -64 R0
SYMATTR SpiceModel +
SYMATTR InstName U5
SYMBOL Math2r 704 -336 M90
SYMATTR SpiceModel x
SYMATTR InstName U9
SYMBOL Math2r 576 -480 M180
SYMATTR InstName U10
SYMBOL Math2r 1040 -480 R0
SYMATTR InstName U12
SYMBOL Math2r 1152 -336 M0
SYMATTR SpiceModel x
SYMATTR InstName U14
SYMBOL Math2r 1248 -480 R0
SYMATTR SpiceModel x
SYMATTR InstName U15
SYMBOL voltage 1088 0 R0
SYMATTR InstName V1
SYMATTR Value {wff}
SYMBOL voltage 240 -288 R0
SYMATTR InstName V2
SYMATTR Value pulse 0 {pi/2} 666m 1u
TEXT 1304 -184 Left 2 !.tran 1
TEXT 1304 -8 Left 2 !.param f=50 k=sqrt(2) tau=1 wff=2*pi*f
TEXT 1304 -120 Left 2 !.four {f} 25 v(v)\n.four {f} 25 v(qv)\n.four {f} 25 v(in)
TEXT -96 -616 Left 2 ;Input distorted by the 5th, 7th and 11th\nharmonics. The three flags show the\nvariations of frequency, amplitude and\nphase.
TEXT 984 -552 Left 2 ;SOGI - second order generalized integrator
TEXT 528 -616 Left 2 ;Adaptive filter based on SOGI
TEXT 528 24 Left 2 ;FLL - frequency-locked loop
TEXT 728 -776 Center 4 ;SOGI-FLL
TEXT 720 -720 Center 2 ;(grid synchronization by the use of second-order generalized integrators)
TEXT -96 232 Left 2 ;FLL gain normalization
TEXT 1304 104 Left 2 ;Normally, there would be many SOGI-QSG (quadrature\nsignal generator) blocks, each tuned to a specific\nfrequency. Here, only one cell is presented, tuned to\nthe fundamental frequency. Even so, the SOGI acts\nas a band-pass filter and it can be seen that, even if\nthe input has harmonics or is distorted -- as is the case\nhere -- by a phase-jump, the output filters them to a\ncertain degree. Also see the error log for the results of\nthe .four commands.
TEXT 624 88 Left 2 ;All the blocks:\n- SOGI\n- Adaptive filter\n- FLL\n- FLL gain normalization\nform the so-called SOGI-FLL
TEXT 1600 -152 Left 2 ;FFT on the:\n<--- in-phase output\n<--- quadrature output\n<--- input voltage
TEXT 1304 40 Left 2 ;.param tripdv=1 tripdt=0.1m
TEXT 1672 40 Left 2 ;<-- uncomment this for slower, but more detailed waveforms
LINE Normal 512 -432 512 -640 1
LINE Normal 960 -432 512 -432 1
LINE Normal 960 -240 960 -432 1
LINE Normal 1808 -240 960 -240 1
LINE Normal 1808 -640 1808 -240 1
LINE Normal 512 -640 1808 -640 1
RECTANGLE Normal 496 -160 -112 -640 2
RECTANGLE Normal 1792 -256 976 -576 2
RECTANGLE Normal 944 48 512 -416 2
RECTANGLE Normal 496 256 -112 -144 2
