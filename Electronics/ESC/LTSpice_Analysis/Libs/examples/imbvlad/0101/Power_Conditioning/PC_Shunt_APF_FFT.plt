[Transient Analysis]
{
   Npanes: 1
   {
      traces: 4 {268959746,0,"V(alpha*)"} {524291,0,"V(test_a)"} {524292,0,"V(beta*)"} {524293,0,"V(test_b)"}
      X: ('m',0,0,0.01,0.1)
      Y[0]: (' ',1,-1.4,0.2,1)
      Y[1]: (' ',0,1e+308,2,-1e+308)
      Volts: (' ',0,0,1,-1.4,0.2,1)
      Log: 0 0 0
      GridStyle: 1
   }
}
