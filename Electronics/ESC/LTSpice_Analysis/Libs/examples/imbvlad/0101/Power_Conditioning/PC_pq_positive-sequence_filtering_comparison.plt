[Transient Analysis]
{
   Npanes: 1
   {
      traces: 7 {524290,0,"V(butterworth)"} {524291,0,"V(cauer)"} {524292,0,"V(bessel)"} {524293,0,"V(movingaverage)"} {524294,0,"V(kaiser)"} {524295,0,"V(gaussian)"} {524296,0,"V(p`)"}
      X: ('m',0,0,0.03,0.3)
      Y[0]: (' ',1,0,0.1,1.3)
      Y[1]: ('m',1,1e+308,0.0002,-1e+308)
      Volts: (' ',0,0,1,0,0.1,1.3)
      Log: 0 0 0
   }
}
