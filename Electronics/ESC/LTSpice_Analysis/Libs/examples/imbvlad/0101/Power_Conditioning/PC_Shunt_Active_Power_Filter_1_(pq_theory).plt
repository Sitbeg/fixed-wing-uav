[Transient Analysis]
{
   Npanes: 1
   {
      traces: 3 {68157444,0,"Ix(U32:a1)"} {524291,1,"V(power)*1A"} {524290,1,"V(power_comp)*1A"}
      X: ('m',0,0,0.02,0.2)
      Y[0]: (' ',0,-50,10,110)
      Y[1]: ('K',0,6000,3000,42000)
      Amps: (' ',0,0,0,-50,10,110)
      Units: "W" ('K',0,0,0,6000,3000,42000)
      Log: 0 0 0
   }
}
