Version 4
SymbolType CELL
LINE Normal -16 16 -16 -16
LINE Normal 16 16 -16 16
LINE Normal 16 -16 16 16
LINE Normal -16 -16 16 -16
TEXT 0 -1 Center 2 c
WINDOW 39 -16 26 Left 2
SYMATTR SpiceLine c=1
SYMATTR Prefix x
SYMATTR SpiceModel const
SYMATTR Description Constant value, but can have expressions, as well (it's a B-source).
SYMATTR ModelFile mathfunc.sub
PIN 16 0 NONE 8
PINATTR PinName out
PINATTR SpiceOrder 1
