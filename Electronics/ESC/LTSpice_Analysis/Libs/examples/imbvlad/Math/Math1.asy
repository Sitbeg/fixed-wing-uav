Version 4
SymbolType CELL
LINE Normal -32 16 -32 -16
LINE Normal 32 16 -32 16
LINE Normal 32 -16 32 16
LINE Normal -32 -16 32 -16
LINE Normal -28 -4 -32 -4
LINE Normal -24 0 -28 -4
LINE Normal -28 4 -24 0
LINE Normal -32 4 -28 4
WINDOW 38 0 0 Center 2
SYMATTR SpiceModel abs
SYMATTR Prefix x
SYMATTR Description Single-input mathematical functions. Select one by either right-clicking on the symbol and choosing it from the drop-down menu, or by directly renaming it.
SYMATTR ModelFile mathfunc1.sub
SYMATTR SpiceLine y=2
SYMATTR SpiceLine2 phi=0
PIN -32 0 NONE 8
PINATTR PinName in
PINATTR SpiceOrder 1
PIN 32 0 NONE 8
PINATTR PinName out
PINATTR SpiceOrder 2
