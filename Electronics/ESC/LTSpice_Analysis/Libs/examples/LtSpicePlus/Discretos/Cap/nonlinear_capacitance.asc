Version 4
SHEET 1 1668 1860
WIRE 464 192 192 192
WIRE -192 208 -464 208
WIRE 192 240 192 192
WIRE -464 256 -464 208
WIRE 464 256 464 192
WIRE -192 272 -192 208
WIRE 192 368 192 320
WIRE 464 368 464 320
WIRE -464 384 -464 336
WIRE -192 384 -192 336
WIRE -192 576 -464 576
WIRE 464 576 192 576
WIRE -464 624 -464 576
WIRE 192 624 192 576
WIRE 464 624 464 576
WIRE -192 640 -192 576
WIRE -464 752 -464 704
WIRE -192 752 -192 704
WIRE 192 752 192 704
WIRE 464 752 464 688
WIRE -192 960 -464 960
WIRE 464 960 192 960
WIRE -464 1008 -464 960
WIRE 192 1008 192 960
WIRE -192 1024 -192 960
WIRE 464 1024 464 960
WIRE -464 1136 -464 1088
WIRE -192 1136 -192 1088
WIRE 192 1136 192 1088
WIRE 464 1136 464 1088
FLAG -464 384 0
FLAG -192 384 0
FLAG -192 208 Cd
FLAG -464 752 0
FLAG -192 752 0
FLAG -192 576 Ceq
FLAG 192 752 0
FLAG 464 752 0
FLAG 464 576 Ctab
FLAG -464 1136 0
FLAG -192 1136 0
FLAG -192 960 Csens
FLAG 192 1136 0
FLAG 464 1136 0
FLAG 464 960 Cpoly
FLAG 192 368 0
FLAG 464 368 0
FLAG 464 192 Cdcharge
SYMBOL varactor -208 336 M180
WINDOW 0 24 72 Left 0
WINDOW 3 24 0 Left 0
SYMATTR InstName D1
SYMATTR Value MV209
SYMBOL cap -208 640 R0
WINDOW 3 -285 181 Left 2
SYMATTR InstName Ceq
SYMATTR Value Q=IF(x>0, x*125p/((1+x/1.4)**0.71), x*125p)
SYMBOL voltage -464 240 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value PWL(0 -0.1 50.1u 50)
SYMBOL voltage -464 608 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V3
SYMATTR Value PWL(0 -0.1 50.1u 50)
SYMBOL cap 448 624 R0
WINDOW 123 -185 -84 Left 0
WINDOW 3 -463 163 Left 2
SYMATTR Value2 6,225p,12,300p,24,385p,35,430p,100,10000p)
SYMATTR InstName Ctab
SYMATTR Value Q=table(x,-1,-100p,.7,70p,1.5,110p,3,165p,
SYMBOL voltage 192 608 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V4
SYMATTR Value PWL(0 -0.1 50.1u 50)
SYMBOL cap -208 1024 R0
WINDOW 3 -272 171 Left 2
SYMATTR InstName Csens
SYMATTR Value Q=IF(x>0, 125p*25/24*ln(1+x*24/25), x*125p)
SYMBOL voltage -464 992 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V5
SYMATTR Value PWL(0 -0.1 50.1u 50)
SYMBOL cap 448 1024 R0
WINDOW 3 -365 145 Left 2
SYMATTR InstName Cpoly
SYMATTR Value Q=IF(x>0, 125p*(x-0.016*x*x), x*125p)
SYMBOL voltage 192 992 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V6
SYMATTR Value PWL(0 -0.1 50.1u 50)
SYMBOL cap 448 256 R0
WINDOW 3 -607 159 Left 2
SYMATTR InstName Cdcharge
SYMATTR Value Q=IF(x>0, 125p*0.75/(1-0.908)*(-1+(1+x/0.75)**(1-0.908)), x*125p)
SYMBOL voltage 192 224 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V2
SYMATTR Value PWL(0 -0.1 50.1u 50)
TEXT -792 176 Left 2 !.model MV209 D(Is=.43p Rs=1 Bv=35 Ibv=10u Isr=4.4p Cjo=125p Vj=.75 M=.908)
TEXT -1312 168 Left 1 ;Simulation Of Nonlinear Capacitors in LTSPICE      \nWritten by Helmut Sennewald, 4/14/2004\n \nLTSPICE allows to specify charge Q(v) to simulate any nonlinear capacitance C(v). \nTherefore a formula must be developed which fits charge versus voltage. \nThis have to be done by any integration method and addtional curve fitting \nif no exact integral function can be developed.\n   Q(x) = Integral(C(v)dv)   from 0 to x\n  \nA linear voltage ramp is applied to the capacitors to provide an easy formula for \nplotting the capacitance.\n  C = -I(V1)*1u*1s/1V\n \n1. The first model is the SPICE diode model. It calculates C(Vd) with the following formula.\n   IF Vd < FC*Vj       Cj = Cj0 / (1 - Vd/Vj)**M\n   IF Vd > FC*Vj       Cj = Cj0 * (1-FC)**(-1-M) * (1 - FC*(1+M) + M*Vd/Vj)\nThe second term applies only if the diode is biased in forward direction.\n Be aware that a positive value of Vd means biased in forward direction.  \n \n2. The second circuit uses an exact replacement of the diode capacitance with a charge formula. \nThe capacitance must be integrated to get the charge versus voltage. \n   C(v) = Cjo/(1-Vd/Vj)^M      here  pos. Vd means diode forward direction\n   Q(x) = Integral(Cj(v)dv)    from 0 to x\n   Q(x) =  Cjo * Vj/(1-M) * ( 1 - (1-Vd/Vj)^(1-M) )  from literature\nWe have here to change some signs, because of the +/- current definition and the opposite \npolarity of Vd for the capacitor.\n   Q(x) =  Cjo * Vj/(1-M) * ( -1 + (1+Vd/Vj)^(1-M) )\n \n3. The third circuit uses an approximation of the diode capacitance with a charge formula. \nA formula must be developed which fits charge versus voltage. \n   Q(x) = Integral(C(v)dv)   from 0 to x\nOften there exist no direct integration formula. So I tried to fit manually with the following formula.  \n   Q(x) =x*125p/((1+x/1.4)**0.71)\nThis fit is excellent compared to the SPICE diode model of the MV209. \nIt seems that  Q(x)=x*Cj0/((1+a*x)**b) is a good formula for such an approximation.\n  \n4. The fourth method uses a table to approximate the charge.\nThe resulting capacitance is a stepped function, because LTSPICE does a \nlinear interpolation between the given data values and thus C=dQ/dV is constant\nbetween given table points.\n \n5. The fifth example shows how integration have to be performed for a given capacitance formula.\n     C(V) = 125pF/(1+V*24/25)   \n     C(x) = 125pF/(1+x*a24/25)       with x = V\n  We get Q(x) with integration of C(x).\n     Q(x) = 125p*25/24*log(1+24/25*x)       log() = natural logarithm ln()\n  \n6. And last a replacement for those nonlinear SPICE POLY-capacitors.\nLTSPICE doesn't support this type of capacitors directly, but we can \nemulate this with an equivalent charge model.\n   Cxx n1 n2 POLY c0 [c1 [c2 [c3 ....]]]                             []=optional parameters\n   Q = Integral(c0+c1*x+c2*x^2+c3*x^3+...)dx  from 0 to x\n   Q = c0 * (x+ c1/2*x^2 + c2/3*x^3 + ....)\nExample:  capacitance changes linearly from 125pF@0V to 5pF@30V\n   C = 125pF*(1-V*0.032)     c0 = 125pF,   c1 = -125pF*0.032\n   Q = 125pC*(x-0.016*x*x)       x is voltage in Volt, C is Coloumb
TEXT -184 528 Left 3 !.tran 0 30.1u 0.1u 0.01u
TEXT 24 504 Left 0 ;MV209 with charge Q from a table. These table values are from Mike Engelhardt.
TEXT -472 496 Left 0 ;MV209 approximation with charge Q from formula
TEXT -72 952 Left 0 ;C(V) = 125pF/(1+V*24/25)\nQ=Integral(C(v)dv) from 0 to V\nQ=125pC*25/24*log(1+V*24/25)\nLTSPICE formula: Q=125p*25/24*log(1+x*24/25)
TEXT -464 904 Left 0 ;Another example, C(V) = 125pF/(1+V)
TEXT 0 248 Left 0 ;MV209:\n 3V     29pF\n10V    11pF\n25V     5pF
TEXT 320 856 Left 0 ;C = Co*(1-V*0.032)     c0=12p5, c1=-125p*0.032\nQ = 125p*(x - 0.032*x*x)
TEXT 192 904 Left 0 ;Another example, C(V) = 125pF/(1+V)
TEXT -144 456 Left 2 ;MV209 equivalent capacitor with charge Q from exact integration
