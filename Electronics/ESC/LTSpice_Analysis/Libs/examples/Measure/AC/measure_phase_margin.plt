[AC Analysis]
{
   Npanes: 1
   {
      traces: 4 {524290,0,"V(out)"} {3,0,"im(V(out))"} {34603012,0,"I(V1)"} {5,0,"ph(V(out))"}
      X: ('M',1,10,0,1e+008)
      Y[0]: (' ',0,1e-006,20,1e+006)
      Y[1]: (' ',0,-280,40,200)
      Volts: (' ',0,0,1,-1.2,0.4,3.2)
      Log: 1 2 0
      GridStyle: 1
      PltMag: 1
      PltPhi: 1 0
   }
}
