Version 4
SHEET 1 1628 680
WIRE 32 64 -16 64
WIRE 192 64 112 64
WIRE 352 64 272 64
WIRE -16 80 -16 64
WIRE 352 80 352 64
WIRE -16 144 -16 80
WIRE 352 144 352 80
WIRE 192 160 192 64
FLAG 352 224 0
FLAG 352 80 OUT
FLAG -16 224 0
FLAG -16 80 Vcc
FLAG 192 160 Lbi
SYMBOL voltage 352 128 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V1
SYMATTR Value {3.3*(1+P3)}
SYMBOL res 288 48 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 55 22 VTop 2
SYMATTR InstName R1
SYMATTR Value {1Meg*(1+p1)}
SYMBOL voltage -16 128 M0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V2
SYMATTR Value {v}
SYMBOL res 16 48 M90
WINDOW 0 0 56 VBottom 2
WINDOW 3 31 61 VTop 2
SYMATTR InstName R2
SYMATTR Value {110K*(1-p1)}
SYMATTR SpiceLine tol=1 pwr=0.1
TEXT -76 340 Left 2 !.op
TEXT 8 232 Left 2 !.step param v 0.9 1.1 1m
TEXT -80 304 Left 2 !.meas ref when v(Lbi)=1.23*(1-p1*3)
TEXT 416 256 Left 2 ;ref: v(lbi)=1.23 AT 1.0023
TEXT 384 288 Left 2 !.step param p1 -0.01 0.01 0.01\n.step param p3 -0.03 0.03 0.03
TEXT 520 112 Left 2 ;min=0.945V\nmax=1.058V
TEXT 744 80 Left 2 ;step            v(lbi)=1.23*(1+p1*3)\n     1          0.967769\n     2          0.972231\n     3          0.976605\n     4          0.956659\n     5          0.961341\n     6          0.96593\n     7          0.945549\n     8          0.950451\n     9          0.955256
TEXT 1032 128 Left 2 ;step            v(lbi)=1.23*(1-p1*3)\n     1          1.04985\n     2          1.05415\n     3          1.05836\n     4          1.03874\n     5          1.04326\n     6          1.04769\n     7          1.02763\n     8          1.03237\n     9          1.03701
