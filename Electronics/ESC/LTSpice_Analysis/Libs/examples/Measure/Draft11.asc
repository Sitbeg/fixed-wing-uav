Version 4
SHEET 1 1136 680
WIRE 176 96 48 96
WIRE 176 192 176 176
WIRE 272 192 176 192
WIRE 304 192 272 192
WIRE 336 192 304 192
WIRE 48 208 48 160
WIRE 176 208 176 192
WIRE 272 208 272 192
WIRE 48 304 48 288
WIRE 176 304 176 288
WIRE 176 304 48 304
WIRE 272 304 272 272
WIRE 272 304 176 304
WIRE 176 320 176 304
FLAG 304 192 out
FLAG 176 320 0
SYMBOL res 160 80 R0
SYMATTR InstName R1
SYMATTR Value 1k
SYMBOL res 160 192 R0
SYMATTR InstName R2
SYMATTR Value 10k
SYMBOL cap 32 96 R0
SYMATTR InstName C1
SYMATTR Value 1�
SYMBOL cap 256 208 R0
SYMATTR InstName C2
SYMATTR Value 10n
SYMBOL voltage 48 192 R0
WINDOW 123 24 132 Left 0
WINDOW 39 0 0 Left 0
SYMATTR Value2 AC 1 0
SYMATTR InstName V1
SYMATTR Value 0
TEXT 16 344 Left 0 !.ac lin 1000 1 1meg
TEXT 432 120 Left 0 !.meas  ac ampl MAX  abs(V(out))
TEXT 432 192 Left 0 !.meas  ac Fh1  when   abs(V(out))=0.7*abs(ampl) fall=last
TEXT 440 264 Left 0 !.meas  ac nomi  FIND abs(V(out)) at 1.3k
TEXT 440 328 Left 0 !.meas  ac Fh2  when   abs(V(out))=0.7*abs(nomi) fall=last
TEXT 424 32 Left 0 !.options meascplxfmt=cartesian
TEXT 424 72 Left 0 !.meas  ac a MAX  V(out)
TEXT 440 376 Left 0 ;.options meascplxfmt=polar
TEXT 440 424 Left 0 ;.options meascplxfmt=bode
