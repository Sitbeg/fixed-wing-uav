Version 4
SHEET 1 1152 680
WIRE 32 64 -112 64
WIRE 176 64 32 64
WIRE 176 96 176 64
WIRE -112 144 -112 64
WIRE 176 192 176 176
WIRE 224 192 176 192
WIRE 176 208 176 192
WIRE -112 304 -112 224
WIRE 32 304 -112 304
WIRE 176 304 176 272
WIRE 176 304 32 304
WIRE 32 320 32 304
FLAG 32 320 0
FLAG 32 64 in
FLAG 224 192 C1
SYMBOL voltage -112 128 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V1
SYMATTR Value SINE(0 10Vp 60Hz)
SYMBOL res 160 80 R0
SYMATTR InstName R1
SYMATTR Value 1K
SYMBOL cap 160 208 R0
SYMATTR InstName C1
SYMATTR Value 1�F
TEXT 296 96 Left 0 !.meas TRAN Vrms RMS V(in) from 0.05 to 0.1\n.meas TRAN Irms RMS I(V1) from 0.05 to 0.1\n.meas Preal AVG V(in)*-I(V1) from 0.05 to 0.1\n.meas Papp PARAM Vrms*Irms\n.meas PF PARAM Preal/Papp\n.meas Angle PARAM ACOS(PF)
TEXT -144 344 Left 0 !.tran 0.1
TEXT 304 320 Left 0 !.options PLOTWINSIZE 0
TEXT 304 296 Left 0 ;Disable compression to increase the accuracy
TEXT 296 64 Left 0 ;The results are saved in "SPICE Error Log"
