Version 4
SHEET 1 2464 920
WIRE -240 160 -368 160
WIRE -128 160 -240 160
WIRE -240 192 -240 160
WIRE -368 240 -368 160
WIRE -128 240 -128 160
WIRE -368 320 -368 272
WIRE -128 336 -128 272
FLAG -368 384 0
FLAG -128 400 0
FLAG -240 272 0
SYMBOL diode -352 384 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D1
SYMATTR Value BAS70H
SYMBOL diode -112 400 R180
WINDOW 0 24 64 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName D2
SYMATTR Value BAS70H
SYMATTR Prefix xD
SYMBOL Ammeter -368 224 R0
SYMATTR InstName V1
SYMBOL Ammeter -128 224 R0
SYMATTR InstName V2
SYMBOL voltage -240 176 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V3
SYMATTR Value 70
TEXT -24 168 Left 2 !.SUBCKT BAS70H 1 2\n*\n* The resistor R1 does not reflect \n* a physical device.  Instead it\n* improves modeling in the reverse \n* mode of operation.\n*\nR1 1 2 1.409E+09\nD1 1 2 BAS70H\n*\n.MODEL BAS70H D\n+ IS = 3.22E-09 \n+ N = 1.018     \n+ BV = 77       \n+ IBV = 1.67E-07\n+ RS = 20.89    \n+ CJO = 1.655E-12\n+ VJ = 0.349    \n+ M = 0.3583    \n+ FC = 0.5      \n+ EG = 0.69     \n+ XTI = 2       \n.ENDS
TEXT -472 104 Left 2 !.model BAS70H d is=3.22n n=1.018 BV={77*(1+7.5m*(Temp-25))} ibv=167n Rs=20.89 Cjo=1.655p vj=0.349  m=0.3583 fc=0.5 Eg=0.69 Xti=2 nbv=5 ibvl={40n*exp((Temp-25)/11.)} nbvl={900+7*(temp-25)}
TEXT -488 456 Left 2 !.dc v3 5 70 0.1
TEXT 232 392 Left 4 ;V=        5V       40V      70V           \nT=25    3n        10n      30n\nT=85    0.4u      1u      2.4u\nT=125   18u     40u      80u
TEXT -448 536 Left 2 !.meas i5V find i(v1) at 5V\n.meas i40V find i(v1) at 40V\n.meas i70V find i(v1) at 70V
TEXT -360 656 Left 2 !.temp 25 85 125
TEXT 376 320 Left 4 ;DATASHEET
TEXT 824 320 Left 2 ;Measurement: i5v\n  step          i(v1)           at\n     1          3.0163e-009     5\n     2          3.96244e-007    5\n     3          6.45707e-006    5\nMeasurement: i40v\n  step          i(v1)           at\n     1          9.34885e-009    40\n     2          1.32503e-006    40\n     3          3.62425e-005    40\nMeasurement: i70v\n  step          i(v1)           at\n     1          3.0856e-008     70\n     2          3.07856e-006    70\n     3          8.21854e-005    70
