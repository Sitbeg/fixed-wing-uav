Version 4
SHEET 1 880 680
WIRE 128 64 0 64
WIRE 176 64 128 64
WIRE 0 112 0 64
WIRE 176 128 176 64
WIRE 0 240 0 192
WIRE 176 240 176 192
WIRE 176 240 0 240
WIRE 0 272 0 240
FLAG 0 272 0
FLAG 128 64 vf
SYMBOL diode 160 128 R0
SYMATTR InstName D1
SYMATTR Value 1N4148
SYMBOL current 0 192 M180
WINDOW 0 24 80 Left 2
WINDOW 3 24 0 Left 2
SYMATTR InstName I1
SYMATTR Value 1m
TEXT 8 -40 Left 2 !.step temp 0 85
TEXT -8 320 Left 2 !.op
TEXT 336 24 Left 2 !.meas VF0 FIND V(vf) at 0\n.meas VF1 FIND V(vf) at 85\n.meas TCVF param (VF1-VF0)/85
