Version 4
SymbolType BLOCK
RECTANGLE Normal -64 -24 80 24
TEXT -58 7 Left 2 In
TEXT 29 10 Left 2 Out
TEXT -33 -9 Left 2 Delay
WINDOW 0 8 -24 Bottom 2
WINDOW 39 9 38 Center 2
SYMATTR SpiceLine Tau=1.36u
PIN -64 0 NONE 8
PINATTR PinName Vin
PINATTR SpiceOrder 1
PIN 80 0 NONE 8
PINATTR PinName Vout
PINATTR SpiceOrder 2
