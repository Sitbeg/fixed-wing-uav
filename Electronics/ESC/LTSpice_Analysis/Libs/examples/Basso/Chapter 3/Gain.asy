Version 4
SymbolType BLOCK
LINE Normal -32 -48 -32 48
LINE Normal 64 0 -32 -48
LINE Normal -32 48 64 0
LINE Normal -80 48 -32 48
LINE Normal -80 -48 -80 48
LINE Normal -32 -48 -80 -48
TEXT -18 -1 Left 2 Gain
TEXT -71 -1 Left 2 K
WINDOW 3 -59 -67 Center 2
SYMATTR Value K=1
SYMATTR Description Simple gain block
SYMATTR SpiceLine K=1
PIN -80 0 NONE 8
PINATTR PinName In
PINATTR SpiceOrder 1
PIN 64 0 NONE 8
PINATTR PinName Out
PINATTR SpiceOrder 2
