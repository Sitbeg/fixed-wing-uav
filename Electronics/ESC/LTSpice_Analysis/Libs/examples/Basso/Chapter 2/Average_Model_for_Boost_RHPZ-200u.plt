[Transient Analysis]
{
   Npanes: 2
   Active Pane: 1
   {
      traces: 2 {524292,0,"V(duty)"} {524291,1,"-Ix(x1:NP)"}
      X: ('m',1,0,0.0001,0.001)
      Y[0]: ('m',1,0.5817,0.0007,0.5908)
      Y[1]: ('m',0,0.096,0.004,0.148)
      Volts: ('m',0,0,0,0.5817,0.0007,0.5908)
      Amps: ('m',0,0,0,0.096,0.004,0.148)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 2 {524293,0,"V(vout)"} {34603010,1,"I(L1)"}
      X: ('m',1,0,0.0001,0.001)
      Y[0]: (' ',2,23.94,0.02,24.2)
      Y[1]: ('m',0,0.23,0.01,0.36)
      Volts: (' ',0,0,0,23.94,0.02,24.2)
      Amps: ('m',0,0,0,0.23,0.01,0.36)
      Log: 0 0 0
      GridStyle: 1
   }
}
