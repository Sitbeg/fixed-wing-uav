Version 4
SymbolType BLOCK
LINE Normal -80 48 -80 -48
LINE Normal -32 -48 -32 48
LINE Normal -32 -48 -80 -48
LINE Normal -80 48 -32 48
LINE Normal 64 0 -32 -48
LINE Normal -32 48 64 0
TEXT -72 -32 Left 2 K1
TEXT -72 32 Left 2 K2
WINDOW 0 -16 -48 Bottom 2
WINDOW 39 -80 88 Left 2
WINDOW 1 -80 64 Left 2
SYMATTR SpiceLine K1=1 K2=1
PIN -80 -32 NONE 8
PINATTR PinName In1
PINATTR SpiceOrder 1
PIN -80 32 NONE 8
PINATTR PinName In2
PINATTR SpiceOrder 2
PIN 64 0 NONE 8
PINATTR PinName Out
PINATTR SpiceOrder 3
