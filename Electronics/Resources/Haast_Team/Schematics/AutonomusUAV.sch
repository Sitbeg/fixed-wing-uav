<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Holybro_PM07_Power-Management" urn="urn:adsk.eagle:library:27452541">
<packages>
<package name="PM07" urn="urn:adsk.eagle:footprint:27452543/2" library_version="5">
<wire x1="68.58" y1="-50.8" x2="-68.58" y2="-50.8" width="0.127" layer="21"/>
<wire x1="-68.58" y1="50.8" x2="68.58" y2="50.8" width="0.127" layer="21"/>
<wire x1="68.58" y1="50.8" x2="68.58" y2="-50.8" width="0.127" layer="21"/>
<wire x1="-68.58" y1="50.8" x2="-68.58" y2="-50.8" width="0.127" layer="21"/>
<wire x1="-26.67" y1="6.35" x2="-21.59" y2="6.35" width="0.127" layer="21"/>
<wire x1="-21.59" y1="6.35" x2="-21.59" y2="20.32" width="0.127" layer="21"/>
<wire x1="-21.59" y1="20.32" x2="-26.67" y2="20.32" width="0.127" layer="21"/>
<wire x1="-26.67" y1="20.32" x2="-26.67" y2="6.35" width="0.127" layer="21"/>
<wire x1="-12.7" y1="6.35" x2="-7.62" y2="6.35" width="0.127" layer="21"/>
<wire x1="-7.62" y1="6.35" x2="-7.62" y2="20.32" width="0.127" layer="21"/>
<wire x1="-7.62" y1="20.32" x2="-12.7" y2="20.32" width="0.127" layer="21"/>
<wire x1="-12.7" y1="20.32" x2="-12.7" y2="6.35" width="0.127" layer="21"/>
<wire x1="63.5" y1="-13.97" x2="58.42" y2="-13.97" width="0.127" layer="21"/>
<wire x1="58.42" y1="-13.97" x2="58.42" y2="-5.08" width="0.127" layer="21"/>
<wire x1="63.5" y1="-13.97" x2="63.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="58.42" y1="2.54" x2="63.5" y2="2.54" width="0.127" layer="21"/>
<wire x1="63.5" y1="2.54" x2="63.5" y2="11.43" width="0.127" layer="21"/>
<wire x1="58.42" y1="2.54" x2="58.42" y2="11.43" width="0.127" layer="21"/>
<wire x1="58.42" y1="11.43" x2="63.5" y2="11.43" width="0.127" layer="21"/>
<wire x1="58.42" y1="-5.08" x2="63.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-33.02" x2="-33.02" y2="-33.02" width="0.127" layer="21"/>
<wire x1="-33.02" y1="-33.02" x2="-33.02" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-33.02" y1="-21.59" x2="-27.94" y2="-21.59" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-21.59" x2="-27.94" y2="-33.02" width="0.127" layer="21"/>
<hole x="-33.65" y="47" drill="4.4"/>
<hole x="65" y="48" drill="4.4"/>
<hole x="65" y="-47" drill="4.4"/>
<hole x="-33.65" y="-45.73" drill="4.4"/>
<smd name="GND_M2" x="-65" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="2-12S_V" x="-58.92" y="-8.54" dx="12" dy="8" layer="1" rot="R180"/>
<smd name="B+_M2" x="-57.38" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M6" x="-49.76" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M6" x="-42.14" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M7" x="32.79" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M7" x="40.41" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M3" x="48.03" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M3" x="55.65" y="44.46" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="2-12S_GND" x="-58.92" y="7.97" dx="12" dy="8" layer="1" rot="R180"/>
<smd name="GND_M4" x="-65" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M4" x="-57.38" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M8" x="-49.76" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M8" x="-42.14" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M5" x="32.79" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M5" x="40.41" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="B+_M1" x="48.03" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="GND_M1" x="55.65" y="-44.44" dx="8" dy="4" layer="1" roundness="100" rot="R90"/>
<smd name="M4" x="-64.77" y="-35.56" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M8" x="-38.1" y="-38.1" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M5" x="27.94" y="-38.1" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M1" x="59.69" y="-38.1" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M3" x="59.69" y="35.56" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M7" x="27.94" y="35.56" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M6" x="-39.37" y="35.56" dx="2" dy="2" layer="1" roundness="100"/>
<smd name="M2" x="-63.5" y="34.29" dx="2" dy="2" layer="1" roundness="100"/>
<pad name="GND_FMU_1" x="12.7" y="46.99" drill="2.5"/>
<pad name="V+_FMU_1" x="12.7" y="41.91" drill="2.5"/>
<pad name="S8" x="-22.86" y="36.83" drill="2.5"/>
<pad name="S7" x="-17.78" y="36.83" drill="2.5"/>
<pad name="S6" x="-12.7" y="36.83" drill="2.5"/>
<pad name="S5" x="-7.62" y="36.83" drill="2.5"/>
<pad name="S4" x="-2.54" y="36.83" drill="2.5"/>
<pad name="S3" x="2.54" y="36.83" drill="2.5"/>
<pad name="S2" x="7.62" y="36.83" drill="2.5"/>
<pad name="S1" x="12.7" y="36.83" drill="2.5"/>
<pad name="VDD_SERVO_1" x="-24.13" y="7.62" drill="0.6" rot="R90"/>
<pad name="IO_CH1" x="-24.13" y="8.89" drill="0.6" rot="R90"/>
<pad name="IO_CH2" x="-24.13" y="10.16" drill="0.6" rot="R90"/>
<pad name="IO_CH3" x="-24.13" y="11.43" drill="0.6" rot="R90"/>
<pad name="IO_CH4" x="-24.13" y="12.7" drill="0.6" rot="R90"/>
<pad name="IO_CH5" x="-24.13" y="13.97" drill="0.6" rot="R90"/>
<pad name="IO_CH6" x="-24.13" y="15.24" drill="0.6" rot="R90"/>
<pad name="IO_CH7" x="-24.13" y="16.51" drill="0.6" rot="R90"/>
<pad name="IO_CH8" x="-24.13" y="17.78" drill="0.6" rot="R90"/>
<pad name="GND_16" x="-24.13" y="19.05" drill="0.6" rot="R90"/>
<pad name="GND_17" x="-10.16" y="19.05" drill="0.6" rot="R90"/>
<pad name="FMU_CH8" x="-10.16" y="17.78" drill="0.6" rot="R90"/>
<pad name="FMU_CH7" x="-10.16" y="16.51" drill="0.6" rot="R90"/>
<pad name="FMU_CH6" x="-10.16" y="15.24" drill="0.6" rot="R90"/>
<pad name="FMU_CH5" x="-10.16" y="13.97" drill="0.6" rot="R90"/>
<pad name="FMU_CH4" x="-10.16" y="12.7" drill="0.6" rot="R90"/>
<pad name="FMU_CH3" x="-10.16" y="11.43" drill="0.6" rot="R90"/>
<pad name="FMU_CH2" x="-10.16" y="10.16" drill="0.6" rot="R90"/>
<pad name="FMU_CH1" x="-10.16" y="8.89" drill="0.6" rot="R90"/>
<pad name="VDD_SERVO_2" x="-10.16" y="7.62" drill="0.6" rot="R90"/>
<pad name="VCC_1" x="60.96" y="-12.7" drill="0.6" rot="R90"/>
<pad name="VCC_2" x="60.96" y="-11.43" drill="0.6" rot="R90"/>
<pad name="CURRENT_1" x="60.96" y="-10.16" drill="0.6" rot="R90"/>
<pad name="VOLTAGE_1" x="60.96" y="-8.89" drill="0.6" rot="R90"/>
<pad name="GND_1" x="60.96" y="-7.62" drill="0.6" rot="R90"/>
<pad name="GND_2" x="60.96" y="-6.35" drill="0.6" rot="R90"/>
<pad name="VCC_3" x="60.96" y="3.81" drill="0.6" rot="R90"/>
<pad name="VCC_4" x="60.96" y="5.08" drill="0.6" rot="R90"/>
<pad name="CURRENT_2" x="60.96" y="6.35" drill="0.6" rot="R90"/>
<pad name="VOLTAGE_2" x="60.96" y="7.62" drill="0.6" rot="R90"/>
<pad name="GND_3" x="60.96" y="8.89" drill="0.6" rot="R90"/>
<pad name="GND_4" x="60.96" y="10.16" drill="0.6" rot="R90"/>
<pad name="GND_12" x="-30.48" y="-22.86" drill="0.6" rot="R90"/>
<pad name="ADC1_SPARE_2" x="-30.48" y="-24.13" drill="0.6" rot="R90"/>
<pad name="ADC1_SPARE_1" x="-30.48" y="-25.4" drill="0.6" rot="R90"/>
<pad name="TIM5_SPARE_4" x="-30.48" y="-26.67" drill="0.6" rot="R90"/>
<pad name="FMU_CAP3" x="-30.48" y="-27.94" drill="0.6" rot="R90"/>
<pad name="FMU_CAP2" x="-30.48" y="-29.21" drill="0.6" rot="R90"/>
<pad name="FMU_CAP1" x="-30.48" y="-30.48" drill="0.6" rot="R90"/>
<pad name="VCC_11" x="-30.48" y="-31.75" drill="0.6" rot="R90"/>
<pad name="V+_FMU_2" x="7.62" y="41.91" drill="2.5"/>
<pad name="V+_FMU_3" x="2.54" y="41.91" drill="2.5"/>
<pad name="V+_FMU_4" x="-2.54" y="41.91" drill="2.5"/>
<pad name="V+_FMU_5" x="-7.62" y="41.91" drill="2.5"/>
<pad name="V+_FMU_6" x="-12.7" y="41.91" drill="2.5"/>
<pad name="V+_FMU_7" x="-17.78" y="41.91" drill="2.5"/>
<pad name="V+_FMU_8" x="-22.86" y="41.91" drill="2.5"/>
<pad name="GND_FMU_2" x="7.62" y="46.99" drill="2.5"/>
<pad name="GND_FMU_3" x="2.54" y="46.99" drill="2.5"/>
<pad name="GND_FMU_4" x="-2.54" y="46.99" drill="2.5"/>
<pad name="GND_FMU_5" x="-7.62" y="46.99" drill="2.5"/>
<pad name="GND_FMU_6" x="-12.7" y="46.99" drill="2.5"/>
<pad name="GND_FMU_7" x="-17.78" y="46.99" drill="2.5"/>
<pad name="GND_FMU_8" x="-22.86" y="46.99" drill="2.5"/>
<pad name="GND_CAP_1" x="-22.86" y="-45.72" drill="2.5"/>
<pad name="GND_CAP_2" x="-17.78" y="-45.72" drill="2.5"/>
<pad name="GND_CAP_3" x="-12.7" y="-45.72" drill="2.5"/>
<pad name="GND_CAP_4" x="-7.62" y="-45.72" drill="2.5"/>
<pad name="GND_CAP_5" x="-2.54" y="-45.72" drill="2.5"/>
<pad name="GND_CAP_6" x="2.54" y="-45.72" drill="2.5"/>
<pad name="V_CAP_1" x="-22.86" y="-40.64" drill="2.5"/>
<pad name="V_CAP_2" x="-17.78" y="-40.64" drill="2.5"/>
<pad name="V_CAP_3" x="-12.7" y="-40.64" drill="2.5"/>
<pad name="V_CAP_4" x="-7.62" y="-40.64" drill="2.5"/>
<pad name="V_CAP_5" x="-2.54" y="-40.64" drill="2.5"/>
<pad name="V_CAP_6" x="2.54" y="-40.64" drill="2.5"/>
<pad name="CAP1" x="-22.86" y="-35.56" drill="2.5"/>
<pad name="CAP2" x="-17.78" y="-35.56" drill="2.5"/>
<pad name="CAP3" x="-12.7" y="-35.56" drill="2.5"/>
<pad name="CAP4" x="-7.62" y="-35.56" drill="2.5"/>
<pad name="ADC-3V3" x="-2.54" y="-35.56" drill="2.5"/>
<pad name="ADC-6V6" x="2.54" y="-35.56" drill="2.5"/>
</package>
</packages>
<packages3d>
<package3d name="PM07" urn="urn:adsk.eagle:package:27452551/2" type="box" library_version="5">
<packageinstances>
<packageinstance name="PM07"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="POWER_IN&amp;OUT" urn="urn:adsk.eagle:symbol:27452545/2" library_version="5">
<pin name="VOLTAGE_IN_LIPO" x="-7.62" y="5.08" visible="pin" length="middle"/>
<pin name="GROUND_IN_LIPO" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="5V_1" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<pin name="GND_1" x="-7.62" y="-5.08" visible="pin" length="middle"/>
<pin name="CURRENT_1" x="-7.62" y="-7.62" visible="pin" length="middle"/>
<pin name="VOLTAGE_1" x="-7.62" y="-10.16" visible="pin" length="middle"/>
<pin name="5V_2" x="35.56" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GND_2" x="35.56" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="CURRENT_2" x="35.56" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="VOLTAGE_2" x="35.56" y="-10.16" visible="pin" length="middle" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="30.48" y2="-12.7" width="0.254" layer="94"/>
<wire x1="30.48" y1="-12.7" x2="30.48" y2="7.62" width="0.254" layer="94"/>
<wire x1="30.48" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="FMU_PWM_OUT" urn="urn:adsk.eagle:symbol:27452548/2" library_version="5">
<pin name="S1" x="-15.24" y="5.08" visible="pin" length="middle"/>
<pin name="S2" x="-15.24" y="2.54" visible="pin" length="middle"/>
<pin name="S3" x="-15.24" y="0" visible="pin" length="middle"/>
<pin name="S4" x="-15.24" y="-2.54" visible="pin" length="middle"/>
<pin name="S5" x="-15.24" y="-5.08" visible="pin" length="middle"/>
<pin name="S6" x="15.24" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="S7" x="15.24" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="S8" x="15.24" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="VOLTAGE" x="15.24" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="2.54" visible="pin" length="middle" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="I/O_PWM_OUT" urn="urn:adsk.eagle:symbol:27452550/2" library_version="5">
<pin name="B+_1" x="-17.78" y="12.7" visible="pin" length="middle"/>
<pin name="GND_1" x="-17.78" y="10.16" visible="pin" length="middle"/>
<pin name="M1" x="-17.78" y="7.62" visible="pin" length="middle"/>
<pin name="B+_2" x="-17.78" y="5.08" visible="pin" length="middle"/>
<pin name="GND_2" x="-17.78" y="2.54" visible="pin" length="middle"/>
<pin name="M2" x="-17.78" y="0" visible="pin" length="middle"/>
<pin name="B+_3" x="-17.78" y="-2.54" visible="pin" length="middle"/>
<pin name="GND_3" x="-17.78" y="-5.08" visible="pin" length="middle"/>
<pin name="M3" x="-17.78" y="-7.62" visible="pin" length="middle"/>
<pin name="B+_4" x="-17.78" y="-10.16" visible="pin" length="middle"/>
<pin name="GND_4" x="-17.78" y="-12.7" visible="pin" length="middle"/>
<pin name="M4" x="-17.78" y="-15.24" visible="pin" length="middle"/>
<pin name="B+_5" x="17.78" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="GND_5" x="17.78" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="M5" x="17.78" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="B+_6" x="17.78" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GND_6" x="17.78" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="M6" x="17.78" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="B+_7" x="17.78" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="GND_7" x="17.78" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="M7" x="17.78" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="B+_8" x="17.78" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="GND_8" x="17.78" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="M8" x="17.78" y="12.7" visible="pin" length="middle" rot="R180"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<text x="-12.7" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="POWER_IN&amp;OUT" urn="urn:adsk.eagle:component:27454338/3" prefix="U" library_version="5">
<gates>
<gate name="G$1" symbol="POWER_IN&amp;OUT" x="-12.7" y="2.54"/>
</gates>
<devices>
<device name="" package="PM07">
<connects>
<connect gate="G$1" pin="5V_1" pad="VCC_1 VCC_2"/>
<connect gate="G$1" pin="5V_2" pad="VCC_3 VCC_4"/>
<connect gate="G$1" pin="CURRENT_1" pad="CURRENT_1"/>
<connect gate="G$1" pin="CURRENT_2" pad="CURRENT_2"/>
<connect gate="G$1" pin="GND_1" pad="GND_1 GND_2"/>
<connect gate="G$1" pin="GND_2" pad="GND_3 GND_4"/>
<connect gate="G$1" pin="GROUND_IN_LIPO" pad="2-12S_GND"/>
<connect gate="G$1" pin="VOLTAGE_1" pad="VOLTAGE_1"/>
<connect gate="G$1" pin="VOLTAGE_2" pad="VOLTAGE_2"/>
<connect gate="G$1" pin="VOLTAGE_IN_LIPO" pad="2-12S_V"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452551/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FMU_PWM_OUT" urn="urn:adsk.eagle:component:27454310/3" prefix="U" library_version="5">
<gates>
<gate name="G$1" symbol="FMU_PWM_OUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PM07">
<connects>
<connect gate="G$1" pin="GND" pad="GND_FMU_1 GND_FMU_2 GND_FMU_3 GND_FMU_4 GND_FMU_5 GND_FMU_6 GND_FMU_7 GND_FMU_8"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
<connect gate="G$1" pin="S3" pad="S3"/>
<connect gate="G$1" pin="S4" pad="S4"/>
<connect gate="G$1" pin="S5" pad="S5"/>
<connect gate="G$1" pin="S6" pad="S6"/>
<connect gate="G$1" pin="S7" pad="S7"/>
<connect gate="G$1" pin="S8" pad="S8"/>
<connect gate="G$1" pin="VOLTAGE" pad="V+_FMU_1 V+_FMU_2 V+_FMU_3 V+_FMU_4 V+_FMU_5 V+_FMU_6 V+_FMU_7 V+_FMU_8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452551/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I/O_PWM_OUT" urn="urn:adsk.eagle:component:27454308/4" prefix="U" library_version="5">
<gates>
<gate name="G$1" symbol="I/O_PWM_OUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PM07">
<connects>
<connect gate="G$1" pin="B+_1" pad="B+_M1"/>
<connect gate="G$1" pin="B+_2" pad="B+_M2"/>
<connect gate="G$1" pin="B+_3" pad="B+_M3"/>
<connect gate="G$1" pin="B+_4" pad="B+_M4"/>
<connect gate="G$1" pin="B+_5" pad="B+_M5"/>
<connect gate="G$1" pin="B+_6" pad="B+_M6"/>
<connect gate="G$1" pin="B+_7" pad="B+_M7"/>
<connect gate="G$1" pin="B+_8" pad="B+_M8"/>
<connect gate="G$1" pin="GND_1" pad="GND_M1"/>
<connect gate="G$1" pin="GND_2" pad="GND_M2"/>
<connect gate="G$1" pin="GND_3" pad="GND_M3"/>
<connect gate="G$1" pin="GND_4" pad="GND_M4"/>
<connect gate="G$1" pin="GND_5" pad="GND_M5"/>
<connect gate="G$1" pin="GND_6" pad="GND_M6"/>
<connect gate="G$1" pin="GND_7" pad="GND_M7"/>
<connect gate="G$1" pin="GND_8" pad="GND_M8"/>
<connect gate="G$1" pin="M1" pad="M1"/>
<connect gate="G$1" pin="M2" pad="M2"/>
<connect gate="G$1" pin="M3" pad="M3"/>
<connect gate="G$1" pin="M4" pad="M4"/>
<connect gate="G$1" pin="M5" pad="M5"/>
<connect gate="G$1" pin="M6" pad="M6"/>
<connect gate="G$1" pin="M7" pad="M7"/>
<connect gate="G$1" pin="M8" pad="M8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452551/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Pixhawk4" urn="urn:adsk.eagle:library:27432483">
<packages>
<package name="PIXHAWK4" urn="urn:adsk.eagle:footprint:27432484/3" library_version="9">
<wire x1="22" y1="-42" x2="-22" y2="-42" width="0.127" layer="21"/>
<wire x1="-22" y1="-42" x2="-22" y2="42" width="0.127" layer="21"/>
<wire x1="-22" y1="42" x2="-8.1" y2="42" width="0.127" layer="21"/>
<wire x1="-8.1" y1="42" x2="7.1" y2="42" width="0.127" layer="21"/>
<wire x1="7.1" y1="42" x2="22" y2="42" width="0.127" layer="21"/>
<wire x1="22" y1="42" x2="22" y2="-42" width="0.127" layer="21"/>
<wire x1="-8.1" y1="42" x2="-8.1" y2="17.5" width="0.127" layer="21"/>
<wire x1="-8.1" y1="17.5" x2="7.1" y2="17.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="17.5" x2="7.1" y2="42" width="0.127" layer="21"/>
<wire x1="-10" y1="18" x2="-10" y2="17" width="0.127" layer="21"/>
<wire x1="-10" y1="17" x2="-12" y2="17" width="0.127" layer="21"/>
<wire x1="-12" y1="17" x2="-12" y2="18" width="0.127" layer="21"/>
<wire x1="-12" y1="18" x2="-10" y2="18" width="0.127" layer="21"/>
<wire x1="-14" y1="17" x2="-14" y2="18" width="0.127" layer="21"/>
<wire x1="-14" y1="18" x2="-16" y2="18" width="0.127" layer="21"/>
<wire x1="-16" y1="18" x2="-16" y2="17" width="0.127" layer="21"/>
<wire x1="-16" y1="17" x2="-14" y2="17" width="0.127" layer="21"/>
<wire x1="-18" y1="17" x2="-18" y2="18" width="0.127" layer="21"/>
<wire x1="-18" y1="18" x2="-20" y2="18" width="0.127" layer="21"/>
<wire x1="-20" y1="18" x2="-20" y2="17" width="0.127" layer="21"/>
<wire x1="-20" y1="17" x2="-18" y2="17" width="0.127" layer="21"/>
<wire x1="9.6" y1="17" x2="9.6" y2="18" width="0.127" layer="21"/>
<wire x1="9.6" y1="18" x2="11.6" y2="18" width="0.127" layer="21"/>
<wire x1="11.6" y1="18" x2="11.6" y2="17" width="0.127" layer="21"/>
<wire x1="11.6" y1="17" x2="9.6" y2="17" width="0.127" layer="21"/>
<wire x1="13.6" y1="17" x2="13.6" y2="18" width="0.127" layer="21"/>
<wire x1="13.6" y1="18" x2="15.6" y2="18" width="0.127" layer="21"/>
<wire x1="15.6" y1="18" x2="15.6" y2="17" width="0.127" layer="21"/>
<wire x1="15.6" y1="17" x2="13.6" y2="17" width="0.127" layer="21"/>
<wire x1="17.5" y1="17" x2="17.5" y2="18" width="0.127" layer="21"/>
<wire x1="17.5" y1="18" x2="19.5" y2="18" width="0.127" layer="21"/>
<wire x1="19.5" y1="18" x2="19.5" y2="17" width="0.127" layer="21"/>
<wire x1="19.5" y1="17" x2="17.5" y2="17" width="0.127" layer="21"/>
<wire x1="-19.05" y1="34.29" x2="-19.05" y2="39.37" width="0.127" layer="21"/>
<wire x1="-19.05" y1="39.37" x2="-8.128" y2="39.37" width="0.127" layer="21"/>
<wire x1="-19.05" y1="34.29" x2="-8.128" y2="34.29" width="0.127" layer="21"/>
<wire x1="-19.05" y1="27.94" x2="-19.05" y2="22.86" width="0.127" layer="21"/>
<wire x1="-19.05" y1="22.86" x2="-8.128" y2="22.86" width="0.127" layer="21"/>
<wire x1="-19.05" y1="27.94" x2="-8.128" y2="27.94" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-34.29" x2="-19.05" y2="-39.37" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-39.37" x2="-5.08" y2="-39.37" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-39.37" x2="-5.08" y2="-34.29" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-34.29" x2="-19.05" y2="-34.29" width="0.127" layer="21"/>
<wire x1="5.08" y1="-34.29" x2="5.08" y2="-39.37" width="0.127" layer="21"/>
<wire x1="5.08" y1="-39.37" x2="19.05" y2="-39.37" width="0.127" layer="21"/>
<wire x1="19.05" y1="-39.37" x2="19.05" y2="-34.29" width="0.127" layer="21"/>
<wire x1="19.05" y1="-34.29" x2="5.08" y2="-34.29" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-26.67" x2="-5.08" y2="-31.75" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-31.75" x2="5.08" y2="-31.75" width="0.127" layer="21"/>
<wire x1="5.08" y1="-31.75" x2="5.08" y2="-26.67" width="0.127" layer="21"/>
<wire x1="5.08" y1="-26.67" x2="-5.08" y2="-26.67" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-19.05" x2="-19.05" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-24.13" x2="-13.97" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-24.13" x2="-13.97" y2="-19.05" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-19.05" x2="-19.05" y2="-19.05" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-19.05" x2="-6.35" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-24.13" x2="-1.27" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-24.13" x2="-1.27" y2="-19.05" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-19.05" x2="-6.35" y2="-19.05" width="0.127" layer="21"/>
<wire x1="5.08" y1="-24.13" x2="19.05" y2="-24.13" width="0.127" layer="21"/>
<wire x1="19.05" y1="-24.13" x2="19.05" y2="-19.05" width="0.127" layer="21"/>
<wire x1="19.05" y1="-19.05" x2="5.08" y2="-19.05" width="0.127" layer="21"/>
<wire x1="5.08" y1="-19.05" x2="5.08" y2="-24.13" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-16.51" x2="-12.7" y2="-16.51" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-16.51" x2="-12.7" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-11.43" x2="-19.05" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-11.43" x2="-19.05" y2="-16.51" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-16.51" x2="0" y2="-16.51" width="0.127" layer="21"/>
<wire x1="0" y1="-16.51" x2="0" y2="-11.43" width="0.127" layer="21"/>
<wire x1="0" y1="-11.43" x2="-6.35" y2="-11.43" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-11.43" x2="-6.35" y2="-16.51" width="0.127" layer="21"/>
<wire x1="7.62" y1="-16.51" x2="7.62" y2="-11.43" width="0.127" layer="21"/>
<wire x1="7.62" y1="-11.43" x2="19.05" y2="-11.43" width="0.127" layer="21"/>
<wire x1="19.05" y1="-11.43" x2="19.05" y2="-16.51" width="0.127" layer="21"/>
<wire x1="19.05" y1="-16.51" x2="7.62" y2="-16.51" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-8.89" x2="-12.7" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-8.89" x2="-12.7" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-3.81" x2="-19.05" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-3.81" x2="-19.05" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-8.89" x2="-6.35" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-8.89" width="0.127" layer="21"/>
<wire x1="2.54" y1="-8.89" x2="-6.35" y2="-8.89" width="0.127" layer="21"/>
<wire x1="19.05" y1="-8.89" x2="19.05" y2="-3.81" width="0.127" layer="21"/>
<wire x1="19.05" y1="-3.81" x2="11.43" y2="-3.81" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.81" x2="11.43" y2="-8.89" width="0.127" layer="21"/>
<wire x1="11.43" y1="-8.89" x2="19.05" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-1.27" x2="-12.7" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-1.27" x2="-12.7" y2="3.81" width="0.127" layer="21"/>
<wire x1="-12.7" y1="3.81" x2="-19.05" y2="3.81" width="0.127" layer="21"/>
<wire x1="-19.05" y1="3.81" x2="-19.05" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="3.81" width="0.127" layer="21"/>
<wire x1="-6.35" y1="3.81" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-6.35" y2="-1.27" width="0.127" layer="21"/>
<wire x1="10.16" y1="-1.27" x2="10.16" y2="3.81" width="0.127" layer="21"/>
<wire x1="10.16" y1="3.81" x2="19.05" y2="3.81" width="0.127" layer="21"/>
<wire x1="19.05" y1="3.81" x2="19.05" y2="-1.27" width="0.127" layer="21"/>
<wire x1="19.05" y1="-1.27" x2="10.16" y2="-1.27" width="0.127" layer="21"/>
<hole x="-16" y="-30" drill="2.3"/>
<hole x="16" y="-30" drill="2.3"/>
<hole x="16" y="31" drill="2.3"/>
<hole x="-16" y="31" drill="2.3"/>
<pad name="VCC_1" x="-17.78" y="36.83" drill="0.6"/>
<pad name="VCC_2" x="-16.51" y="36.83" drill="0.6"/>
<pad name="CURRENT_1" x="-15.24" y="36.83" drill="0.6"/>
<pad name="VOLTAGE_1" x="-13.97" y="36.83" drill="0.6"/>
<pad name="GND_1" x="-12.7" y="36.83" drill="0.6"/>
<pad name="GND_2" x="-11.43" y="36.83" drill="0.6"/>
<pad name="VCC_3" x="-17.78" y="25.4" drill="0.6"/>
<pad name="VCC_4" x="-16.51" y="25.4" drill="0.6"/>
<pad name="CURRENT_2" x="-15.24" y="25.4" drill="0.6"/>
<pad name="VOLTAGE_2" x="-13.97" y="25.4" drill="0.6"/>
<pad name="GND_3" x="-12.7" y="25.4" drill="0.6"/>
<pad name="GND_4" x="-11.43" y="25.4" drill="0.6"/>
<pad name="VDD_SERVO_1" x="-17.78" y="-36.83" drill="0.6"/>
<pad name="IO_CH1" x="-16.51" y="-36.83" drill="0.6"/>
<pad name="IO_CH2" x="-15.24" y="-36.83" drill="0.6"/>
<pad name="IO_CH3" x="-13.97" y="-36.83" drill="0.6"/>
<pad name="IO_CH4" x="-12.7" y="-36.83" drill="0.6"/>
<pad name="IO_CH5" x="-11.43" y="-36.83" drill="0.6"/>
<pad name="IO_CH6" x="-10.16" y="-36.83" drill="0.6"/>
<pad name="IO_CH7" x="-8.89" y="-36.83" drill="0.6"/>
<pad name="IO_CH8" x="-7.62" y="-36.83" drill="0.6"/>
<pad name="GND_16" x="-6.35" y="-36.83" drill="0.6"/>
<pad name="GND_17" x="17.78" y="-36.83" drill="0.6"/>
<pad name="FMU_CH8" x="16.51" y="-36.83" drill="0.6"/>
<pad name="FMU_CH7" x="15.24" y="-36.83" drill="0.6"/>
<pad name="FMU_CH6" x="13.97" y="-36.83" drill="0.6"/>
<pad name="FMU_CH5" x="12.7" y="-36.83" drill="0.6"/>
<pad name="FMU_CH4" x="11.43" y="-36.83" drill="0.6"/>
<pad name="FMU_CH3" x="10.16" y="-36.83" drill="0.6"/>
<pad name="FMU_CH2" x="8.89" y="-36.83" drill="0.6"/>
<pad name="FMU_CH1" x="7.62" y="-36.83" drill="0.6"/>
<pad name="VDD_SERVO_2" x="6.35" y="-36.83" drill="0.6"/>
<pad name="MOSI" x="0" y="-29.21" drill="0.6"/>
<pad name="MISO" x="-1.27" y="-29.21" drill="0.6"/>
<pad name="SCK" x="-2.54" y="-29.21" drill="0.6"/>
<pad name="VCC_14" x="-3.81" y="-29.21" drill="0.6"/>
<pad name="CS1" x="1.27" y="-29.21" drill="0.6"/>
<pad name="CS2" x="2.54" y="-29.21" drill="0.6"/>
<pad name="GND_15" x="3.81" y="-29.21" drill="0.6"/>
<pad name="SBUS_OUT" x="-17.78" y="-21.59" drill="0.6"/>
<pad name="VCC" x="-16.51" y="-21.59" drill="0.6"/>
<pad name="GND18" x="-15.24" y="-21.59" drill="0.6"/>
<pad name="VCC_12" x="-5.08" y="-21.59" drill="0.6"/>
<pad name="PPM" x="-3.81" y="-21.59" drill="0.6"/>
<pad name="GND_13" x="-2.54" y="-21.59" drill="0.6"/>
<pad name="GND_14" x="17.78" y="-21.59" drill="0.6"/>
<pad name="BUZZER" x="16.51" y="-21.59" drill="0.6"/>
<pad name="VDD_3V3" x="15.24" y="-21.59" drill="0.6"/>
<pad name="SAFETY_SWITCH_LED" x="13.97" y="-21.59" drill="0.6"/>
<pad name="SAFETY_SWITCH" x="12.7" y="-21.59" drill="0.6"/>
<pad name="SDA1" x="11.43" y="-21.59" drill="0.6"/>
<pad name="SCL1" x="10.16" y="-21.59" drill="0.6"/>
<pad name="RX_4" x="8.89" y="-21.59" drill="0.6"/>
<pad name="TX_4" x="7.62" y="-21.59" drill="0.6"/>
<pad name="VCC_13" x="6.35" y="-21.59" drill="0.6"/>
<pad name="VCC_9" x="-17.78" y="-13.97" drill="0.6"/>
<pad name="CANH_2" x="-16.51" y="-13.97" drill="0.6"/>
<pad name="CANL_2" x="-15.24" y="-13.97" drill="0.6"/>
<pad name="GND_10" x="-13.97" y="-13.97" drill="0.6"/>
<pad name="VCC_10" x="-5.08" y="-13.97" drill="0.6"/>
<pad name="SCL4" x="-3.81" y="-13.97" drill="0.6"/>
<pad name="SDA4" x="-2.54" y="-13.97" drill="0.6"/>
<pad name="GND_11" x="-1.27" y="-13.97" drill="0.6"/>
<pad name="GND_12" x="17.78" y="-13.97" drill="0.6"/>
<pad name="ADC1_SPARE_2" x="16.51" y="-13.97" drill="0.6"/>
<pad name="ADC1_SPARE_1" x="15.24" y="-13.97" drill="0.6"/>
<pad name="TIM5_SPARE_4" x="13.97" y="-13.97" drill="0.6"/>
<pad name="FMU_CAP3" x="12.7" y="-13.97" drill="0.6"/>
<pad name="FMU_CAP2" x="11.43" y="-13.97" drill="0.6"/>
<pad name="FMU_CAP1" x="10.16" y="-13.97" drill="0.6"/>
<pad name="VCC_11" x="8.89" y="-13.97" drill="0.6"/>
<pad name="VCC_8" x="-17.78" y="-6.35" drill="0.6"/>
<pad name="CANH_1" x="-16.51" y="-6.35" drill="0.6"/>
<pad name="CANL_1" x="-15.24" y="-6.35" drill="0.6"/>
<pad name="GND_9" x="-13.97" y="-6.35" drill="0.6"/>
<pad name="VCC_6" x="-5.08" y="-6.35" drill="0.6"/>
<pad name="TX_2" x="-3.81" y="-6.35" drill="0.6"/>
<pad name="RX_2" x="-2.54" y="-6.35" drill="0.6"/>
<pad name="CTS_2" x="-1.27" y="-6.35" drill="0.6"/>
<pad name="RTS_2" x="0" y="-6.35" drill="0.6"/>
<pad name="GND_7" x="1.27" y="-6.35" drill="0.6"/>
<pad name="GND" x="17.78" y="-6.35" drill="0.6"/>
<pad name="VDD_3V3_SPEKTRUM" x="16.51" y="-6.35" drill="0.6"/>
<pad name="RSSI" x="15.24" y="-6.35" drill="0.6"/>
<pad name="SBUS" x="13.97" y="-6.35" drill="0.6"/>
<pad name="VDD_5V_SBUS_RC" x="12.7" y="-6.35" drill="0.6"/>
<pad name="VBUS" x="-17.78" y="1.27" drill="0.6"/>
<pad name="DM" x="-16.51" y="1.27" drill="0.6"/>
<pad name="DP" x="-15.24" y="1.27" drill="0.6"/>
<pad name="GND_5" x="-13.97" y="1.27" drill="0.6"/>
<pad name="VCC_5" x="-5.08" y="1.27" drill="0.6"/>
<pad name="TX_1" x="-3.81" y="1.27" drill="0.6"/>
<pad name="RX_1" x="-2.54" y="1.27" drill="0.6"/>
<pad name="CTS_1" x="-1.27" y="1.27" drill="0.6"/>
<pad name="RTS_1" x="0" y="1.27" drill="0.6"/>
<pad name="GND_6" x="1.27" y="1.27" drill="0.6"/>
<pad name="VCC_7" x="11.43" y="1.27" drill="0.6"/>
<pad name="TX_3" x="12.7" y="1.27" drill="0.6"/>
<pad name="RX_3" x="13.97" y="1.27" drill="0.6"/>
<pad name="SCL2" x="15.24" y="1.27" drill="0.6"/>
<pad name="SDA2" x="16.51" y="1.27" drill="0.6"/>
<pad name="GND_8" x="17.78" y="1.27" drill="0.6"/>
<text x="-13" y="9" size="5.08" layer="21" font="vector">pixhawk 4</text>
<text x="-19.05" y="39.624" size="1.016" layer="21">POWER1</text>
<text x="-19.05" y="28.194" size="1.016" layer="21">POWER2</text>
<text x="-19.177" y="4.064" size="1.016" layer="21">USB</text>
<text x="-6.35" y="4.064" size="1.016" layer="21">TELEM1</text>
<text x="-6.604" y="-3.556" size="1.016" layer="21">TELEM2</text>
<text x="10.033" y="4.191" size="1.016" layer="21">UART &amp; I2C B</text>
<text x="11.303" y="-3.556" size="0.8128" layer="21">DSM/SBUS RC</text>
<text x="-19.177" y="-3.556" size="1.016" layer="21">CAN1</text>
<text x="-19.177" y="-11.176" size="1.016" layer="21">CAN2</text>
<text x="-6.477" y="-11.176" size="1.016" layer="21">I2C A</text>
<text x="7.493" y="-11.176" size="1.016" layer="21">CAP &amp; ADC IN</text>
<text x="-19.177" y="-18.669" size="0.8128" layer="21">SBUS OUT</text>
<text x="-6.477" y="-18.542" size="0.8128" layer="21">PPM RC</text>
<text x="4.953" y="-18.669" size="1.016" layer="21">GPS MODULE</text>
<text x="-5.08" y="-26.416" size="1.016" layer="21">SPI</text>
<text x="-19.177" y="-34.036" size="1.016" layer="21">I/O PWM OUT</text>
<text x="4.953" y="-34.036" size="1.016" layer="21">FMU PWM OUT</text>
</package>
</packages>
<packages3d>
<package3d name="PIXHAWK4" urn="urn:adsk.eagle:package:27432501/3" type="box" library_version="9">
<packageinstances>
<packageinstance name="PIXHAWK4"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="POWER_1" urn="urn:adsk.eagle:symbol:27432500/3" library_version="9">
<wire x1="-5.08" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="5V" x="15.24" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="CURRENT" x="15.24" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="VOLTAGE" x="15.24" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="2.54" visible="pin" length="middle" rot="R180"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GPS_MODULE" urn="urn:adsk.eagle:symbol:27432492/2" library_version="9">
<pin name="VCC" x="-15.24" y="12.7" visible="pin" length="middle"/>
<pin name="RX" x="-15.24" y="7.62" visible="pin" length="middle"/>
<pin name="TX" x="-15.24" y="10.16" visible="pin" length="middle"/>
<pin name="SCL1" x="-15.24" y="5.08" visible="pin" length="middle"/>
<pin name="SDA1" x="-15.24" y="2.54" visible="pin" length="middle"/>
<pin name="SAFETY_SWITCH" x="-15.24" y="0" visible="pin" length="middle"/>
<pin name="SAFETY_SWITCH_LED" x="-15.24" y="-2.54" visible="pin" length="middle"/>
<pin name="VDD_3V3" x="-15.24" y="-5.08" visible="pin" length="middle"/>
<pin name="BUZZER" x="-15.24" y="-7.62" visible="pin" length="middle"/>
<pin name="GND" x="-15.24" y="-10.16" visible="pin" length="middle"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TELEM_1" urn="urn:adsk.eagle:symbol:27432498/3" library_version="10">
<pin name="VCC" x="-12.7" y="7.62" visible="pin" length="middle"/>
<pin name="TX" x="-12.7" y="5.08" visible="pin" length="middle"/>
<pin name="RX" x="-12.7" y="2.54" visible="pin" length="middle"/>
<pin name="CTS" x="-12.7" y="0" visible="pin" length="middle"/>
<pin name="RTS" x="-12.7" y="-2.54" visible="pin" length="middle"/>
<pin name="GND" x="-12.7" y="-5.08" visible="pin" length="middle"/>
<text x="-7.62" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="I2C_A" urn="urn:adsk.eagle:symbol:27432495/3" library_version="11">
<pin name="VCC" x="12.7" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="SCL4" x="12.7" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="SDA4" x="12.7" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="12.7" y="-2.54" visible="pin" length="middle" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="TELEM_2" urn="urn:adsk.eagle:symbol:27432489/3" library_version="11">
<pin name="VCC" x="-12.7" y="5.08" visible="pin" length="middle"/>
<pin name="TX" x="-12.7" y="2.54" visible="pin" length="middle"/>
<pin name="RX" x="-12.7" y="0" visible="pin" length="middle"/>
<pin name="CTS" x="-12.7" y="-2.54" visible="pin" length="middle"/>
<pin name="RTS" x="-12.7" y="-5.08" visible="pin" length="middle"/>
<pin name="GND" x="-12.7" y="-7.62" visible="pin" length="middle"/>
<text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="PPM" urn="urn:adsk.eagle:symbol:27432487/2" library_version="11">
<pin name="VCC" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="PPM" x="-7.62" y="0" visible="pin" length="middle"/>
<pin name="GND" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="POWER_1" urn="urn:adsk.eagle:component:27454088/4" prefix="U" library_version="9">
<gates>
<gate name="G$1" symbol="POWER_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="5V" pad="VCC_1 VCC_2"/>
<connect gate="G$1" pin="CURRENT" pad="CURRENT_1"/>
<connect gate="G$1" pin="GND" pad="GND_1 GND_2"/>
<connect gate="G$1" pin="VOLTAGE" pad="VOLTAGE_1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GPS_MODULE" urn="urn:adsk.eagle:component:27454091/3" prefix="U" library_version="9">
<gates>
<gate name="G$1" symbol="GPS_MODULE" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="BUZZER" pad="BUZZER"/>
<connect gate="G$1" pin="GND" pad="GND_14"/>
<connect gate="G$1" pin="RX" pad="RX_4"/>
<connect gate="G$1" pin="SAFETY_SWITCH" pad="SAFETY_SWITCH"/>
<connect gate="G$1" pin="SAFETY_SWITCH_LED" pad="SAFETY_SWITCH_LED"/>
<connect gate="G$1" pin="SCL1" pad="SCL1"/>
<connect gate="G$1" pin="SDA1" pad="SDA1"/>
<connect gate="G$1" pin="TX" pad="TX_4"/>
<connect gate="G$1" pin="VCC" pad="VCC_13"/>
<connect gate="G$1" pin="VDD_3V3" pad="VDD_3V3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TELEM_1" urn="urn:adsk.eagle:component:27454084/4" prefix="U" library_version="10">
<gates>
<gate name="G$1" symbol="TELEM_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="CTS" pad="CTS_1"/>
<connect gate="G$1" pin="GND" pad="GND_6"/>
<connect gate="G$1" pin="RTS" pad="RTS_1"/>
<connect gate="G$1" pin="RX" pad="RX_1"/>
<connect gate="G$1" pin="TX" pad="TX_1"/>
<connect gate="G$1" pin="VCC" pad="VCC_5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="I2C_A" urn="urn:adsk.eagle:component:27454089/4" prefix="U" library_version="11">
<gates>
<gate name="G$1" symbol="I2C_A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="GND" pad="GND_11"/>
<connect gate="G$1" pin="SCL4" pad="SCL4"/>
<connect gate="G$1" pin="SDA4" pad="SDA4"/>
<connect gate="G$1" pin="VCC" pad="VCC_10"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TELEM_2" urn="urn:adsk.eagle:component:27454083/4" prefix="U" library_version="11">
<gates>
<gate name="G$1" symbol="TELEM_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="CTS" pad="CTS_2"/>
<connect gate="G$1" pin="GND" pad="GND_7"/>
<connect gate="G$1" pin="RTS" pad="RTS_2"/>
<connect gate="G$1" pin="RX" pad="RX_2"/>
<connect gate="G$1" pin="TX" pad="TX_2"/>
<connect gate="G$1" pin="VCC" pad="VCC_6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PPM" urn="urn:adsk.eagle:component:27454086/3" prefix="U" library_version="11">
<gates>
<gate name="G$1" symbol="PPM" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="PIXHAWK4">
<connects>
<connect gate="G$1" pin="GND" pad="GND_13"/>
<connect gate="G$1" pin="PPM" pad="PPM"/>
<connect gate="G$1" pin="VCC" pad="VCC_12"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27432501/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="JST-2-SMD" urn="urn:adsk.eagle:footprint:37657/1" library_version="1">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="JST-2-SMD" urn="urn:adsk.eagle:package:38042/1" type="box" library_version="1">
<description>JST-Right Angle Male Header SMT
Specifications:
Pin count: 2
Pin pitch: 2mm

Datasheet referenced for footprint
Example device(s):
CONN_02
JST_2MM_MALE
</description>
<packageinstances>
<packageinstance name="JST-2-SMD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="JST_2MM_MALE" urn="urn:adsk.eagle:symbol:37934/1" library_version="1">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.302" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.302" x2="5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="4.064" y2="0" width="0.254" layer="94"/>
<wire x1="4.064" y1="0" x2="-1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="-1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="4.318" y2="1.778" width="0.254" layer="94"/>
<wire x1="4.318" y1="1.778" x2="4.318" y2="3.302" width="0.254" layer="94"/>
<wire x1="4.318" y1="3.302" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="1.016" x2="3.048" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="1.524" width="0.254" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="+" x="2.54" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="PAD2" x="5.08" y="2.54" visible="off" length="point" rot="R90"/>
<pin name="PAD1" x="-2.54" y="2.54" visible="off" length="point" rot="R90"/>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_2MM_MALE" urn="urn:adsk.eagle:component:38321/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;JST 2MM MALE RA CONNECTOR&lt;/h3&gt;
Two pin, compact surface mount connector. Commonly used as a battery input connection point. We really like the solid locking feeling and high current rating on these small connectors. We use these all the time as battery connectors. Mates to single-cell LiPo batteries.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8612"&gt;JST Right Angle Connector - White&lt;/a&gt; (PRT-08612)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/Prototyping/Connectors/JST-Horizontal.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It was used on these SparkFun products:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11214"&gt;SparkFun MOSFET Power Controller&lt;/a&gt; (PRT-11214)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13777"&gt;SparkFun Battery Babysitter - LiPo Battery Manager&lt;/a&gt; (PRT-13777)&lt;/li&gt;
&lt;li&gt;And many, many others that required a lipo battery connection.&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="JST_2MM_MALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
<connect gate="G$1" pin="PAD1" pad="NC1"/>
<connect gate="G$1" pin="PAD2" pad="NC2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38042/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
<attribute name="SF_ID" value="PRT-08612" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="bec" urn="urn:adsk.eagle:library:27454581">
<packages>
<package name="BEC" urn="urn:adsk.eagle:footprint:27454582/1" library_version="3">
<pad name="VIN" x="-2.54" y="2.54" drill="0.6" shape="square"/>
<pad name="GND" x="-2.54" y="0" drill="0.6" shape="square"/>
<pad name="GND_2" x="2.54" y="0" drill="0.6" shape="square"/>
<pad name="VOUT" x="2.54" y="2.54" drill="0.6" shape="square"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="-3.81" y2="3.81" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="BEC" urn="urn:adsk.eagle:package:27454584/1" type="box" library_version="3">
<packageinstances>
<packageinstance name="BEC"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BEC" urn="urn:adsk.eagle:symbol:27454583/3" library_version="3">
<pin name="VIN" x="-12.7" y="0" visible="pin" length="middle"/>
<pin name="GND" x="-12.7" y="-2.54" visible="pin" length="middle"/>
<pin name="VOUT" x="15.24" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="GND_2" x="15.24" y="-2.54" visible="pin" length="middle" rot="R180"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-7.62" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BECIRCUIT" urn="urn:adsk.eagle:component:27454585/3" prefix="U" library_version="3">
<gates>
<gate name="G$1" symbol="BEC" x="-2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="BEC">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND_2" pad="GND_2"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VOUT" pad="VOUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27454584/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Electromechanical" urn="urn:adsk.eagle:library:516">
<description>&lt;h3&gt;SparkFun Electromechanical Parts&lt;/h3&gt;
This library contains electromechanical devices, like motors, speakers,servos, and relays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X03" urn="urn:adsk.eagle:footprint:38549/1" library_version="1">
<description>&lt;h3&gt;3-Pin Header&lt;/h3&gt;
&lt;p&gt;0.1"-spaced 3-pin header. 
&lt;ul&gt;&lt;li&gt;Drill diameter: 40 mil&lt;/li&gt;
&lt;li&gt;Copper diameter: 74 mil&lt;/li&gt;&lt;/ul&gt;
&lt;h4&gt;Devices Using&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;SERVO&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="1X03" urn="urn:adsk.eagle:package:38562/1" type="box" library_version="1">
<description>3-Pin Header
0.1"-spaced 3-pin header. 
Drill diameter: 40 mil
Copper diameter: 74 mil
Devices Using
SERVO</description>
<packageinstances>
<packageinstance name="1X03"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SMALL_SERVO_MOTOR" urn="urn:adsk.eagle:symbol:38548/1" library_version="1">
<description>&lt;h3&gt;Servo Motor&lt;/h3&gt;</description>
<wire x1="-12.7" y1="3.81" x2="-10.16" y2="3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="-10.16" y1="-8.89" x2="-12.7" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="-3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="-15.24" y1="-3.81" x2="-15.24" y2="-1.27" width="0.254" layer="94" curve="-180"/>
<wire x1="-15.24" y1="-1.27" x2="-12.7" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-1.27" x2="-12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="-12.7" y1="1.27" x2="-12.7" y2="3.81" width="0.254" layer="94"/>
<wire x1="-10.16" y1="3.81" x2="-10.16" y2="1.27" width="0.254" layer="94"/>
<wire x1="-10.16" y1="1.27" x2="-10.16" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-1.27" x2="-7.62" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-10.16" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-3.81" x2="-10.16" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-6.35" x2="-10.16" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-3.81" x2="-12.7" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-3.81" x2="-12.7" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-6.35" x2="-12.7" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-12.7" y1="1.27" x2="-16.764" y2="1.27" width="0.254" layer="94"/>
<wire x1="-16.764" y1="1.27" x2="-17.78" y2="1.27" width="0.254" layer="94"/>
<wire x1="-17.78" y1="1.27" x2="-17.78" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-6.35" x2="-16.764" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-16.764" y1="-6.35" x2="-12.7" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-6.35" x2="-1.016" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-6.35" x2="0" y2="-6.35" width="0.254" layer="94"/>
<wire x1="0" y1="-6.35" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-10.16" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-1.016" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="-16.764" y1="1.27" x2="-16.764" y2="-6.35" width="0.1524" layer="94"/>
<pin name="SIG" x="2.54" y="0" visible="pin" length="short" rot="R180"/>
<pin name="V+" x="2.54" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="2.54" y="-5.08" visible="pin" length="short" rot="R180"/>
<text x="0.508" y="0.254" size="1.27" layer="94">W</text>
<text x="0.508" y="-2.286" size="1.27" layer="94">R</text>
<text x="0.508" y="-4.826" size="1.27" layer="94">B</text>
<text x="-9.906" y="1.524" size="1.778" layer="95" font="vector">&gt;Name</text>
<text x="-9.906" y="-6.604" size="1.778" layer="95" font="vector" align="top-left">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SERVO" urn="urn:adsk.eagle:component:38568/1" prefix="M" library_version="1">
<description>&lt;h3&gt;Servo Motor&lt;/h3&gt;
&lt;p&gt;3-pin connector for a standard servo motor (voltage supply, ground, and control signal.)&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9065"&gt;Servo - Generic (Sub-Micro Size)&lt;/a&gt; (ROB-09065)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9347"&gt;Servo - Generic High Torque Continuous Rotation (Standard Size)&lt;/a&gt; (ROB-09347)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11965"&gt;Servo - Generic High Torque (Standard Size)&lt;/a&gt; (ROB-11965)&lt;/li&gt;</description>
<gates>
<gate name="G$1" symbol="SMALL_SERVO_MOTOR" x="7.62" y="2.54"/>
</gates>
<devices>
<device name="" package="1X03">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="SIG" pad="1"/>
<connect gate="G$1" pin="V+" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38562/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="motor" urn="urn:adsk.eagle:library:27452721">
<packages>
<package name="308-100" urn="urn:adsk.eagle:footprint:27452722/1" library_version="1">
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-3.75" y1="1.5" x2="-6.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="1.5" x2="-7" y2="1" width="0.127" layer="21" curve="90"/>
<wire x1="-7" y1="1" x2="-7" y2="-1" width="0.127" layer="21"/>
<wire x1="-7" y1="-1" x2="-6.5" y2="-1.5" width="0.127" layer="21" curve="90"/>
<wire x1="-6.5" y1="-1.5" x2="-3.75" y2="-1.5" width="0.127" layer="21"/>
<pad name="A" x="-5.5" y="2.119" drill="0.26" diameter="0.8" shape="long"/>
<pad name="C" x="-5.5" y="-2.119" drill="0.26" diameter="0.8" shape="long"/>
<pad name="B" x="-8.04" y="-0.04" drill="0.26" diameter="0.8" shape="long"/>
<text x="0" y="4.5" size="1.016" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="308-100" urn="urn:adsk.eagle:package:27452724/1" type="box" library_version="1">
<packageinstances>
<packageinstance name="308-100"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MOTOR" urn="urn:adsk.eagle:symbol:27452723/1" library_version="1">
<circle x="0" y="0" radius="3.5921" width="0.254" layer="94"/>
<text x="0" y="0" size="2.54" layer="94" ratio="12" align="center">M</text>
<text x="5.08" y="2.54" size="1.778" layer="95" ratio="12">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96" ratio="12">&gt;VALUE</text>
<wire x1="0" y1="-5.08" x2="0" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<pin name="A" x="-2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="B" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="C" x="2.54" y="-7.62" visible="pad" length="short" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="308-100" urn="urn:adsk.eagle:component:27452725/1" prefix="M" library_version="1">
<description>Pico Vibe&lt;br/&gt;&lt;br /&gt;

8mm Vibration Motor - 3mm Type</description>
<gates>
<gate name="G$1" symbol="MOTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="308-100">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452724/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PRECISION_MICRODRIVES" value="308-100" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ESC" urn="urn:adsk.eagle:library:27453053">
<packages>
<package name="ESC" urn="urn:adsk.eagle:footprint:27453055/1" library_version="3">
<pad name="VCC(B+)" x="-2.54" y="7.62" drill="0.6" shape="square"/>
<pad name="GND" x="-2.54" y="6.35" drill="0.6" shape="square"/>
<pad name="A" x="-2.54" y="5.08" drill="0.6" shape="square"/>
<pad name="B" x="-2.54" y="3.81" drill="0.6" shape="square"/>
<pad name="C" x="-2.54" y="2.54" drill="0.6" shape="square"/>
<pad name="5V" x="-2.54" y="1.27" drill="0.6" shape="square"/>
<pad name="GND_S" x="-2.54" y="0" drill="0.6" shape="square"/>
<pad name="SIGNAL(PWM)" x="-2.54" y="-1.27" drill="0.6" shape="square"/>
<wire x1="-3.81" y1="8.89" x2="-1.27" y2="8.89" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="8.89" width="0.1524" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="ESC" urn="urn:adsk.eagle:package:27453057/1" type="box" library_version="3">
<packageinstances>
<packageinstance name="ESC"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ESC" urn="urn:adsk.eagle:symbol:27453056/2" library_version="3">
<pin name="A" x="12.7" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="B" x="12.7" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="C" x="12.7" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="VCC(B+)" x="-25.4" y="17.78" visible="pin" length="middle"/>
<pin name="GND" x="-25.4" y="12.7" visible="pin" length="middle"/>
<pin name="5V" x="-25.4" y="5.08" visible="pin" length="middle"/>
<pin name="SIGNAL(PWM)" x="-25.4" y="2.54" visible="pin" length="middle"/>
<pin name="GND_S" x="-25.4" y="0" visible="pin" length="middle"/>
<wire x1="7.62" y1="22.86" x2="-20.32" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="22.86" x2="-20.32" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="-2.54" x2="7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="22.86" width="0.1524" layer="94"/>
<text x="-20.32" y="25.4" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESPEEDC" urn="urn:adsk.eagle:component:27453058/3" prefix="U" library_version="3">
<gates>
<gate name="G$1" symbol="ESC" x="5.08" y="-7.62"/>
</gates>
<devices>
<device name="" package="ESC">
<connects>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND_S" pad="GND_S"/>
<connect gate="G$1" pin="SIGNAL(PWM)" pad="SIGNAL(PWM)"/>
<connect gate="G$1" pin="VCC(B+)" pad="VCC(B+)"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27453057/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="gps_module" urn="urn:adsk.eagle:library:27452558">
<packages>
<package name="GPS" urn="urn:adsk.eagle:footprint:27452559/1" library_version="4">
<pad name="VCC" x="-3.81" y="7.62" drill="0.6" shape="square"/>
<pad name="RX" x="-3.81" y="6.35" drill="0.6" shape="square"/>
<pad name="TX" x="-3.81" y="5.08" drill="0.6" shape="square"/>
<pad name="SCL" x="-3.81" y="3.81" drill="0.6" shape="square"/>
<pad name="SDA" x="-3.81" y="2.54" drill="0.6" shape="square"/>
<pad name="SFT_SW" x="-3.81" y="1.27" drill="0.6" shape="square"/>
<pad name="S_SW_LED" x="-3.81" y="0" drill="0.6" shape="square"/>
<pad name="VDD(3V3)" x="-3.81" y="-1.27" drill="0.6" shape="square"/>
<pad name="BUZZER" x="-3.81" y="-2.54" drill="0.6" shape="square" rot="R90"/>
<pad name="GND" x="-3.81" y="-3.81" drill="0.6" shape="square" rot="R90"/>
<wire x1="-5.08" y1="8.89" x2="-1.27" y2="8.89" width="0.127" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="8.89" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="GPS" urn="urn:adsk.eagle:package:27452561/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="GPS"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="M8NGPS" urn="urn:adsk.eagle:symbol:27452560/3" library_version="4">
<pin name="VCC(5V)" x="-7.62" y="10.16" visible="pin" length="middle"/>
<pin name="RX" x="-7.62" y="7.62" visible="pin" length="middle"/>
<pin name="TX" x="-7.62" y="5.08" visible="pin" length="middle"/>
<pin name="SCL" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="SDA" x="-7.62" y="0" visible="pin" length="middle"/>
<pin name="SFTY_SW" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<pin name="S_SW_LED" x="-7.62" y="-5.08" visible="pin" length="middle"/>
<pin name="VDD(3V3)" x="-7.62" y="-7.62" visible="pin" length="middle"/>
<pin name="BUZZER" x="-7.62" y="-10.16" visible="pin" length="middle"/>
<pin name="GND" x="-7.62" y="-12.7" visible="pin" length="middle"/>
<wire x1="-2.54" y1="12.7" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GPSMODULE" urn="urn:adsk.eagle:component:27452562/4" prefix="U" library_version="4">
<gates>
<gate name="G$1" symbol="M8NGPS" x="-7.62" y="0"/>
</gates>
<devices>
<device name="" package="GPS">
<connects>
<connect gate="G$1" pin="BUZZER" pad="BUZZER"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RX" pad="RX"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
<connect gate="G$1" pin="SFTY_SW" pad="SFT_SW"/>
<connect gate="G$1" pin="S_SW_LED" pad="S_SW_LED"/>
<connect gate="G$1" pin="TX" pad="TX"/>
<connect gate="G$1" pin="VCC(5V)" pad="VCC"/>
<connect gate="G$1" pin="VDD(3V3)" pad="VDD(3V3)"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452561/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="telemetry" urn="urn:adsk.eagle:library:27453039">
<packages>
<package name="TELEMETRY" urn="urn:adsk.eagle:footprint:27453040/1" library_version="4">
<pad name="TX" x="0" y="0" drill="0.6" shape="square"/>
<pad name="RX" x="0" y="1.27" drill="0.6" shape="square"/>
<pad name="VCC" x="0" y="2.54" drill="0.6" shape="square"/>
<pad name="CTS" x="0" y="-1.27" drill="0.6" shape="square"/>
<pad name="RTS" x="0" y="-2.54" drill="0.6" shape="square"/>
<pad name="GND" x="0" y="-3.81" drill="0.6" shape="square"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-5.08" x2="1.27" y2="-5.08" width="0.127" layer="21"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.81" x2="-1.27" y2="3.81" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="TELEMETRY" urn="urn:adsk.eagle:package:27453042/1" type="box" library_version="4">
<packageinstances>
<packageinstance name="TELEMETRY"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TELEMETRY" urn="urn:adsk.eagle:symbol:27453041/3" library_version="4">
<pin name="VCC" x="10.16" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="TX" x="10.16" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="RX" x="10.16" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="CTS" x="10.16" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="RTS" x="10.16" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="10.16" y="-7.62" visible="pin" length="middle" rot="R180"/>
<text x="-5.08" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TELEMETRY" urn="urn:adsk.eagle:component:27453043/4" prefix="U" library_version="4">
<gates>
<gate name="G$1" symbol="TELEMETRY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TELEMETRY">
<connects>
<connect gate="G$1" pin="CTS" pad="CTS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="RTS" pad="RTS"/>
<connect gate="G$1" pin="RX" pad="RX"/>
<connect gate="G$1" pin="TX" pad="TX"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27453042/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Nvidia_Jetson_Nano" urn="urn:adsk.eagle:library:27450991">
<packages>
<package name="JETSON_NANO" urn="urn:adsk.eagle:footprint:27450998/2" library_version="4">
<wire x1="50" y1="-40" x2="-50" y2="-40" width="0.127" layer="21"/>
<wire x1="-50" y1="-40" x2="-50" y2="40" width="0.127" layer="21"/>
<wire x1="-50" y1="40" x2="50" y2="40" width="0.127" layer="21"/>
<wire x1="50" y1="40" x2="50" y2="-40" width="0.127" layer="21"/>
<pad name="I2S_4_SDOUT" x="42" y="-18" drill="0.6" shape="square"/>
<pad name="GND_8" x="40" y="-18" drill="0.6" shape="square"/>
<pad name="SPI_2_MOSI" x="40" y="-16" drill="0.6" shape="square"/>
<pad name="I2S_4_SDIN" x="42" y="-16" drill="0.6" shape="square"/>
<pad name="UART_2_CTS" x="42" y="-14" drill="0.6" shape="square"/>
<pad name="I2S_4_LRCK" x="40" y="-14" drill="0.6" shape="square"/>
<pad name="GPIO_PE6" x="40" y="-12" drill="0.6" shape="square"/>
<pad name="GND_7" x="42" y="-12" drill="0.6" shape="square"/>
<pad name="LCD_BL_PWM" x="42" y="-10" drill="0.6" shape="square"/>
<pad name="GPIO_PZ0" x="40" y="-10" drill="0.6" shape="square"/>
<pad name="GND_6" x="42" y="-8" drill="0.6" shape="square"/>
<pad name="CAM_AF_EN" x="40" y="-8" drill="0.6" shape="square"/>
<pad name="I2C_1_SDA" x="40" y="-6" drill="0.6" shape="square"/>
<pad name="I2C_1_SCL" x="42" y="-6" drill="0.6" shape="square"/>
<pad name="SPI_1_CS1" x="42" y="-4" drill="0.6" shape="square"/>
<pad name="GND_5" x="40" y="-4" drill="0.6" shape="square"/>
<pad name="SPI_1_CS0" x="42" y="-2" drill="0.6" shape="square"/>
<pad name="SPI_1_SCK" x="40" y="-2" drill="0.6" shape="square"/>
<pad name="SPI_1_MISO" x="40" y="0" drill="0.6" shape="square"/>
<pad name="SPI_2_MISO" x="42" y="0" drill="0.6" shape="square"/>
<pad name="GND_4" x="42" y="2" drill="0.6" shape="square"/>
<pad name="SPI_1_MOSI" x="40" y="2" drill="0.6" shape="square"/>
<pad name="3.3_VDC_2" x="40" y="4" drill="0.6" shape="square"/>
<pad name="SPI_2_CS0" x="42" y="4" drill="0.6" shape="square"/>
<pad name="SPI_2_CS1" x="42" y="6" drill="0.6" shape="square"/>
<pad name="LCD_TE" x="40" y="6" drill="0.6" shape="square"/>
<pad name="SPI_2_SCK" x="40" y="8" drill="0.6" shape="square"/>
<pad name="GND_3" x="42" y="8" drill="0.6" shape="square"/>
<pad name="UART_2_RTS" x="40" y="10" drill="0.6" shape="square"/>
<pad name="I2S_4_SCLK" x="42" y="10" drill="0.6" shape="square"/>
<pad name="UART_2_RX" x="42" y="12" drill="0.6" shape="square"/>
<pad name="GND_2" x="40" y="12" drill="0.6" shape="square"/>
<pad name="AUDIO_MCLK" x="40" y="14" drill="0.6" shape="square"/>
<pad name="UART_2_TX" x="42" y="14" drill="0.6" shape="square"/>
<pad name="GND_1" x="42" y="16" drill="0.6" shape="square"/>
<pad name="I2C_2_SCL" x="40" y="16" drill="0.6" shape="square"/>
<pad name="5.0_VDC_2" x="42" y="18" drill="0.6" shape="square"/>
<pad name="I2C_2_SDA" x="40" y="18" drill="0.6" shape="square"/>
<pad name="3.3_VDC_1" x="40" y="20" drill="0.6" shape="square"/>
<pad name="5.0_VDC_1" x="42" y="20" drill="0.6" shape="square"/>
<pad name="GND" x="-45.09" y="16.19" drill="0.6" shape="square"/>
<pad name="CAM_D0_N" x="-45.09" y="13.65" drill="0.6" shape="square"/>
<pad name="CAM_D0_P" x="-45.09" y="11.11" drill="0.6" shape="square"/>
<pad name="CAM_D1_N" x="-45.09" y="8.57" drill="0.6" shape="square"/>
<pad name="CAM_D1_P" x="-45.09" y="6.03" drill="0.6" shape="square"/>
<pad name="CAM_CK_N" x="-45.09" y="3.49" drill="0.6" shape="square"/>
<pad name="CAM_CK_P" x="-45.09" y="0.95" drill="0.6" shape="square"/>
<pad name="CAM_I00" x="-45.09" y="-1.59" drill="0.6" shape="square"/>
<pad name="CAM_I01" x="-45.09" y="-4.13" drill="0.6" shape="square"/>
<pad name="CAM_SCL" x="-45.09" y="-6.67" drill="0.6" shape="square"/>
<pad name="CAM_SDA" x="-45.09" y="-9.21" drill="0.6" shape="square"/>
<pad name="CAM_3V3" x="-45.09" y="-11.75" drill="0.6" shape="square"/>
</package>
</packages>
<packages3d>
<package3d name="JETSON_NANO" urn="urn:adsk.eagle:package:27451000/2" type="box" library_version="4">
<packageinstances>
<packageinstance name="JETSON_NANO"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="J13_HEADER" urn="urn:adsk.eagle:symbol:27455815/1" library_version="4">
<pin name="GND" x="-15.24" y="12.7" visible="pin" length="middle"/>
<pin name="CAM_D0_N" x="-15.24" y="10.16" visible="pin" length="middle"/>
<pin name="CAM_D0_P" x="-15.24" y="7.62" visible="pin" length="middle"/>
<pin name="CAM_D1_N" x="-15.24" y="5.08" visible="pin" length="middle"/>
<pin name="CAM_D1_P" x="-15.24" y="2.54" visible="pin" length="middle"/>
<pin name="CAM_CK_N" x="-15.24" y="0" visible="pin" length="middle"/>
<pin name="CAM_CK_P" x="-15.24" y="-2.54" visible="pin" length="middle"/>
<pin name="CAM_I00" x="-15.24" y="-5.08" visible="pin" length="middle"/>
<pin name="CAM_I01" x="-15.24" y="-7.62" visible="pin" length="middle"/>
<pin name="CAM_SCL" x="-15.24" y="-10.16" visible="pin" length="middle"/>
<pin name="CAM_SDA" x="-15.24" y="-12.7" visible="pin" length="middle"/>
<pin name="CAM_3V3" x="-15.24" y="-15.24" visible="pin" length="middle"/>
<wire x1="-10.16" y1="15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="J41_HEADER" urn="urn:adsk.eagle:symbol:27450999/2" library_version="4">
<pin name="SPI_1_MOSI" x="22.86" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="LCD_TE" x="-22.86" y="-5.08" visible="pin" length="middle"/>
<pin name="SPI_2_SCK" x="22.86" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="UART_2_RTS" x="22.86" y="-25.4" visible="pin" length="middle" rot="R180"/>
<pin name="GND" x="22.86" y="17.78" visible="pin" length="middle" rot="R180"/>
<pin name="AUDIO_MCLK" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<pin name="I2C_2_SCL" x="-22.86" y="17.78" visible="pin" length="middle"/>
<pin name="I2C_2_SDA" x="-22.86" y="15.24" visible="pin" length="middle"/>
<pin name="3.3_VDC" x="22.86" y="20.32" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_1_MISO" x="22.86" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_1_SCK" x="22.86" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="I2C_1_SDA" x="-22.86" y="20.32" visible="pin" length="middle"/>
<pin name="CAM_AF_EN" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="GPIO_PZ0" x="-22.86" y="-15.24" visible="pin" length="middle"/>
<pin name="GPIO_PE6" x="-22.86" y="-17.78" visible="pin" length="middle"/>
<pin name="I2S_4_LRCK" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="SPI_2_MOSI" x="22.86" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="I2S_4_SDOUT" x="-22.86" y="5.08" visible="pin" length="middle"/>
<pin name="I2S_4_SDIN" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="UART_2_CTS" x="22.86" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="LCD_BL_PWM" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<pin name="I2C_1_SCL" x="-22.86" y="22.86" visible="pin" length="middle"/>
<pin name="SPI_1_CS1" x="22.86" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_1_CS0" x="22.86" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_2_MISO" x="22.86" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_2_CS0" x="22.86" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="SPI_2_CS1" x="22.86" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="I2S_4_SCLK" x="-22.86" y="10.16" visible="pin" length="middle"/>
<pin name="UART_2_RX" x="22.86" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="UART_2_TX" x="22.86" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="5.0_VDC" x="22.86" y="22.86" visible="pin" length="middle" rot="R180"/>
<wire x1="-17.78" y1="25.4" x2="-17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-27.94" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<text x="-17.78" y="27.94" size="1.778" layer="95">&gt;NAME</text>
<text x="-17.78" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="J13" urn="urn:adsk.eagle:component:27455816/1" prefix="U" library_version="4">
<gates>
<gate name="G$1" symbol="J13_HEADER" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="JETSON_NANO">
<connects>
<connect gate="G$1" pin="CAM_3V3" pad="CAM_3V3"/>
<connect gate="G$1" pin="CAM_CK_N" pad="CAM_CK_N"/>
<connect gate="G$1" pin="CAM_CK_P" pad="CAM_CK_P"/>
<connect gate="G$1" pin="CAM_D0_N" pad="CAM_D0_N"/>
<connect gate="G$1" pin="CAM_D0_P" pad="CAM_D0_P"/>
<connect gate="G$1" pin="CAM_D1_N" pad="CAM_D1_N"/>
<connect gate="G$1" pin="CAM_D1_P" pad="CAM_D1_P"/>
<connect gate="G$1" pin="CAM_I00" pad="CAM_I00"/>
<connect gate="G$1" pin="CAM_I01" pad="CAM_I01"/>
<connect gate="G$1" pin="CAM_SCL" pad="CAM_SCL"/>
<connect gate="G$1" pin="CAM_SDA" pad="CAM_SDA"/>
<connect gate="G$1" pin="GND" pad="GND"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27451000/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="J41" urn="urn:adsk.eagle:component:27451001/4" prefix="U" library_version="4">
<gates>
<gate name="G$1" symbol="J41_HEADER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JETSON_NANO">
<connects>
<connect gate="G$1" pin="3.3_VDC" pad="3.3_VDC_1 3.3_VDC_2"/>
<connect gate="G$1" pin="5.0_VDC" pad="5.0_VDC_1 5.0_VDC_2"/>
<connect gate="G$1" pin="AUDIO_MCLK" pad="AUDIO_MCLK"/>
<connect gate="G$1" pin="CAM_AF_EN" pad="CAM_AF_EN"/>
<connect gate="G$1" pin="GND" pad="GND_1 GND_2 GND_3 GND_4 GND_5 GND_6 GND_7 GND_8"/>
<connect gate="G$1" pin="GPIO_PE6" pad="GPIO_PE6"/>
<connect gate="G$1" pin="GPIO_PZ0" pad="GPIO_PZ0"/>
<connect gate="G$1" pin="I2C_1_SCL" pad="I2C_1_SCL"/>
<connect gate="G$1" pin="I2C_1_SDA" pad="I2C_1_SDA"/>
<connect gate="G$1" pin="I2C_2_SCL" pad="I2C_2_SCL"/>
<connect gate="G$1" pin="I2C_2_SDA" pad="I2C_2_SDA"/>
<connect gate="G$1" pin="I2S_4_LRCK" pad="I2S_4_LRCK"/>
<connect gate="G$1" pin="I2S_4_SCLK" pad="I2S_4_SCLK"/>
<connect gate="G$1" pin="I2S_4_SDIN" pad="I2S_4_SDIN"/>
<connect gate="G$1" pin="I2S_4_SDOUT" pad="I2S_4_SDOUT"/>
<connect gate="G$1" pin="LCD_BL_PWM" pad="LCD_BL_PWM"/>
<connect gate="G$1" pin="LCD_TE" pad="LCD_TE"/>
<connect gate="G$1" pin="SPI_1_CS0" pad="SPI_1_CS0"/>
<connect gate="G$1" pin="SPI_1_CS1" pad="SPI_1_CS1"/>
<connect gate="G$1" pin="SPI_1_MISO" pad="SPI_1_MISO"/>
<connect gate="G$1" pin="SPI_1_MOSI" pad="SPI_1_MOSI"/>
<connect gate="G$1" pin="SPI_1_SCK" pad="SPI_1_SCK"/>
<connect gate="G$1" pin="SPI_2_CS0" pad="SPI_2_CS0"/>
<connect gate="G$1" pin="SPI_2_CS1" pad="SPI_2_CS1"/>
<connect gate="G$1" pin="SPI_2_MISO" pad="SPI_2_MISO"/>
<connect gate="G$1" pin="SPI_2_MOSI" pad="SPI_2_MOSI"/>
<connect gate="G$1" pin="SPI_2_SCK" pad="SPI_2_SCK"/>
<connect gate="G$1" pin="UART_2_CTS" pad="UART_2_CTS"/>
<connect gate="G$1" pin="UART_2_RTS" pad="UART_2_RTS"/>
<connect gate="G$1" pin="UART_2_RX" pad="UART_2_RX"/>
<connect gate="G$1" pin="UART_2_TX" pad="UART_2_TX"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27451000/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="raspberrycamera" urn="urn:adsk.eagle:library:27452758">
<packages>
<package name="RPCAMERA" urn="urn:adsk.eagle:footprint:27452759/1" library_version="3">
<pad name="GND_1" x="-2.54" y="10.16" drill="0.6" shape="square"/>
<pad name="CAM_D0_N" x="-2.54" y="8.89" drill="0.6" shape="square"/>
<pad name="CAM_D0_P" x="-2.54" y="7.62" drill="0.6" shape="square"/>
<pad name="GND_2" x="-2.54" y="6.35" drill="0.6" shape="square"/>
<pad name="CAM_D1_N" x="-2.54" y="5.08" drill="0.6" shape="square"/>
<pad name="CAM_D1_P" x="-2.54" y="3.81" drill="0.6" shape="square"/>
<pad name="GND_3" x="-2.54" y="2.54" drill="0.6" shape="square"/>
<pad name="CAM_CK_N" x="-2.54" y="1.27" drill="0.6" shape="square"/>
<pad name="CAM_CK_P" x="-2.54" y="0" drill="0.6" shape="square"/>
<pad name="GND_4" x="-2.54" y="-1.27" drill="0.6" shape="square"/>
<pad name="CAM_IO0" x="-2.54" y="-2.54" drill="0.6" shape="square"/>
<pad name="CAM_IO1" x="-2.54" y="-3.81" drill="0.6" shape="square"/>
<pad name="CAM_SCL" x="-2.54" y="-5.08" drill="0.6" shape="square"/>
<pad name="CAM_SDA" x="-2.54" y="-6.35" drill="0.6" shape="square"/>
<pad name="CAM_3V3" x="-2.54" y="-7.62" drill="0.6" shape="square"/>
<wire x1="-3.81" y1="11.43" x2="-1.27" y2="11.43" width="0.127" layer="21"/>
<wire x1="-1.27" y1="11.43" x2="-1.27" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-8.89" x2="-3.81" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-8.89" x2="-3.81" y2="11.43" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="RPCAMERA" urn="urn:adsk.eagle:package:27452761/1" type="box" library_version="3">
<packageinstances>
<packageinstance name="RPCAMERA"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RASPCAMERA" urn="urn:adsk.eagle:symbol:27452760/2" library_version="3">
<pin name="GND" x="-7.62" y="20.32" visible="pin" length="middle"/>
<pin name="CAM_D0_N" x="-7.62" y="17.78" visible="pin" length="middle"/>
<pin name="CAM_D0_P" x="-7.62" y="15.24" visible="pin" length="middle"/>
<pin name="CAM_D1_N" x="-7.62" y="12.7" visible="pin" length="middle"/>
<pin name="CAM_D1_P" x="-7.62" y="10.16" visible="pin" length="middle"/>
<pin name="CAM_CK_N" x="-7.62" y="7.62" visible="pin" length="middle"/>
<pin name="CAM_CK_P" x="-7.62" y="5.08" visible="pin" length="middle"/>
<pin name="CAM_I00" x="-7.62" y="2.54" visible="pin" length="middle"/>
<pin name="CAM_I01" x="-7.62" y="0" visible="pin" length="middle"/>
<pin name="CAM_SCL" x="-7.62" y="-2.54" visible="pin" length="middle"/>
<pin name="CAM_SDA" x="-7.62" y="-5.08" visible="pin" length="middle"/>
<pin name="CAM_3V3" x="-7.62" y="-7.62" visible="pin" length="middle"/>
<wire x1="-2.54" y1="22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="22.86" width="0.254" layer="94"/>
<text x="-2.54" y="25.4" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RPICAMERA" urn="urn:adsk.eagle:component:27452762/3" prefix="U" library_version="3">
<gates>
<gate name="G$1" symbol="RASPCAMERA" x="-5.08" y="-7.62"/>
</gates>
<devices>
<device name="" package="RPCAMERA">
<connects>
<connect gate="G$1" pin="CAM_3V3" pad="CAM_3V3"/>
<connect gate="G$1" pin="CAM_CK_N" pad="CAM_CK_N"/>
<connect gate="G$1" pin="CAM_CK_P" pad="CAM_CK_P"/>
<connect gate="G$1" pin="CAM_D0_N" pad="CAM_D0_N"/>
<connect gate="G$1" pin="CAM_D0_P" pad="CAM_D0_P"/>
<connect gate="G$1" pin="CAM_D1_N" pad="CAM_D1_N"/>
<connect gate="G$1" pin="CAM_D1_P" pad="CAM_D1_P"/>
<connect gate="G$1" pin="CAM_I00" pad="CAM_IO0"/>
<connect gate="G$1" pin="CAM_I01" pad="CAM_IO1"/>
<connect gate="G$1" pin="CAM_SCL" pad="CAM_SCL"/>
<connect gate="G$1" pin="CAM_SDA" pad="CAM_SDA"/>
<connect gate="G$1" pin="GND" pad="GND_1 GND_2 GND_3 GND_4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27452761/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pitottüp" urn="urn:adsk.eagle:library:27454429">
<packages>
<package name="PITOTTUBE" urn="urn:adsk.eagle:footprint:27454430/1" library_version="2">
<pad name="5V" x="-1.27" y="5.08" drill="0.6" shape="square"/>
<pad name="SCL" x="-1.27" y="3.81" drill="0.6" shape="square"/>
<pad name="SDA" x="-1.27" y="2.54" drill="0.6" shape="square"/>
<pad name="GND" x="-1.27" y="1.27" drill="0.6" shape="square"/>
<wire x1="-2.54" y1="6.35" x2="1.27" y2="6.35" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.35" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="6.35" width="0.1524" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="PITOTTUBE" urn="urn:adsk.eagle:package:27454432/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="PITOTTUBE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PITOTTUBE" urn="urn:adsk.eagle:symbol:27454431/2" library_version="2">
<pin name="5V" x="-5.08" y="10.16" visible="pin" length="middle"/>
<pin name="SCL" x="-5.08" y="7.62" visible="pin" length="middle"/>
<pin name="SDA" x="-5.08" y="5.08" visible="pin" length="middle"/>
<pin name="GND" x="-5.08" y="2.54" visible="pin" length="middle"/>
<wire x1="0" y1="12.7" x2="10.16" y2="12.7" width="0.1524" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="0" width="0.1524" layer="94"/>
<wire x1="10.16" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="12.7" width="0.1524" layer="94"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PT60AIRMODULE" urn="urn:adsk.eagle:component:27454433/2" prefix="U" library_version="2">
<gates>
<gate name="G$1" symbol="PITOTTUBE" x="-5.08" y="-7.62"/>
</gates>
<devices>
<device name="" package="PITOTTUBE">
<connects>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SCL" pad="SCL"/>
<connect gate="G$1" pin="SDA" pad="SDA"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27454432/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="receiver" urn="urn:adsk.eagle:library:27453701">
<packages>
<package name="RECEIVER" urn="urn:adsk.eagle:footprint:27453702/2" library_version="7">
<pad name="GND" x="-5.08" y="6.35" drill="0.6" shape="square"/>
<pad name="VCC" x="-5.08" y="3.81" drill="0.6" shape="square"/>
<pad name="SBUS/PPM" x="-5.08" y="1.27" drill="0.6" shape="square"/>
<wire x1="-7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-2.54" y2="0" width="0.1524" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="RECEIVER" urn="urn:adsk.eagle:package:27453704/2" type="box" library_version="7">
<packageinstances>
<packageinstance name="RECEIVER"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RECEIVER" urn="urn:adsk.eagle:symbol:27453703/5" library_version="7">
<pin name="GND" x="12.7" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="SBUS/PPM" x="12.7" y="0" visible="pin" length="middle" rot="R180"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RECEVIER" urn="urn:adsk.eagle:component:27453705/7" prefix="U" library_version="7">
<gates>
<gate name="G$1" symbol="RECEIVER" x="-5.08" y="-7.62"/>
</gates>
<devices>
<device name="" package="RECEIVER">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SBUS/PPM" pad="SBUS/PPM"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27453704/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BeagleBone_Blue_R3" urn="urn:adsk.eagle:library:5828899">
<description>Generated from &lt;b&gt;BeagleBone_Blue.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:footprint:5829391/2" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="-3" y="2" size="0.762" layer="25">&gt;NAME</text>
<text x="-3" y="-2.7" size="0.762" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SPARKFUN-ELECTROMECHANICAL_TACTILE-SWITCH-1101NE" urn="urn:adsk.eagle:package:5829833/3" type="model" library_version="49">
<description>SparkFun SKU# COM-08229</description>
<packageinstances>
<packageinstance name="TACTILE-SWITCH-1101NE"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" urn="urn:adsk.eagle:symbol:5828967/1" library_version="49">
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS032G05A3SM-SPST-MOMENTARY-SW" urn="urn:adsk.eagle:component:5829909/3" prefix="S" library_version="49">
<description>6x3.5mm 2-pin Momentary Switch (Push-button)
&lt;p&gt;
&lt;a href="https://www.digikey.com/products/en?keywords=CKN10388TR-ND"&gt;Digikey Link&lt;/a&gt;
&lt;br&gt;
&lt;a href="https://media.digikey.com/pdf/Data%20Sheets/C&amp;K/RS-032G05_-SM_RT.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="SPARKFUN-ELECTROMECHANICAL_SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-1101NE" package="TACTILE-SWITCH-1101NE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829833/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fuse" urn="urn:adsk.eagle:library:233">
<description>&lt;b&gt;Fuses and Fuse Holders&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="F457" urn="urn:adsk.eagle:footprint:14035/1" library_version="2">
<description>&lt;b&gt;FUSE HOLDER&lt;/b&gt;&lt;p&gt;
CS Schukat</description>
<wire x1="7.366" y1="6.223" x2="7.366" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-27.686" y1="-6.223" x2="-19.685" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-27.686" y1="-6.223" x2="-27.686" y2="6.223" width="0.1524" layer="21"/>
<wire x1="7.366" y1="6.223" x2="4.318" y2="6.223" width="0.1524" layer="21"/>
<wire x1="4.318" y1="6.223" x2="4.318" y2="3.175" width="0.1524" layer="21"/>
<wire x1="4.318" y1="6.223" x2="-16.637" y2="6.223" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-6.223" x2="7.366" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="4.318" y1="3.175" x2="-16.637" y2="3.175" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.27" x2="4.318" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="4.318" y1="-3.175" x2="-16.637" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-3.175" x2="4.318" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-16.637" y1="6.223" x2="-16.637" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-16.637" y1="6.223" x2="-19.685" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="6.223" x2="-19.685" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="6.223" x2="-27.686" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-6.223" x2="-16.637" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-16.637" y1="-6.223" x2="4.318" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-21.717" y1="4.064" x2="-21.717" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-4.064" x2="-21.717" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-21.717" y1="4.064" x2="-25.654" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-4.064" x2="-25.654" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-0.127" x2="-25.654" y2="0.127" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-0.127" x2="-25.4" y2="-0.127" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.127" x2="-25.4" y2="-0.127" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.127" x2="-25.654" y2="0.127" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="0.127" x2="-25.654" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-3.683" x2="-22.86" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-3.683" x2="-25.654" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-0.381" x2="-22.86" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-0.381" x2="-25.654" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-25.654" y1="-0.381" x2="-25.654" y2="-0.127" width="0.1524" layer="21"/>
<wire x1="4.318" y1="3.175" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-16.637" y1="3.175" x2="-16.637" y2="-6.223" width="0.1524" layer="21"/>
<pad name="2" x="5.08" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<pad name="1" x="-5.08" y="0" drill="1.3208" diameter="2.54" shape="long" rot="R90"/>
<text x="-15.875" y="1.143" size="1.27" layer="51" ratio="10">F457 10A/250V</text>
<text x="-15.875" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-27.686" y="-8.636" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-25.654" y1="-3.683" x2="-22.86" y2="-0.381" layer="21"/>
<rectangle x1="-16.637" y1="3.175" x2="4.318" y2="6.223" layer="21"/>
<rectangle x1="-16.637" y1="-6.223" x2="4.318" y2="-3.175" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="F457" urn="urn:adsk.eagle:package:14076/1" type="box" library_version="2">
<description>FUSE HOLDER
CS Schukat</description>
<packageinstances>
<packageinstance name="F457"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FUSE" urn="urn:adsk.eagle:symbol:14027/1" library_version="2">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="F457" urn="urn:adsk.eagle:component:14103/2" prefix="F" uservalue="yes" library_version="2">
<description>&lt;b&gt;FUSE HOLDER&lt;/b&gt;&lt;p&gt;
CS, Schukat</description>
<gates>
<gate name="1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="F457">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14076/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="Holybro_PM07_Power-Management" library_urn="urn:adsk.eagle:library:27452541" deviceset="POWER_IN&amp;OUT" device="" package3d_urn="urn:adsk.eagle:package:27452551/2"/>
<part name="U2" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="POWER_1" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="J1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="JST_2MM_MALE" device="" package3d_urn="urn:adsk.eagle:package:38042/1"/>
<part name="U3" library="bec" library_urn="urn:adsk.eagle:library:27454581" deviceset="BECIRCUIT" device="" package3d_urn="urn:adsk.eagle:package:27454584/1" value="5V"/>
<part name="U4" library="Holybro_PM07_Power-Management" library_urn="urn:adsk.eagle:library:27452541" deviceset="FMU_PWM_OUT" device="" package3d_urn="urn:adsk.eagle:package:27452551/2"/>
<part name="M1" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M2" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M3" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M4" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M5" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M6" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="M7" library="SparkFun-Electromechanical" library_urn="urn:adsk.eagle:library:516" deviceset="SERVO" device="" package3d_urn="urn:adsk.eagle:package:38562/1"/>
<part name="U5" library="Holybro_PM07_Power-Management" library_urn="urn:adsk.eagle:library:27452541" deviceset="I/O_PWM_OUT" device="" package3d_urn="urn:adsk.eagle:package:27452551/2"/>
<part name="M9" library="motor" library_urn="urn:adsk.eagle:library:27452721" deviceset="308-100" device="" package3d_urn="urn:adsk.eagle:package:27452724/1"/>
<part name="U6" library="ESC" library_urn="urn:adsk.eagle:library:27453053" deviceset="ESPEEDC" device="" package3d_urn="urn:adsk.eagle:package:27453057/1"/>
<part name="U7" library="gps_module" library_urn="urn:adsk.eagle:library:27452558" deviceset="GPSMODULE" device="" package3d_urn="urn:adsk.eagle:package:27452561/1"/>
<part name="U8" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="GPS_MODULE" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="U11" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="TELEM_1" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="U12" library="telemetry" library_urn="urn:adsk.eagle:library:27453039" deviceset="TELEMETRY" device="" package3d_urn="urn:adsk.eagle:package:27453042/1"/>
<part name="U13" library="Nvidia_Jetson_Nano" library_urn="urn:adsk.eagle:library:27450991" deviceset="J13" device="" package3d_urn="urn:adsk.eagle:package:27451000/2"/>
<part name="U14" library="raspberrycamera" library_urn="urn:adsk.eagle:library:27452758" deviceset="RPICAMERA" device="" package3d_urn="urn:adsk.eagle:package:27452761/1"/>
<part name="U15" library="pitottüp" library_urn="urn:adsk.eagle:library:27454429" deviceset="PT60AIRMODULE" device="" package3d_urn="urn:adsk.eagle:package:27454432/1"/>
<part name="U16" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="I2C_A" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="U17" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="TELEM_2" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="U18" library="Nvidia_Jetson_Nano" library_urn="urn:adsk.eagle:library:27450991" deviceset="J41" device="" package3d_urn="urn:adsk.eagle:package:27451000/2"/>
<part name="U9" library="Pixhawk4" library_urn="urn:adsk.eagle:library:27432483" deviceset="PPM" device="" package3d_urn="urn:adsk.eagle:package:27432501/3"/>
<part name="U10" library="receiver" library_urn="urn:adsk.eagle:library:27453701" deviceset="RECEVIER" device="" package3d_urn="urn:adsk.eagle:package:27453704/2"/>
<part name="S1" library="BeagleBone_Blue_R3" library_urn="urn:adsk.eagle:library:5828899" deviceset="RS032G05A3SM-SPST-MOMENTARY-SW" device="SMD-1101NE" package3d_urn="urn:adsk.eagle:package:5829833/3"/>
<part name="F2" library="fuse" library_urn="urn:adsk.eagle:library:233" deviceset="F457" device="" package3d_urn="urn:adsk.eagle:package:14076/1" value="100A"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="134.62" y="-91.44" size="2.54" layer="97">Fırçasız DC Motorun Güç Dağıtım Kartına Bağlantısı</text>
<text x="134.62" y="-17.78" size="2.54" layer="97">Kumanda Alıcısının Uçuş Kontrolcüsü Üzerindeki PPM Bağlantısı</text>
<text x="134.62" y="-48.26" size="2.54" layer="97">Telemetrinin Uçuş Kontrolcüsü Üzerindeki Telem1 Pinine Bağlantısı</text>
<text x="-83.82" y="-88.9" size="2.286" layer="97">Raspberry PI Kamerasının Jetson Nano Üzerindeki J13 Pinine Bağlantısı</text>
<text x="134.62" y="15.24" size="2.54" layer="97">Pitot Tüpünün Uçuş Kontrolcüsü Üzerindeki I2C Bağlantısı</text>
<text x="35.56" y="-68.58" size="2.54" layer="97">Jetson Nanonun Uçuş Kontrolcüsü Bağlantısı</text>
<text x="-81.28" y="-20.32" size="1.778" layer="97">Pixhawk Güç Girişi</text>
<text x="-79.502" y="3.048" size="1.778" layer="97">LIPO Pil Girişi</text>
<text x="-50.292" y="-2.54" size="1.27" layer="97">Akım Kesici</text>
<text x="-37.592" y="-2.54" size="1.27" layer="97">Cam Sigorta</text>
<text x="-15.24" y="-22.86" size="1.27" layer="97">U1 Sembolü üzerinde güç dağıtım kartının üzerindeki
LIPO girişi ve Pixhawk güç çıkışları birleştirilmiştir</text>
<text x="-27.94" y="27.94" size="2.54" layer="97">BEC Devresi</text>
<text x="58.42" y="-5.08" size="1.27" layer="97">Güç Dağıtım Kartı Servo PWM Çıkışları</text>
<wire x1="30.48" y1="-63.5" x2="30.48" y2="-33.02" width="0.1524" layer="97" style="longdash"/>
<wire x1="30.48" y1="-33.02" x2="30.48" y2="12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="30.48" y1="12.7" x2="30.48" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="30.48" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="20.32" x2="129.54" y2="-12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-12.7" x2="129.54" y2="-43.18" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-43.18" x2="129.54" y2="-63.5" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-63.5" x2="30.48" y2="-63.5" width="0.1524" layer="97" style="longdash"/>
<text x="99.06" y="15.24" size="2.54" layer="97">Servo Bağlantıları</text>
<wire x1="30.48" y1="12.7" x2="-86.36" y2="12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="-86.36" y1="12.7" x2="-86.36" y2="-33.02" width="0.1524" layer="97" style="longdash"/>
<wire x1="-86.36" y1="-33.02" x2="30.48" y2="-33.02" width="0.1524" layer="97" style="longdash"/>
<text x="-83.82" y="-30.48" size="2.54" layer="97">Uçuş Kontrolcüsü ve Güç Dağıtım Kartı İçin Güç Bağlantıları</text>
<text x="-63.5" y="-78.74" size="1.778" layer="97">Uçuş Kontrolcüsü GPS Girişi</text>
<text x="-10.16" y="-78.74" size="1.778" layer="97">GPS Sensörü</text>
<text x="-83.82" y="-38.1" size="2.54" layer="97">GPS Sensörü İçin Uçuş Kontrolcüsü Bağlantısı</text>
<wire x1="-86.36" y1="-33.02" x2="-86.36" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="-86.36" y1="-83.82" x2="30.48" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="30.48" y1="-83.82" x2="30.48" y2="-63.5" width="0.1524" layer="97" style="longdash"/>
<text x="-20.32" y="-134.62" size="1.778" layer="97">Nvidia Jetson Nano</text>
<text x="-55.88" y="-134.62" size="1.778" layer="97">Raspberry PI Kamerası</text>
<wire x1="-86.36" y1="-83.82" x2="-86.36" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="-86.36" y1="-139.7" x2="30.48" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="30.48" y1="-139.7" x2="30.48" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<text x="104.14" y="-101.6" size="1.778" layer="97">Kontrolcü Telem2 Pini</text>
<text x="53.34" y="-137.16" size="1.778" layer="97">Nvidia Jetson Nano</text>
<wire x1="30.48" y1="-139.7" x2="129.54" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-63.5" x2="129.54" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-83.82" x2="129.54" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-139.7" x2="241.3" y2="-139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="241.3" y1="-139.7" x2="241.3" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<text x="142.24" y="-137.16" size="1.27" layer="97">Güç Dağıtım Kartı BLDC Motor PWM Çıkışları</text>
<text x="187.96" y="-132.08" size="1.778" layer="97">ESC</text>
<text x="227.584" y="-123.19" size="1.778" layer="97">BLDC Motor</text>
<text x="162.56" y="-7.62" size="1.778" layer="97">Kontrolcü I2C Pini</text>
<text x="187.96" y="-7.62" size="1.778" layer="97">Pitot Tüpü</text>
<text x="190.5" y="-38.1" size="1.778" layer="97">Pixhawk PPM Pini</text>
<text x="160.02" y="-38.1" size="1.778" layer="97">Kumanda Alıcısı</text>
<text x="167.64" y="-78.74" size="1.778" layer="97">Telemetri</text>
<text x="193.04" y="-78.74" size="1.778" layer="97">Pixhawk Telemetri1 Pini</text>
<wire x1="241.3" y1="-83.82" x2="241.3" y2="-43.18" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-83.82" x2="241.3" y2="-83.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-43.18" x2="241.3" y2="-43.18" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="-12.7" x2="241.3" y2="-12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="241.3" y1="-12.7" x2="241.3" y2="-43.18" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="20.32" x2="241.3" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="241.3" y1="20.32" x2="241.3" y2="-12.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="20.32" x2="129.54" y2="33.02" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="33.02" x2="-86.36" y2="33.02" width="0.1524" layer="97" style="longdash"/>
<wire x1="-86.36" y1="33.02" x2="-86.36" y2="12.7" width="0.1524" layer="97" style="longdash"/>
</plain>
<instances>
<instance part="U1" gate="G$1" x="-12.7" y="-2.54" smashed="yes">
<attribute name="NAME" x="-15.24" y="5.842" size="1.778" layer="95"/>
<attribute name="VALUE" x="-15.24" y="-17.78" size="1.778" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="-76.2" y="-10.16" smashed="yes">
<attribute name="NAME" x="-81.28" y="-2.032" size="1.778" layer="95"/>
<attribute name="VALUE" x="-81.28" y="-17.526" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="-58.42" y="0" smashed="yes" rot="R90">
<attribute name="NAME" x="-63.5" y="5.588" size="1.778" layer="95"/>
</instance>
<instance part="U3" gate="G$1" x="10.16" y="25.4" smashed="yes">
<attribute name="NAME" x="2.54" y="28.448" size="1.778" layer="95"/>
<attribute name="VALUE" x="2.54" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="68.58" y="7.62" smashed="yes">
<attribute name="NAME" x="58.42" y="15.748" size="1.778" layer="95"/>
<attribute name="VALUE" x="58.166" y="-2.286" size="1.778" layer="96"/>
</instance>
<instance part="M1" gate="G$1" x="40.64" y="-25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="37.846" y="-26.924" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="37.846" y="-31.496" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M2" gate="G$1" x="66.04" y="-25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="63.246" y="-29.464" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="63.246" y="-34.036" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M3" gate="G$1" x="53.34" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="50.546" y="-44.704" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="50.546" y="-49.276" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M4" gate="G$1" x="78.74" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="75.946" y="-44.704" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="75.946" y="-49.276" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M5" gate="G$1" x="91.44" y="-25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="88.646" y="-29.464" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="88.646" y="-34.036" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M6" gate="G$1" x="104.14" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="101.346" y="-44.704" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="101.346" y="-49.276" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="M7" gate="G$1" x="116.84" y="-25.4" smashed="yes" rot="R90">
<attribute name="NAME" x="114.046" y="-29.464" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="114.046" y="-34.036" size="1.778" layer="95" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="U5" gate="G$1" x="154.94" y="-114.3" smashed="yes" rot="MR0">
<attribute name="NAME" x="145.288" y="-98.552" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="159.766" y="-134.874" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="M9" gate="G$1" x="233.68" y="-114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="231.14" y="-109.22" size="1.778" layer="95" ratio="12"/>
<attribute name="VALUE" x="229.87" y="-120.396" size="1.778" layer="96" ratio="12"/>
</instance>
<instance part="U6" gate="G$1" x="208.28" y="-124.46" smashed="yes">
<attribute name="NAME" x="188.214" y="-101.092" size="1.778" layer="95"/>
<attribute name="VALUE" x="187.96" y="-129.54" size="1.778" layer="96"/>
</instance>
<instance part="U7" gate="G$1" x="-7.62" y="-58.42" smashed="yes">
<attribute name="NAME" x="-10.16" y="-44.958" size="1.778" layer="95"/>
<attribute name="VALUE" x="-10.16" y="-76.2" size="1.778" layer="96"/>
</instance>
<instance part="U8" gate="G$1" x="-48.26" y="-60.96" smashed="yes" rot="MR0">
<attribute name="NAME" x="-60.706" y="-44.958" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-46.736" y="-76.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U11" gate="G$1" x="200.66" y="-66.04" smashed="yes">
<attribute name="NAME" x="193.04" y="-55.118" size="1.778" layer="95"/>
<attribute name="VALUE" x="193.04" y="-76.2" size="1.778" layer="96"/>
</instance>
<instance part="U12" gate="G$1" x="172.72" y="-63.5" smashed="yes">
<attribute name="NAME" x="167.64" y="-55.118" size="1.778" layer="95"/>
<attribute name="VALUE" x="167.64" y="-76.2" size="1.778" layer="96"/>
</instance>
<instance part="U13" gate="G$1" x="-10.16" y="-111.76" smashed="yes">
<attribute name="NAME" x="-20.32" y="-96.012" size="1.778" layer="95"/>
<attribute name="VALUE" x="-20.32" y="-132.08" size="1.778" layer="96"/>
</instance>
<instance part="U14" gate="G$1" x="-40.64" y="-119.38" smashed="yes" rot="MR0">
<attribute name="NAME" x="-51.562" y="-96.012" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-41.656" y="-132.08" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="U15" gate="G$1" x="187.96" y="-2.54" smashed="yes">
<attribute name="NAME" x="187.96" y="10.668" size="1.778" layer="95"/>
<attribute name="VALUE" x="187.96" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="U16" gate="G$1" x="165.1" y="2.54" smashed="yes">
<attribute name="NAME" x="162.306" y="10.668" size="1.778" layer="95"/>
<attribute name="VALUE" x="162.306" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="U17" gate="G$1" x="119.38" y="-86.36" smashed="yes">
<attribute name="NAME" x="111.76" y="-78.232" size="1.778" layer="95"/>
<attribute name="VALUE" x="111.506" y="-99.06" size="1.778" layer="96"/>
</instance>
<instance part="U18" gate="G$1" x="71.12" y="-104.14" smashed="yes">
<attribute name="NAME" x="53.34" y="-77.978" size="1.778" layer="95"/>
<attribute name="VALUE" x="53.086" y="-134.366" size="1.778" layer="96"/>
</instance>
<instance part="U9" gate="G$1" x="193.04" y="-27.94" smashed="yes">
<attribute name="NAME" x="190.5" y="-22.098" size="1.778" layer="95"/>
<attribute name="VALUE" x="190.5" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="U10" gate="G$1" x="167.64" y="-27.94" smashed="yes">
<attribute name="NAME" x="160.02" y="-22.352" size="1.778" layer="95"/>
<attribute name="VALUE" x="160.02" y="-35.56" size="1.778" layer="96"/>
</instance>
<instance part="S1" gate="G$1" x="-45.72" y="2.54" smashed="yes">
<attribute name="NAME" x="-49.022" y="4.318" size="1.778" layer="95"/>
</instance>
<instance part="F2" gate="1" x="-33.02" y="2.54" smashed="yes">
<attribute name="NAME" x="-39.37" y="3.937" size="1.778" layer="95"/>
<attribute name="VALUE" x="-32.004" y="3.937" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="5V"/>
<pinref part="U1" gate="G$1" pin="5V_1"/>
<wire x1="-60.96" y1="-5.08" x2="-20.32" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND_1"/>
<wire x1="-60.96" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="CURRENT"/>
<pinref part="U1" gate="G$1" pin="CURRENT_1"/>
<wire x1="-60.96" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="VOLTAGE"/>
<pinref part="U1" gate="G$1" pin="VOLTAGE_1"/>
<wire x1="-60.96" y1="-12.7" x2="-20.32" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VOLTAGE_IN_LIPO"/>
<pinref part="U3" gate="G$1" pin="VIN"/>
<wire x1="-25.4" y1="2.54" x2="-20.32" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="25.4" x2="-25.4" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="25.4" x2="-25.4" y2="2.54" width="0.1524" layer="91"/>
<pinref part="F2" gate="1" pin="2"/>
<wire x1="-27.94" y1="2.54" x2="-25.4" y2="2.54" width="0.1524" layer="91"/>
<junction x="-25.4" y="2.54"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="-"/>
<pinref part="U1" gate="G$1" pin="GROUND_IN_LIPO"/>
<wire x1="-53.34" y1="0" x2="-22.86" y2="0" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="-22.86" y1="0" x2="-20.32" y2="0" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="22.86" x2="-22.86" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="22.86" x2="-22.86" y2="0" width="0.1524" layer="91"/>
<junction x="-22.86" y="0"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VOUT"/>
<wire x1="25.4" y1="25.4" x2="86.36" y2="25.4" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="V+"/>
<wire x1="43.18" y1="-22.86" x2="43.18" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-17.78" x2="55.88" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-17.78" x2="68.58" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-17.78" x2="81.28" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-17.78" x2="93.98" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-17.78" x2="93.98" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VOLTAGE"/>
<wire x1="93.98" y1="12.7" x2="86.36" y2="12.7" width="0.1524" layer="91"/>
<pinref part="M3" gate="G$1" pin="V+"/>
<wire x1="86.36" y1="12.7" x2="83.82" y2="12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-38.1" x2="55.88" y2="-17.78" width="0.1524" layer="91"/>
<junction x="55.88" y="-17.78"/>
<pinref part="M2" gate="G$1" pin="V+"/>
<wire x1="68.58" y1="-22.86" x2="68.58" y2="-17.78" width="0.1524" layer="91"/>
<junction x="68.58" y="-17.78"/>
<pinref part="M5" gate="G$1" pin="V+"/>
<wire x1="93.98" y1="-22.86" x2="93.98" y2="-17.78" width="0.1524" layer="91"/>
<junction x="93.98" y="-17.78"/>
<pinref part="M4" gate="G$1" pin="V+"/>
<wire x1="81.28" y1="-38.1" x2="81.28" y2="-17.78" width="0.1524" layer="91"/>
<junction x="81.28" y="-17.78"/>
<pinref part="M6" gate="G$1" pin="V+"/>
<wire x1="106.68" y1="-38.1" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<wire x1="106.68" y1="12.7" x2="93.98" y2="12.7" width="0.1524" layer="91"/>
<junction x="93.98" y="12.7"/>
<pinref part="M7" gate="G$1" pin="V+"/>
<wire x1="119.38" y1="-22.86" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="12.7" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<junction x="106.68" y="12.7"/>
<wire x1="86.36" y1="25.4" x2="86.36" y2="12.7" width="0.1524" layer="91"/>
<junction x="86.36" y="12.7"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="GND_2"/>
<wire x1="25.4" y1="22.86" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
<pinref part="M7" gate="G$1" pin="GND"/>
<pinref part="M1" gate="G$1" pin="GND"/>
<wire x1="45.72" y1="-22.86" x2="45.72" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-20.32" x2="58.42" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-20.32" x2="71.12" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-20.32" x2="83.82" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-20.32" x2="96.52" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-20.32" x2="99.06" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="-20.32" x2="99.06" y2="10.16" width="0.1524" layer="91"/>
<wire x1="99.06" y1="10.16" x2="88.9" y2="10.16" width="0.1524" layer="91"/>
<pinref part="M3" gate="G$1" pin="GND"/>
<wire x1="88.9" y1="10.16" x2="83.82" y2="10.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-38.1" x2="58.42" y2="-20.32" width="0.1524" layer="91"/>
<junction x="58.42" y="-20.32"/>
<pinref part="M2" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="-22.86" x2="71.12" y2="-20.32" width="0.1524" layer="91"/>
<junction x="71.12" y="-20.32"/>
<pinref part="M4" gate="G$1" pin="GND"/>
<wire x1="83.82" y1="-38.1" x2="83.82" y2="-20.32" width="0.1524" layer="91"/>
<junction x="83.82" y="-20.32"/>
<pinref part="M5" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="-22.86" x2="96.52" y2="-20.32" width="0.1524" layer="91"/>
<junction x="96.52" y="-20.32"/>
<junction x="99.06" y="10.16"/>
<pinref part="M6" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="10.16" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="10.16" x2="99.06" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-38.1" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<junction x="109.22" y="10.16"/>
<wire x1="121.92" y1="-22.86" x2="121.92" y2="10.16" width="0.1524" layer="91"/>
<wire x1="88.9" y1="22.86" x2="88.9" y2="10.16" width="0.1524" layer="91"/>
<junction x="88.9" y="10.16"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="M1" gate="G$1" pin="SIG"/>
<pinref part="U4" gate="G$1" pin="S1"/>
<wire x1="40.64" y1="-22.86" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="12.7" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="M3" gate="G$1" pin="SIG"/>
<wire x1="53.34" y1="-38.1" x2="53.34" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-15.24" x2="43.18" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-15.24" x2="43.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="S2"/>
<wire x1="43.18" y1="10.16" x2="53.34" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="M2" gate="G$1" pin="SIG"/>
<wire x1="66.04" y1="-22.86" x2="66.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="-12.7" x2="45.72" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-12.7" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="S3"/>
<wire x1="45.72" y1="7.62" x2="53.34" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="M4" gate="G$1" pin="SIG"/>
<wire x1="78.74" y1="-38.1" x2="78.74" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-10.16" x2="48.26" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="S4"/>
<wire x1="48.26" y1="-10.16" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="5.08" x2="53.34" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="M5" gate="G$1" pin="SIG"/>
<wire x1="91.44" y1="-22.86" x2="91.44" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-7.62" x2="50.8" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-7.62" x2="50.8" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="S5"/>
<wire x1="50.8" y1="2.54" x2="53.34" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="M6" gate="G$1" pin="SIG"/>
<pinref part="U4" gate="G$1" pin="S6"/>
<wire x1="104.14" y1="-38.1" x2="104.14" y2="2.54" width="0.1524" layer="91"/>
<wire x1="104.14" y1="2.54" x2="83.82" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="M7" gate="G$1" pin="SIG"/>
<pinref part="U4" gate="G$1" pin="S7"/>
<wire x1="116.84" y1="-22.86" x2="116.84" y2="5.08" width="0.1524" layer="91"/>
<wire x1="116.84" y1="5.08" x2="83.82" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="M9" gate="G$1" pin="A"/>
<wire x1="226.06" y1="-111.76" x2="223.52" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-111.76" x2="223.52" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="A"/>
<wire x1="223.52" y1="-106.68" x2="220.98" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="M9" gate="G$1" pin="B"/>
<wire x1="226.06" y1="-114.3" x2="220.98" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="B"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="M9" gate="G$1" pin="C"/>
<wire x1="226.06" y1="-116.84" x2="223.52" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-116.84" x2="223.52" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="C"/>
<wire x1="223.52" y1="-121.92" x2="220.98" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="VCC(B+)"/>
<wire x1="182.88" y1="-106.68" x2="180.34" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-106.68" x2="180.34" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="B+_1"/>
<wire x1="180.34" y1="-101.6" x2="172.72" y2="-101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="-111.76" x2="177.8" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="177.8" y1="-111.76" x2="177.8" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND_1"/>
<wire x1="177.8" y1="-104.14" x2="172.72" y2="-104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="SIGNAL(PWM)"/>
<wire x1="182.88" y1="-121.92" x2="175.26" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-121.92" x2="175.26" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="M1"/>
<wire x1="175.26" y1="-106.68" x2="172.72" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="VCC"/>
<pinref part="U7" gate="G$1" pin="VCC(5V)"/>
<wire x1="-33.02" y1="-48.26" x2="-15.24" y2="-48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="TX"/>
<pinref part="U7" gate="G$1" pin="RX"/>
<wire x1="-33.02" y1="-50.8" x2="-15.24" y2="-50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="RX"/>
<pinref part="U7" gate="G$1" pin="TX"/>
<wire x1="-33.02" y1="-53.34" x2="-15.24" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SCL1"/>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="-33.02" y1="-55.88" x2="-15.24" y2="-55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SDA1"/>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="-33.02" y1="-58.42" x2="-15.24" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SAFETY_SWITCH"/>
<pinref part="U7" gate="G$1" pin="SFTY_SW"/>
<wire x1="-33.02" y1="-60.96" x2="-15.24" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="SAFETY_SWITCH_LED"/>
<pinref part="U7" gate="G$1" pin="S_SW_LED"/>
<wire x1="-33.02" y1="-63.5" x2="-15.24" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="VDD_3V3"/>
<pinref part="U7" gate="G$1" pin="VDD(3V3)"/>
<wire x1="-33.02" y1="-66.04" x2="-15.24" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="BUZZER"/>
<pinref part="U7" gate="G$1" pin="BUZZER"/>
<wire x1="-33.02" y1="-68.58" x2="-15.24" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="GND"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="-33.02" y1="-71.12" x2="-15.24" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="VCC"/>
<pinref part="U11" gate="G$1" pin="VCC"/>
<wire x1="182.88" y1="-58.42" x2="187.96" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="RX"/>
<pinref part="U11" gate="G$1" pin="TX"/>
<wire x1="182.88" y1="-60.96" x2="187.96" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="TX"/>
<pinref part="U11" gate="G$1" pin="RX"/>
<wire x1="182.88" y1="-63.5" x2="187.96" y2="-63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="CTS"/>
<pinref part="U11" gate="G$1" pin="CTS"/>
<wire x1="182.88" y1="-66.04" x2="187.96" y2="-66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="RTS"/>
<pinref part="U11" gate="G$1" pin="RTS"/>
<wire x1="182.88" y1="-68.58" x2="187.96" y2="-68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="GND"/>
<pinref part="U11" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="-71.12" x2="187.96" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="GND"/>
<pinref part="U13" gate="G$1" pin="GND"/>
<wire x1="-33.02" y1="-99.06" x2="-25.4" y2="-99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_D0_N"/>
<pinref part="U13" gate="G$1" pin="CAM_D0_N"/>
<wire x1="-33.02" y1="-101.6" x2="-25.4" y2="-101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_D0_P"/>
<pinref part="U13" gate="G$1" pin="CAM_D0_P"/>
<wire x1="-33.02" y1="-104.14" x2="-25.4" y2="-104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_D1_N"/>
<pinref part="U13" gate="G$1" pin="CAM_D1_N"/>
<wire x1="-33.02" y1="-106.68" x2="-25.4" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_D1_P"/>
<pinref part="U13" gate="G$1" pin="CAM_D1_P"/>
<wire x1="-33.02" y1="-109.22" x2="-25.4" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_CK_N"/>
<pinref part="U13" gate="G$1" pin="CAM_CK_N"/>
<wire x1="-33.02" y1="-111.76" x2="-25.4" y2="-111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_CK_P"/>
<pinref part="U13" gate="G$1" pin="CAM_CK_P"/>
<wire x1="-33.02" y1="-114.3" x2="-25.4" y2="-114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_I00"/>
<pinref part="U13" gate="G$1" pin="CAM_I00"/>
<wire x1="-33.02" y1="-116.84" x2="-25.4" y2="-116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_I01"/>
<pinref part="U13" gate="G$1" pin="CAM_I01"/>
<wire x1="-33.02" y1="-119.38" x2="-25.4" y2="-119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_SCL"/>
<pinref part="U13" gate="G$1" pin="CAM_SCL"/>
<wire x1="-33.02" y1="-121.92" x2="-25.4" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_SDA"/>
<pinref part="U13" gate="G$1" pin="CAM_SDA"/>
<wire x1="-33.02" y1="-124.46" x2="-25.4" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="U14" gate="G$1" pin="CAM_3V3"/>
<pinref part="U13" gate="G$1" pin="CAM_3V3"/>
<wire x1="-33.02" y1="-127" x2="-25.4" y2="-127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="VCC"/>
<pinref part="U15" gate="G$1" pin="5V"/>
<wire x1="177.8" y1="7.62" x2="182.88" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="SCL4"/>
<pinref part="U15" gate="G$1" pin="SCL"/>
<wire x1="177.8" y1="5.08" x2="182.88" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="SDA4"/>
<pinref part="U15" gate="G$1" pin="SDA"/>
<wire x1="177.8" y1="2.54" x2="182.88" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U16" gate="G$1" pin="GND"/>
<pinref part="U15" gate="G$1" pin="GND"/>
<wire x1="177.8" y1="0" x2="182.88" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="VCC"/>
<pinref part="U18" gate="G$1" pin="5.0_VDC"/>
<wire x1="106.68" y1="-81.28" x2="93.98" y2="-81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="TX"/>
<wire x1="106.68" y1="-83.82" x2="99.06" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-83.82" x2="99.06" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="UART_2_RX"/>
<wire x1="99.06" y1="-124.46" x2="93.98" y2="-124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="RX"/>
<wire x1="106.68" y1="-86.36" x2="101.6" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-86.36" x2="101.6" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="UART_2_TX"/>
<wire x1="101.6" y1="-121.92" x2="93.98" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="U17" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="-93.98" x2="96.52" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-93.98" x2="96.52" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="U18" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="-86.36" x2="93.98" y2="-86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="VCC"/>
<pinref part="U9" gate="G$1" pin="VCC"/>
<wire x1="180.34" y1="-25.4" x2="185.42" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="SBUS/PPM"/>
<pinref part="U9" gate="G$1" pin="PPM"/>
<wire x1="180.34" y1="-27.94" x2="185.42" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="GND"/>
<pinref part="U9" gate="G$1" pin="GND"/>
<wire x1="180.34" y1="-30.48" x2="185.42" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="+"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="2.54" x2="-50.8" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="2"/>
<pinref part="F2" gate="1" pin="1"/>
<wire x1="-40.64" y1="2.54" x2="-38.1" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
