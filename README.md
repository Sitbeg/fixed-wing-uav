<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Instagram][instagram-shield]][instagram-url]
[![Patreon][patreon-shield]][patreon-url]
[![Github][github-shield]][github-url]
[![Gitlab][gitlab-shield]][gitlab-url]
<!--[![Contributors][contributors-shield]][contributors-url]-->
<!--[![Issues][issues-shield]][issues-url]-->
<!--[![Forks][forks-shield]][forks-url]-->
<!--[![Stargazers][stars-shield]][stars-url]-->
<!--[![MIT License][license-shield]][license-url]-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Sitbeg/fixed-wing-uav/-/blob/main/Report/Vega_Logo.png">
    <img src="Report/Vega_Logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">VEGA AUTONOMOUS FIXED WING UAV PROJECT</h3>

  <p align="center">
    v0.87
    <br />
    <a href="https://gitlab.com/Sitbeg/fixed-wing-uav"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/Sitbeg/fixed-wing-uav">View Demo</a>
    ·
    <a href="https://gitlab.com/Sitbeg/fixed-wing-uav/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Sitbeg/fixed-wing-uav/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-team">About The Team</a></li>
    <li><a href="#about-the-members">About The Members</a></li>
    <li><a href="#about-the-project">About The Project</a></li>
    <li>
      <a href="#domestic-productions">Domestic Productions</a>
      <ul>
        <li><a href="#ground-station">Ground Station</a></li>
        <li><a href="#bldc-motor">BLDC Motor</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- About the Team -->
# About The Team

Although our team was founded in 2018 as an idea, it started its activities actively in 2020. The purpose of the establishment of our team is to train its members by putting them under pressure through competitions. It is to give the members the opportunity to improve themselves with their individual efforts before they graduate in many areas such as teamwork, developing a complete project, process management, as well as participating in competitions that will improve themselves much faster by trying to achieve open-ended outputs with certain inputs in limited time. The mentioned competitions can be in any field such as innovation, entrepreneurship, idea or prototype development.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- About the Members -->
# About The Members

## Electronics Team
  ### Alperen Arslan (Team Leader)
  - Education: Bachelor's Degree, Gebze Technical University, Electronics Engineering
  - Task: Team Coordination and Planning, Electronic Subsystem Planning and Material Selections, Circuit Analysis

  [![Instagram][instagram-shield]][instagram-url-alperen]    [![LinkedIn][linkedin-shield]][linkedin-url-alperen]    [![Github][github-shield]][github-url-alperen]    

  ### Emirhan Köse
  - Education: Bachelor's Degree, Gebze Technical University, Electronics Engineering
  - Task: Circuit Schematics and Diagram Design, Electronic Material Selection, Circuit Analysis 

  [![Instagram][instagram-shield]][instagram-url-emirhan]    [![LinkedIn][linkedin-shield]][linkedin-url-emirhan]    [![Github][github-shield]][github-url-emirhan]    

  ### Faruk Akdeniz
  - Education: Bachelor's Degree, Istanbul Medeniyet University, Electric and Electronics Engineering
  - Task: Motor Design, Analysis and Production 

  [![Instagram][instagram-shield]][instagram-url-faruk]

## Software Team
  ### Yavuz Selim İkizler
  - Education: Bachelor's Degree, Gebze Technical University, Computer Engineering
  - Task: Image Processing and It's Analysis

  [![Instagram][instagram-shield]][instagram-url-yavuz]

  ### İrem Çağın Yurttürk
  - Education: Bachelor's Degree, Gebze Technical University, Computer Engineering
  - Task: Ground Station Design and Analysis

  [![Instagram][instagram-shield]][instagram-url-irem]

  ### Tanay Bensu Yurttürk
  - Education: Bachelor's Degree, Gebze Technical University, Computer Engineering
  - Task: Ground Station Design and Analysis

  [![Instagram][instagram-shield]][instagram-url-bensu]

## Mechanics Team
  ### Ozancan Korkmaz
  - Education: Bachelor's Degree, Duzce University, Mechanics Engineering
  - Task: Mechanical Systems, Propeller, Finite Analysis Applications

  [![Instagram][instagram-shield]][instagram-url-ozan]
  [![LinkedIn][linkedin-shield]][linkedin-url-ozan]

  ### Mehmet Fatih Bulgur
  - Education: Bachelor's Degree, Gebze Technical University, Mechanics Engineering
  - Task: 3D Modelling, CAD, Body Design, Mechanical Systems

  [![Instagram][instagram-shield]][instagram-url-fatih]
  [![LinkedIn][linkedin-shield]][linkedin-url-fatih]

  ### Arif Emre Keçeci
  - Education: Bachelor's Degree, Gebze Technical University, Mechanics Engineering
  - Task: Airfoil Analysis, Tail Types Research and Design, Servo Control Systems

  [![Instagram][instagram-shield]][instagram-url-arif]

  ### Ömer Faruk Tokur
  - Education: Bachelor's Degree, Duzce University, Mechanics Engineering
  - Task: Airfoil Design, Mechanical Systems, Material Type Research, Finite Analysis

  [![Instagram][instagram-shield]][instagram-url-omer]

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- About the Project -->
# About The Project

Sitbeg UAV team consists of 10 undergraduate students from different universities and different fields. Our team consists of 3 separate sub-teams, namely software, mechanics and electronics, and our team captain has been determined as Alperen Arslan. Our UAV name is “VEGA”. 

Our team participates in the "Teknofest International Unmanned Aerial Vehicle" competition, which will be held in 2022 together with "VEGA".

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- Domestic Productions -->
# Domestic Productions
## Ground Station

Our team continues to work on ground station development. At the end of the process, it aims to produce a uniquely designed, useful and stable ground station.


![Ground Station][ground-station-url]


## BLDC Motor

Our team continues to work on BLDC motor. We aim to produce the most performance BLDC motor by producing many prototypes until the competition period. Although the diameter of the first designed engine is approximately 8 cm, the design studies for 4 cm are still in progress. The first prototypes will be produced with 3D printing, and when the desired performance is approached, aluminum production will begin, and performance will be increased.

The analyzes of the motors are made both theoretically and in software via Ansys Maxwell. 

![Rotor Part][rotor-1-url]

![Stator Part][stator-4-url]

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [contributors-url]: https://gitlab.com/Sitbeg/fixed-wing-uav/-/graphs/main -->
<!--[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge-->
<!--[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members-->
<!--[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge-->
<!--[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers-->
<!-- [issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [issues-url]: https://gitlab.com/Sitbeg/fixed-wing-uav/-/issues -->
<!--[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge-->
<!--[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt-->
[instagram-shield]: https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white
[instagram-url]: https://www.instagram.com/sitbeg/
[github-shield]: https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white
[github-url]: https://github.com/sitbeg?tab=repositories
[gitlab-shield]: https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white
[gitlab-url]: https://gitlab.com/Sitbeg
[patreon-shield]: https://img.shields.io/badge/Patreon-F96854?style=for-the-badge&logo=patreon&logoColor=white
[patreon-url]: https://www.patreon.com/sitbeg
[linkedin-shield]: https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white

[ground-station-url]: Software/Report_for_Software/arayüz.png
[rotor-1-url]: Electronics/Motor/Images/Component-1.1.jpeg
[stator-4-url]: Electronics/Motor/Images/Component-2.4.jpeg

<!-- MARKDOWN LINKS & IMAGES -->
<!-- Electronics Team -->
[instagram-url-alperen]: https://www.instagram.com/arslanalperen55/
[github-url-alperen]: https://github.com/arslanalperen
[linkedin-url-alperen]: https://www.linkedin.com/in/arslanalperen/

[instagram-url-emirhan]: https://www.instagram.com/emirhankose2/
[github-url-emirhan]: https://github.com/emirhankose
[linkedin-url-emirhan]: https://www.linkedin.com/in/emirhan-köse-b448b118b/

[instagram-url-faruk]: https://www.instagram.com/farukakdenz/

<!-- Software Team -->
[instagram-url-yavuz]: https://www.instagram.com/selimikzlr/

[instagram-url-irem]: https://www.instagram.com/ikizler_15/

[instagram-url-bensu]: https://www.instagram.com/ikizler_15/

<!-- Mechanics Team -->
[instagram-url-ozan]: https://www.instagram.com/canozankorkmaz/
[linkedin-url-ozan]: https://www.linkedin.com/in/ozancan-korkmaz-1a2457194/

[instagram-url-fatih]: https://www.instagram.com/ikizler_15/
[linkedin-url-fatih]: https://www.linkedin.com/in/mehmet-fatih-bulgur-61aba31a4/

[instagram-url-arif]: https://www.instagram.com/arif._.kececi/

[instagram-url-omer]: https://www.instagram.com/omer_tokur/
